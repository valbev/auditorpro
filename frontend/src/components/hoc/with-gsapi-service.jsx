import React from 'react'
import { GetSecretServiceConsumer } from '../react-context'

export const withGsapiService = () => (Wrapped) => (props) => (
  <GetSecretServiceConsumer>
    {(getSecretService) => {
      return <Wrapped {...props} getSecretService={getSecretService} />
    }}
  </GetSecretServiceConsumer>
)
