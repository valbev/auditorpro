export const user = (user) => ({
  id: user.id,
  email: user.email,
  name: user.first_name,
  lastName: user.last_name,
  phone: user.phone,
  birthday: user.birthday,
  address: user.address,
  sex: user.sex,
  type: 'secret-guest',
})
