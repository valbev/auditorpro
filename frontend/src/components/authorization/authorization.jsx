import React, { useContext, useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Context } from '../../context'
import { useValidation } from '../../services/utils'
import {
  AuthModalTemplate,
  AuthorizationModal,
  // PasswordRecoveryFirstStepModal,
  RegisterFirstStepModal,
  RegisterSecondStepModal,
} from './components'

import './modals.scss'
import { Actions } from './redux/actions'
import { authValidation, registerFirstStepValidation, registerSecondStepValidation } from './validation'

const authModals = {
  authorizationModal: {
    component: AuthorizationModal,
    title: 'Вход',
    initialData: {
      email: '',
      password: '',
    },
    validation: authValidation,
    socials: true,
  },
  registerFirstStepModal: {
    component: RegisterFirstStepModal,
    title: 'Регистрация',
    initialData: {
      email: '',
      password: '',
      repassword: '',
    },
    validation: registerFirstStepValidation,
    socials: true,
  },
  registerSecondStepModal: {
    component: RegisterSecondStepModal,
    title: 'Заполнение личных данных',
    initialData: {
      first_name: '',
      last_name: '',
      birthday: '',
      address: '',
      phone: '',
    },
    validation: registerSecondStepValidation,
  },
  passwordRecoveryFirstStepModal: {
    // component: PasswordRecoveryFirstStepModal,
    component: null,
    title: 'Восстановление пароля',
  },
  passwordRecoverySecondStepModal: {
    component: null,
  },
  passwordRecoverySuccessModal: {
    component: null,
    title: 'Восстановление пароля',
  },
}

export const Authorization = ({ children }) => {
  const dispatch = useDispatch()
  const { login, register } = useContext(Context)
  const { toggleModalOpened, closeAllModals } = Actions
  const modals = useSelector((store) => store.authorization)

  const activeModalArr = Object.entries(modals).find(([key, value]) => value)
  const activeModal = activeModalArr ? activeModalArr[0] : null

  const [data, setData] = useState({})
  const [errors, setErrors] = useState({})
  const [validation, setValidation] = useState({})
  const [savedDataToSubmit, saveDataToSubmit] = useState({})
  const { validateField, validateAll, hasErrors } = useValidation(validation)

  useEffect(() => {
    const initialData = activeModal ? authModals[activeModal].initialData : {}
    const initialErrors = { ...initialData }
    const validation = activeModal ? authModals[activeModal].validation : {}

    setData(initialData)
    setErrors(initialErrors)
    setValidation(validation)
  }, [activeModal])

  const handleChangeField = (event) => {
    const { name, value } = event.target
    const newData = {
      ...data,
      [name]: value,
    }
    setData(newData)
    setErrors({
      ...errors,
      [name]: validateField(name, value, newData),
    })
  }

  const handleSetModalOpened = (modal, isOpened = true) => {
    dispatch(toggleModalOpened(modal, isOpened))
  }

  const handleLogin = () => {
    const newErrors = validateAll(data)
    setErrors(newErrors)

    if (!hasErrors(newErrors)) {
      login(data)
      handleSetModalOpened('authorizationModal', false)
    }
  }

  const handleRegister = () => {
    const newErrors = validateAll(data)
    setErrors(newErrors)

    if (!hasErrors(newErrors)) {
      register({
        ...data,
        ...savedDataToSubmit,
        phone: `+7${data.phone}`,
      })
      handleSetModalOpened('registerSecondStepModal', false)
    }
  }

  const changeModal = (modal, savePreviousStep) => {
    if (savePreviousStep) {
      saveDataToSubmit({ ...data })
    } else {
      saveDataToSubmit({})
    }
    dispatch(closeAllModals())
    setTimeout(() => {
      handleSetModalOpened(modal)
    }, 0)
  }

  const handleShowAuthorizationModal = () => {
    changeModal('authorizationModal')
  }

  const handleShowPasswordRecoveryModal = () => {
    changeModal('passwordRecoveryFirstStepModal')
  }

  const handleShowRegisterFirstStepModal = () => {
    changeModal('registerFirstStepModal')
  }

  const handleShowRegisterSecondStepModal = () => {
    const newErrors = validateAll(data)
    setErrors(newErrors)

    if (!hasErrors(newErrors)) {
      changeModal('registerSecondStepModal', true)
    }
  }

  return (
    <>
      {Object.entries(modals).map(([modal, isOpened]) => {
        const { component, title, socials } = authModals[modal]
        if (!component) {
          return null
        }
        const Content = component
        return (
          isOpened && (
            <AuthModalTemplate
              key={modal}
              title={title}
              withSocials={socials}
              handleClose={() => handleSetModalOpened(modal, false)}
            >
              <Content
                handleChangeField={handleChangeField}
                actions={{
                  handleLogin,
                  handleRegister,
                  handleShowAuthorizationModal,
                  handleShowPasswordRecoveryModal,
                  handleShowRegisterFirstStepModal,
                  handleShowRegisterSecondStepModal,
                }}
              />
              <div className='modal-sign__errors'>
                {Object.values(errors).map((item, index) => (
                  <div key={index}>{item}</div>
                ))}
              </div>
            </AuthModalTemplate>
          )
        )
      })}
      {children}
    </>
  )
}
