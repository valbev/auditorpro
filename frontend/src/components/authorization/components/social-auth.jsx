import React, { useContext } from 'react'
import GoogleLogin from 'react-google-login'
import { Context } from '../../../context'

import fb from './images/fb.svg'
import google from './images/google.svg'
import linkedIn from './images/linkedin.svg'
import mail from './images/mail.svg'
import vk from './images/vk.svg'
import ya from './images/yandex.svg'

export const SocialAuth = ({ handleCloseModal }) => {
  const { loginWithSoc } = useContext(Context)
  return (
    <>
      <div className='modal-sign__social'>
        <div className='modal-sign__social-icon fb'>
          <img src={fb} alt='Fb' />
        </div>
        <div className='modal-sign__social-icon vk'>
          <img src={vk} alt='Vk' />
        </div>
        <GoogleLogin
          clientId='589270471984-lvja2c09vif54lqim16hiobpmhnu7htr.apps.googleusercontent.com'
          render={(renderProps) => (
            <div onClick={renderProps.onClick} className='modal-sign__social-icon google'>
              <img src={google} alt='G' />
            </div>
          )}
          onSuccess={(res) => {
            loginWithSoc({
              access_token: res.wc.access_token,
              email: res.profileObj.email,
              first_name: res.profileObj.givenName,
              last_name: res.profileObj.familyName,
              provider: 'google',
            })
            handleCloseModal()
          }}
          onFailure={(fail) => {
            console.log('Error while trying login/signup with google - ', fail)
          }}
          cookiePolicy='single_host_origin'
        />
        <div className='modal-sign__social-icon linkedin'>
          <img src={linkedIn} alt='LIn' />
        </div>
        <div className='modal-sign__social-icon yandex'>
          <img src={ya} alt='Ya' />
        </div>
        <div className='modal-sign__social-icon mail'>
          <img src={mail} alt='M' />
        </div>
      </div>
      <p className='modal-sign__or'>или</p>
    </>
  )
}
