import React from 'react'

export const AuthorizationModal = ({
  handleChangeField,
  actions: { handleLogin, handleShowPasswordRecoveryModal, handleShowRegisterFirstStepModal },
}) => (
  <>
    <div className='modal-sign__input-wrapper'>
      <label htmlFor='email'>E-mail</label>
      <input id='email' name='email' type='mail' placeholder='example@mail.ru' onChange={handleChangeField} />
    </div>
    <div className='modal-sign__input-wrapper mb-4'>
      <label htmlFor='password'>Пароль</label>
      <input id='password' name='password' type='password' placeholder='*********' onChange={handleChangeField} />
    </div>
    {/* <div className='d-flex justify-content-center mb-4'> */}
    {/*  <button */}
    {/*    className='modal-sign__forgot modal-sign__button_link' */}
    {/*    onClick={handleShowPasswordRecoveryModal} */}
    {/*  > */}
    {/*    Забыли пароль? */}
    {/*  </button> */}
    {/* </div> */}
    <button className='modal-sign__button modal-sign__button_sign-in mb-3' onClick={handleLogin}>
      Войти
    </button>
    <p className='modal-sign__text modal-sign__text_no-account'>
      У меня нет аккаунта.
      <button className='modal-sign__button_link' onClick={handleShowRegisterFirstStepModal}>
        Зарегистрироваться
      </button>
    </p>
  </>
)
