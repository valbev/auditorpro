import React, { useRef } from 'react'
import { Portal } from '../../../base-components'

import cross from './images/cross.svg'
import { SocialAuth } from './social-auth'

export const AuthModalTemplate = ({ handleClose, title, withSocials, children }) => {
  const innerRef = useRef()

  const handleClickOverlay = (target) => {
    if (!innerRef.current.contains(target)) {
      handleClose()
    }
  }

  return (
    <Portal onClickTarget={handleClickOverlay}>
      <div className='modal-sign'>
        <div className='modal-sign__inner' ref={innerRef}>
          <h2 className='modal-sign__title'>{title}</h2>
          {withSocials && <SocialAuth handleCloseModal={handleClose} />}
          <div className='modal-sign__form'>{children}</div>
          <button className='modal-sign__close' onClick={handleClose}>
            <img src={cross} alt='X' />
          </button>
        </div>
      </div>
    </Portal>
  )
}
