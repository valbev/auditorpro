import React from 'react'

export const RegisterFirstStepModal = ({
  handleChangeField,
  actions: { handleShowAuthorizationModal, handleShowRegisterSecondStepModal },
}) => {
  return (
    <>
      <div className='modal-sign__input-wrapper'>
        <label htmlFor='email'>E-mail</label>
        <input id='email' type='mail' name='email' placeholder='example@mail.ru' onChange={handleChangeField} />
      </div>
      <div className='modal-sign__input-wrapper'>
        <label htmlFor='password'>Пароль</label>
        <input id='password' type='password' name='password' placeholder='*********' onChange={handleChangeField} />
      </div>
      <div className='modal-sign__input-wrapper'>
        <label htmlFor=''>Подтвердите пароль</label>
        <input id='repassword' type='password' name='repassword' placeholder='*********' onChange={handleChangeField} />
      </div>
      <button className='modal-sign__button reg' onClick={handleShowRegisterSecondStepModal}>
        Продолжить регистрацию
      </button>
      <p className='modal-sign__text'>
        У меня уже есть аккаунт?
        <button className='modal-sign__button_link' onClick={handleShowAuthorizationModal}>
          Войти
        </button>
      </p>
    </>
  )
}
