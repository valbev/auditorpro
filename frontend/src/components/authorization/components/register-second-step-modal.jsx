import React from 'react'
import { format, subYears } from 'date-fns'

import flag from './images/flag.png'
import calendar from './images/calendar.svg'

export const RegisterSecondStepModal = ({ handleChangeField, actions: { handleRegister } }) => {
  const maxBirthDate = format(new Date(), 'yyyy-MM-dd')
  const minBirthDate = format(subYears(new Date(), 120), 'yyyy-MM-dd')

  return (
    <>
      <div className='modal-sign__input-wrapper'>
        <label htmlFor='first_name'>Имя</label>
        <input
          id='first_name'
          name='first_name'
          type='text'
          placeholder='Иван'
          onChange={handleChangeField}
          maxLength={64}
        />
      </div>
      <div className='modal-sign__input-wrapper'>
        <label htmlFor='last_name'>Фамилия</label>
        <input
          id='last_name'
          name='last_name'
          type='text'
          placeholder='Иванов'
          onChange={handleChangeField}
          maxLength={64}
        />
      </div>
      <div className='modal-sign__input-wrapper'>
        Дата рождения
        <label htmlFor='birthday' className='birthday-field'>
          <input
            id='birthday'
            name='birthday'
            type='date'
            min={minBirthDate}
            max={maxBirthDate}
            onChange={handleChangeField}
          />
          <img src={calendar} className='birthday-field__icon' alt='Выбрать дату рождения' />
        </label>
      </div>
      <div className='modal-sign__input-wrapper'>
        <label htmlFor='address'>Город проживания</label>
        <input
          id='address'
          name='address'
          type='text'
          placeholder='Москва'
          onChange={handleChangeField}
          maxLength={64}
        />
      </div>
      <div className='modal-sign__input-wrapper'>
        <label htmlFor='phone'>Номер телефона</label>
        <div className='modal-sign__input-group'>
          <div className='modal-sign__flag'>
            <img src={flag} alt='ru' />
            <span>+7</span>
          </div>
          <input
            id='phone'
            name='phone'
            type='tel'
            placeholder='923-374-23-**'
            onChange={handleChangeField}
            maxLength={10}
          />
        </div>
      </div>
      <div className='modal-sign__prove'>Телефон необходим для подтверждения аккаунта</div>
      <button className='modal-sign__button' onClick={handleRegister}>
        Завершить регистрацию
      </button>
      <div className='modal-sign__policy'>
        <p>Создавая аккаунт, вы соглашаетесь с нашими</p>
        <button className='modal-sign__button_link'>Правилами и условиями конфиденциальности</button>
      </div>
    </>
  )
}
