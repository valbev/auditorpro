import React from 'react'

export const PasswordRecoveryFirstStepModal = ({ handleChangeField, actions }) => {
  return (
    <>
      <div className='modal-sign__input-wrapper'>
        <label htmlFor='email'>Введите ваш E-mail</label>
        <input id='email' name='email' type='mail' placeholder='example@mail.ru' onChange={handleChangeField} />
      </div>
      <button className='modal-sign__button' onClick={() => {}}>
        Восстановить пароль
      </button>
    </>
  )
}
