import { format, isAfter, isBefore, subYears } from 'date-fns'

const emailCheckRegular = /\S+@\S+\.\S+/
const nameOrCityCheckRegular = /^[a-zA-Zа-яА-Я-]+$/

export const authValidation = {
  email: (value) => {
    const reg = emailCheckRegular

    if (!value) {
      return 'Введите email'
    } else if (!reg.test(value)) {
      return 'Введите корректный email'
    }
  },
  password: (value) => {
    if (!value) {
      return 'Введите пароль'
    }
  },
}

export const registerFirstStepValidation = {
  email: (value) => {
    const reg = emailCheckRegular

    if (!value) {
      return 'Введите email'
    } else if (!reg.test(value)) {
      return 'Введите корректный email'
    }
  },
  password: (value) => {
    if (!value) {
      return 'Введите пароль'
    } else if (value.length < 8) {
      return 'Пароль должен быть не меньше 8-и символов'
    }
  },
  repassword: (value, data) => {
    if (value !== data.password) {
      return 'Пароли не совпадают'
    }
  },
}

export const registerSecondStepValidation = {
  first_name: (value) => {
    const reg = nameOrCityCheckRegular

    if (!value) {
      return 'Введите имя'
    } else if (!reg.test(value)) {
      return 'Некорректное имя'
    }
  },
  last_name: (value) => {
    const reg = nameOrCityCheckRegular

    if (!value) {
      return 'Введите фамилию'
    } else if (!reg.test(value)) {
      return 'Некорректная фамилия'
    }
  },
  birthday: (value) => {
    const maxBirthDate = format(new Date(), 'yyyy-MM-dd')
    const minBirthDate = format(subYears(new Date(), 120), 'yyyy-MM-dd')

    if (!value) {
      return 'Заполните дату рождения'
    } else if (isBefore(new Date(value), new Date(minBirthDate)) || isAfter(new Date(value), new Date(maxBirthDate))) {
      return 'Некорректная дата рождения'
    }
  },
  address: (value) => {
    const reg = nameOrCityCheckRegular

    if (!value) {
      return 'Введите город'
    } else if (!reg.test(value)) {
      return 'Некорректный город'
    }
  },
  phone: (value) => {
    const reg = /^\d{10}$/

    if (!value) {
      return 'Введите телефон'
    } else if (!reg.test(value)) {
      return 'Некорректный телефон'
    }
  },
}
