import { createReducer } from 'reduxsauce'
import { Types } from './actions'

const INITIAL_STATE = {
  authorizationModal: false,
  registerFirstStepModal: false,
  registerSecondStepModal: false,
  passwordRecoveryFirstStepModal: false,
  passwordRecoverySecondStepModal: false,
  passwordRecoverySuccessModal: false,
}

const toggleModalOpened = (state = INITIAL_STATE, action) => {
  const { modal, isOpened } = action
  return {
    ...state,
    [modal]: isOpened,
  }
}

const closeAllModals = (state = INITIAL_STATE) => {
  const closed = Object.entries(state).reduce((acc, [key, value]) => {
    if (value) {
      acc[key] = !value
    }
    return acc
  }, {})

  return {
    ...state,
    ...closed,
  }
}

export const HANDLERS = {
  [Types.TOGGLE_MODAL_OPENED]: toggleModalOpened,
  [Types.CLOSE_ALL_MODALS]: closeAllModals,
}

export const authorizationReducer = createReducer(INITIAL_STATE, HANDLERS)
