import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  toggleModalOpened: ['modal', 'isOpened'],
  closeAllModals: [],
})

export { Types, Creators as Actions }
