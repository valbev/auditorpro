import React from 'react'
import { Container } from 'reactstrap'
import './content.scss'

export const Content = (props) => (
  <div className='content' style={props.style}>
    <Container className='main-container-style' fluid>
      {props.children}
    </Container>
  </div>
)
