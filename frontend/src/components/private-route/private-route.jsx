import React, { useContext } from 'react'
import { Route, Redirect } from 'react-router-dom'

import { Context } from '../../context'

export const PrivateRoute = ({ component: Component, ...rest }) => {
  const { user } = useContext(Context)

  const renderComponent = (props) =>
    user && user.isLoggedIn ? <Component {...props} /> : <Redirect to='/unauthorized' />

  return <Route {...rest} render={renderComponent} />
}
