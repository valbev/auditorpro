import React, { Component } from 'react'
import { Button } from '../../../assets/components/button'
import { CheckListBody } from './check-lists-tabs-body'
import './check-lists.scss'
import { Link, Redirect } from 'react-router-dom'
import { withAuth, withGsapiService } from '../../hoc'
import { connect } from 'react-redux'
import { Actions } from '../constructor/redux/actions'
import { Loader } from '../../../assets/components/loader'

@withGsapiService()
@withAuth()
class CheckLists extends Component {
  state = {
    tableData: null,
    table: null,
    redirect: false,
    editedPoll: null,
    loading: true,
  }

  async componentDidMount() {
    try {
      localStorage.setItem('id_image_poll', null)
      localStorage.setItem('id_full_poll', null)
      localStorage.setItem('poll_type', null)
      await this.getPollsFromApi()
      await this.props.toggleConstructorId(null, null)
      this.setState({
        loading: false,
      })
    } catch (error) {}
  }

  buildTableData = async () => {
    const { getSecretService } = this.props
    const response = await getSecretService.fetchData({ action: 'getPolls', id: '0', slug: 'my' })
    const fullPolls = await getSecretService.fetchData({ action: 'getFullConstructorPolls', id: 'no', slug: 'my' })
    const tableDataFirst = response.result.map((item, index) => {
      return {
        id: item.image_poll_id,
        name: `${item.title}`,
        type: item.type_poll,
        author: `${item.author}`,
        answers: `${item.count_completed}`,
      }
    })

    const tableDataSecond = fullPolls.result.map((poll) => {
      return {
        id: poll.poll_id,
        name: poll.title.length ? poll.title : 'no name',
        type: 'full_poll',
        author: poll.author,
        answers: '0',
      }
    })

    const dataArray = tableDataFirst.concat(tableDataSecond)

    this.setState({
      tableData: dataArray,
    })
  }

  getPollsFromApi = async () => {
    await this.buildTableData()
    const { tableData } = this.state
    const tableObject = {
      head: [
        { title: 'name', settings: {}, name: 'Название' },
        { title: 'author', settings: {}, name: 'Автор' },
        { title: 'answers', settings: {}, name: 'Ответов' },
      ],
      data: tableData,
      settings: {
        tableRowButtons: {
          edit: true,
          link: true,
          copy: true,
          delete: true,
        },
      },
    }
    this.setState({
      table: tableObject,
    })
  }

  tableActionHandler = (data, id, action, type) => {
    // Handler depends on action type (delete, copy and ect.)
    // Data - current data array of the table
    // Index - array index
    // Action - Button type
    switch (action) {
      case 'delete':
        return this.handleDeletePoll(id, type)
      case 'edit':
        return this.handleEditPoll(id, type)
      default:
        break
    }
  }

  handleDeletePoll = async (id, type) => {
    const { getSecretService } = this.props
    const { tableData } = this.state
    try {
      if (type === 'full_poll') {
        await getSecretService.fetchData({ action: 'deleteFullPoll', id })
      } else {
        await getSecretService.fetchData({ action: 'deleteImagePoll', id })
      }
      const newItems = tableData.filter((item) => item.id !== id)
      this.setState({
        tableData: newItems,
      })
    } catch (error) {
      return error
    }
  }

  handleEditPoll = (id, type) => {
    this.props.toggleConstructorId(id, type)
    localStorage.setItem('poll_type', type)
    this.setState({
      redirect: true,
      editedPoll: id,
    })
  }

  renderRedirect = () => {
    const { redirect, editedPoll } = this.state
    if (redirect) {
      return <Redirect to={`/constructor/${editedPoll}`} />
    }
  }

  render() {
    const { table, tableData, loading } = this.state
    return (
      <div>
        {loading ? (
          <Loader />
        ) : (
          <div>
            {this.renderRedirect()}
            <div className='check-lists-page-title'> Чек листы </div>
            <div>
              <Link to='/constructor'>
                <Button type='primary' className='mb-3'>
                  Создать чек лист
                </Button>
              </Link>
              {table && (
                <CheckListBody table={table} data={tableData} tableBtnActionHandler={this.tableActionHandler} />
              )}
            </div>
          </div>
        )}
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    toggleConstructorId: (id, constructorType) => dispatch(Actions.toggleConstructorId(id, constructorType)),
  }
}

export default connect(null, mapDispatchToProps)(CheckLists)
