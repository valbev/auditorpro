import React from 'react'
import { Tabs, Table } from '../../../assets/components'

export const CheckListBody = ({ table, data, tableBtnActionHandler }) => {
  const createCheckListData = () => {
    return [
      {
        title: 'Активные',
        content: <Table table={table} data={data} tableBtnActionHandler={tableBtnActionHandler} />,
      },
      {
        title: 'Удалено',
        content: <></>,
      },
    ]
  }

  return <Tabs tabList={createCheckListData()} />
}
