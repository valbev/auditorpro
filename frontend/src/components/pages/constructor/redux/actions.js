import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  toggleConstructorId: ['id', 'constructorType'],
})

export { Types, Creators as Actions }
