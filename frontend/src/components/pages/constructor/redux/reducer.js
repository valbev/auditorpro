import { createReducer } from 'reduxsauce'
import { Types } from './actions'

const INITIAL_STATE = {
  constructorType: null,
  constructorId: null,
}

const toggleConstructorId = (state = INITIAL_STATE, { id, constructorType }) => {
  return {
    ...state,
    constructorId: id,
    constructorType,
  }
}

export const HANDLERS = {
  [Types.TOGGLE_CONSTRUCTOR_ID]: toggleConstructorId,
}

export const constructorReducer = createReducer(INITIAL_STATE, HANDLERS)
