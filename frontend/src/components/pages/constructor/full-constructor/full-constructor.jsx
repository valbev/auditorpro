import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import { CheckListBody } from '../check-list-body'
import { SettingsPanel } from '../parts/settings-panel/settings-panel'
import { PreviewPanel } from '../parts/preview-panel'
import { ImageUploadDialog } from '../components/image-upload-dialog'

import { SERVER } from '../../../global-constants'
import { cardTypes, serverCardTypes, serverSideTypes } from '../constants/index'

import { withGsapiService } from '../../../hoc'
import { Actions } from '../redux/actions'
import debounce from 'lodash/debounce'

@withGsapiService()
@withRouter
class FullConstructor extends Component {
  constructor(props) {
    super(props)
    this.state = {
      constructorTitle: '',
      items: [],
      showType: 0,
      currentSettings: null,
      settings: {
        multiple: {
          testMode: false,
          required: false,
          description: false,
          description_mode: false,
          multipleAnswersSelect: true,
          multipleAnswers: false,
          answerFrom: '',
          answerTo: '',
          currentNumber: false,
          currentNumberValue: '',
          selectedCount: 0,
          title_image: '',
          // dynamicAnswer: false,
          mixAnswers: false,
          timeForAnswer: false,
          timeMinute: '',
          timeSeconds: '',
          answerTime: 0,
          photoAttach: true,
          // comments: true,
          showedType: [0, 1, 2, 3],
          countOfAnswer: ['Неограчинено', 'Точное число', 'Диапазон'],
          countOfAnswerValue: 0,
          showType: null,
          stretchImage: false,
        },
        section: {
          description: false,
        },
        oneFromList: {
          required: false,
          description: true,
          points: true,
          pointsValue: 0,
          mixAnswers: false,
          photoAttach: true,
          comments: true,
        },
        fileUpload: {
          points: true,
          pointsValue: 0,
          fileType: true,
          audio: true,
          photoVideo: false,
          required: false,
          description: true,
          photoAttach: true,
          fileTypeUpload: true,
          isAudio: true,
          comments: true,
        },
        text: {
          required: false,
          description: false,
          onlyNumberToggler: true,
          onlyNumber: false,
          textCountToggler: true,
          maxTextLengthFlag: true,
          maxTextLength: 100,
          photoAttach: true,
          comments: true,
        },
        yesNo: {
          required: true,
          description: true,
          photoAttach: true,
          comments: true,
        },
        rating: {
          points: 0,
          scale: [10, 20, 100],
          form: ['Звезда'],
          required: true,
          description: true,
          photoAttach: true,
          comments: true,
        },
      },
      fullConstructorFormat: this.props.isFullConstructor,
      imageUploadModal: false,
      currentImageCardModalIndex: null,
    }
    this.onInputChangeApi = debounce(this.onInputChangeApi, 500)
    this.onApiTextInputChange = debounce(this.onApiTextInputChange, 500)
  }

  componentDidMount() {
    const { fullConstructorData } = this.props
    console.log('fullConstructorData ========>>>>', fullConstructorData)
    if (fullConstructorData.questions && fullConstructorData.questions.length) {
      const pollQuestions = fullConstructorData.questions.map((question) => {
        const content = question.items
          ? question.items.map((input) => {
              return {
                id: input.item_question_id,
                order_id: input.order_id,
                check: input.checked,
                title: input.text,
                points: input.points,
                photoPath: input.photo_path,
                focus: false,
                image: true,
                closeBtn: true,
              }
            })
          : []
        const minute = Math.floor(question.answer_time / 60)
        const second = question.answer_time - minute * 60
        const settings = Object.assign({}, this.state.settings[serverSideTypes[question.question_type]])
        settings.required = question.require
        settings.mixAnswers = question.mix_answers
        settings.timeForAnswer = question.time_for_answer
        settings.answerTime = question.answer_time
        settings.showType = question.type_for_show === 0 ? null : question.type_for_show
        settings.stretchImage = question.resize_image
        settings.testMode = fullConstructorData.test_mode_global
        settings.timeMinute = minute
        settings.timeSeconds = second
        settings.description_mode = question.description_mode
        settings.countOfAnswerValue = question.count_of_answer ? question.count_of_answer : 0
        settings.currentNumberValue = question.current_number_value ? question.current_number_value : ''
        settings.answerFrom = question.answer_from ? question.answer_from : ''
        settings.answerTo = question.answer_to ? question.answer_to : ''

        return {
          id: question.question_id,
          order_id: question.order_id,
          title_image: question.title_image,
          type: serverSideTypes[question.question_type],
          // file: item.image !== '' ? item.image : null,
          content: {
            inputs: content,
          },
          settings,
          title: question.caption,
          description: question.description,
        }
      })
      this.setState({
        items: pollQuestions,
        currentCardIndex: null,
        settingsOpened: true,
        constructorTitle: fullConstructorData.title,
      })
    } else {
      this.setState({
        items: [],
        currentCardIndex: null,
        settingsOpened: true,
      })
    }
  }

  settingsPanelToggler = (currentCardId) => {
    const { items } = this.state
    const cardId = items.find((currentItem) => currentItem.id === currentCardId)
    const newItems = items.map((item) => {
      if (item.id === currentCardId) {
        item.active = true
      } else {
        item.active = false
      }
      return item
    })
    this.setState({
      currentCardIndex: cardId.id,
      settingsOpened: true,
      items: newItems,
      currentSettings: items.find((item) => item.id === currentCardId),
    })
  }

  reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list)
    const [removed] = result.splice(startIndex, 1)
    result.splice(endIndex, 0, removed)
    return result
  }

  onCardChange = (result) => {
    if (!result.destination) {
      return
    }
    const item = this.reorder(this.state.items, result.source.index, result.destination.index)

    this.setState({
      items: item,
    })
  }

  onInputChange = (result, id) => {
    if (!result.destination) {
      return
    }
    const currentCardInput = this.state.items.find((item) => item.id === this.state.currentCardIndex).content.inputs
    const currentIndex = this.state.items.findIndex((item) => item.id === this.state.currentCardIndex)
    const inputs = this.reorder(currentCardInput, result.source.index, result.destination.index)

    const newValue = [...this.state.items]
    newValue[currentIndex].content.inputs = inputs

    this.setState({
      items: newValue,
    })
  }

  contentBuilder(card) {
    switch (card.type) {
      case cardTypes.oneFromList:
        card.content = {
          inputs: [
            {
              id: 0,
              check: false,
              points: 2,
              image: true,
              title: '',
              closeBtn: true,
            },
          ],
        }
        break
      default:
        break
    }
  }

  createCard = async (type) => {
    const { toggleConstructorType, location, getSecretService } = this.props
    const { items } = this.state
    await toggleConstructorType(true, items)
    const pollId = location.pathname.split('/')[location.pathname.split('/').length - 1]
    const id = pollId !== 'constructor' ? pollId : this.props.constructorId
    const params = {
      order_id: items.length + 1,
      caption: '',
      description: '',
      comment: '',
      require: false,
      mix_answers: false,
      time_for_answer: false,
      answer_time: 0,
      type_for_show: 0,
      title_image: '',
      resize_image: false,
      test_mode: false,
      description_mode: false,
      count_of_answer: 0,
      current_number_value: null,
      answer_from: null,
      answer_to: null,
      question_type: serverCardTypes[type],
      items: [
        {
          order_id: 1,
          text: '',
          checked: true,
          photo_path: '',
          points: 0,
        },
      ],
    }
    this.props.toggleConstructorId(this.props.constructorId, 'full_poll')
    try {
      const question = await getSecretService.fetchData({ action: 'createPollQuestion', id, routeType: true }, params)
      if (question && question.result) {
        const settings = Object.assign({}, this.state.settings[serverSideTypes[question.result.question_type]])
        const content = question.result.items
          ? question.result.items.map((input) => {
              return {
                id: input.item_question_id ? input.item_question_id : 0,
                order_id: input.order_id,
                check: input.checked,
                focus: false,
                points: 2,
                image: true,
                photoPath: input.photo_path,
                title: input.text,
                closeBtn: true,
              }
            })
          : []

        const card = {
          id: question.result.question_id,
          order_id: question.result.order_id,
          type: serverSideTypes[question.result.question_type],
          title_image: question.result.title_image,
          settings,
          // hover: false,
          active: false,
          content: {
            inputs: content,
          },
          title: question.result.caption ? question.result.caption : '',
          description: question.result.description ? question.result.description : '',
        }
        const newValue = [...items]
        newValue.push(card)

        this.setState({
          items: newValue,
        })
      }
    } catch (err) {
      this.props.toggleConstructorId(null, null)
    }
  }

  handleHoverCard = (value, card) => {
    const { items } = this.state
    const newItems = items.map((item) => {
      if (item.id === card.id) {
        item.hover = value
      }
      return item
    })
    this.setState({
      items: newItems,
    })
  }

  addNewTextInput = async (item, inputId) => {
    const { getSecretService } = this.props
    const { id } = item

    // const params = {
    //   order_id: item.content.inputs.length + 1,
    //   text: '',
    //   checked: true,
    //   photo_path: '',
    //   points: 0,
    // }

    const data = {
      order_id: item.content.inputs.length + 1,
      text: 'test',
      checked: true,
      photo_path: 'test',
      points: 1,
    }

    const inputs = await getSecretService.fetchData({ action: 'createQuestionPoll', id }, data)

    let input = {
      id: item.content.inputs.length,
      order_id: item.content.inputs.length,
      check: false,
      focus: false,
      points: 2,
      image: true,
      title: '',
      closeBtn: true,
    }

    if (inputs.result) {
      input = {
        id: inputs.result.item_question_id,
        order_id: inputs.result.order_id,
        check: inputs.result.checked,
        focus: false,
        points: inputs.result.points,
        image: true,
        photoPath: inputs.result.photo_path === 'test' ? '' : inputs.result.photo_path,
        title: inputs.result.text === 'test' ? '' : inputs.result.text,
        closeBtn: true,
      }
      const { items } = this.state
      const newValue = [...items]
      const currentCardIndex = newValue.findIndex((card) => card.id === item.id)
      const currentInputIndex = newValue[currentCardIndex].content.inputs.findIndex((input) => input.id === inputId)
      // const currentInput = items[currentCardIndex].content.inputs[currentInputIndex]
      await this.onApiTextInputChange(input.id, input)
      newValue[currentCardIndex].content.inputs.map((item) => {
        item.focus = false
      })
      newValue[currentCardIndex].content.inputs.splice(currentInputIndex + 1, 0, input)
      newValue[currentCardIndex].content.inputs[currentInputIndex + 1].focus = true
      this.setState({
        items: newValue,
      })
    }
  }

  constructorTypeToggler = (value) => {
    this.props.constructorTypeToggler(value)
  }

  deleteCard = (e, id) => {
    e.stopPropagation()
    try {
      const { items, currentCardIndex } = this.state
      const { getSecretService, constructorId } = this.props
      const { type } = items.find((card) => card.id === id)
      const newItems = [...items]
      const currentDeletedIndex = items.findIndex((card) => card.id === id)

      const value = newItems.filter((card) => card.id !== id)
      const cardId =
        items[currentDeletedIndex].id !== currentCardIndex
          ? currentCardIndex
          : newItems.length <= 0
          ? null
          : currentDeletedIndex === 0
          ? items[currentDeletedIndex + 1].id
          : items[currentDeletedIndex - 1].id
      if (newItems.length > 0) {
        if (currentDeletedIndex === 0 && items[currentDeletedIndex].active === true) {
          items[currentDeletedIndex + 1].active = true
        } else if (items[currentDeletedIndex].active === true) {
          items[currentDeletedIndex - 1].active = true
        }
      }

      this.setState(
        {
          items: value,
          currentCardIndex: cardId,
          settingsOpened: newItems.length !== 0,
          currentSettings:
            newItems.length > 0
              ? currentDeletedIndex === 0
                ? items[currentDeletedIndex + 1]
                : items[currentDeletedIndex - 1]
              : null,
        },
        async () => {
          await getSecretService.fetchData({ action: 'deleteFullPollQuestion', id, slug: serverCardTypes[type] })
          if (value.length === 0) {
            await getSecretService.fetchData({ action: 'deleteFullPoll', id: constructorId })
          }
        },
      )
    } catch (err) {
      return err
    }
  }

  handleCloseSettings = () => {
    const { items } = this.state
    const newItems = items.map((item) => {
      item.active = false
      return item
    })
    this.setState({
      settingsOpened: false,
      items: newItems,
    })
  }

  copyCard = (id) => {
    const { items } = this.state
    const currentItemIndex = items.findIndex((card) => card.id === id)
    const newArray = [...items]
    newArray.splice(currentItemIndex, 0, Object.assign({}, newArray[currentItemIndex]))
    newArray[currentItemIndex + 1].id = newArray.length + 1
    newArray[currentItemIndex + 1].active = false
    this.setState({
      items: newArray,
    })
  }

  cardTypeSelectHandler = (index, type) => {
    const item = this.state.items[index]
    item.type = type
    item.settings = this.state.settings[type]
    // this.contentBuilder(item);
    this.setState({
      items: this.state.items,
    })
  }

  handleChangeConstructorType = (type) => {
    this.setState({
      isFullConstructor: type,
    })
  }

  handleChangeCountAnswer = (selectedItem, name) => {
    const { currentCardIndex, items } = this.state
    const newItems = [...items]
    const currentCard = newItems.find((item) => item.id === currentCardIndex)
    const setting = currentCard.settings
    switch (selectedItem) {
      case 0: {
        setting.multipleAnswers = false
        setting.currentNumber = false
        break
      }
      case 1: {
        setting.multipleAnswers = false
        setting.currentNumber = true
        break
      }
      case 2: {
        setting.multipleAnswers = true
        setting.currentNumber = false
        break
      }
    }
    this.setState({
      items: newItems,
    })
  }

  handleChangeShowType = (index) => {
    const { currentCardIndex, items } = this.state
    const item = items.find((item) => item.id === currentCardIndex)
    const setting = item.settings
    setting.showType = index
    this.contentBuilder(item)
    this.setState(
      {
        showType: index,
        items,
      },
      () => this.apiEditQuestion(currentCardIndex, items),
    )
  }

  tooglerActionHandler = (id, property, all) => {
    const { items } = this.state
    const { fullConstructorData, getSecretService } = this.props
    if (all) {
      const allItems = items.map((item) => {
        const allSettings = item.settings
        allSettings[property] = !allSettings[property]
        return item
      })
      this.setState({
        items: allItems,
      })
      const params = {
        title: fullConstructorData.title,
        test_mode_global: !fullConstructorData.test_mode_global,
      }
      const id = fullConstructorData.poll_id
      getSecretService.fetchData({ action: 'updateFullPoll', id }, params)
    } else {
      const item = items.find((item) => item.id === id)
      const { settings } = item
      settings[property] = !settings[property]
      this.setState(
        {
          items,
        },
        () => this.apiEditQuestion(id, items),
      )
    }
  }

  apiEditQuestion = async (id, items) => {
    const { getSecretService } = this.props
    const newItems = [...items]
    const currentItem = newItems.find((item) => item.id === id)
    const setting = currentItem.settings
    const serverItems = currentItem.content.inputs.map((input) => {
      return {
        order_id: input.order_id,
        checked: input.check,
        points: 2,
        photo_path: input.photoPath,
        text: input.title,
      }
    })

    const data = {
      order_id: currentItem.order_id,
      caption: currentItem.title,
      description: currentItem.description,
      comment: 'pew-pew',
      require: setting.required,
      description_mode: setting.description_mode,
      mix_answers: setting.mixAnswers,
      time_for_answer: setting.timeForAnswer,
      answer_time: setting.answerTime,
      answer_to: typeof setting.answerTo === 'string' ? 0 : setting.answerTo,
      answer_from: typeof setting.answerFrom === 'string' ? 0 : setting.answerFrom,
      current_number_value: typeof setting.currentNumberValue === 'string' ? 0 : setting.currentNumberValue,
      type_for_show: setting.showType === null ? 0 : setting.showType,
      title_image: currentItem.title_image,
      resize_image: setting.stretchImage,
      test_mode: setting.testMode,
      question_type: serverCardTypes[currentItem.type],
      items: serverItems,
    }

    console.log('DATA =========+>>>>>', data)
    const response = await getSecretService.fetchData(
      { action: 'editFullQuestion', id, slug: serverCardTypes[currentItem.type] },
      data,
    )
    console.log('RESPONSE =============+>>>>', response)
  }

  onInputChangeApi = (cardId, newItems) => {
    this.apiEditQuestion(cardId, newItems)
  }

  handleChangeSettingInput = (e, cardId, mode) => {
    const { items } = this.state
    const newItems = [...items]
    if (mode === '60') {
      const itemIndex = newItems.findIndex((item) => item.id === cardId)
      const currentItem = newItems[itemIndex]
      const settings = Object.assign({}, currentItem.settings)
      let { value } = e.target
      if (!/^[0-9]+$/.test(value)) {
        value = ''
      }
      settings.answerTime = Number(value) * 60 + Number(settings.timeSeconds)
      settings[e.target.name] = value
      newItems[itemIndex].settings = settings
      this.setState(
        {
          items: newItems,
        },
        () => this.onInputChangeApi(cardId, newItems),
      )
    } else if (mode === '59') {
      const itemIndex = newItems.findIndex((item) => item.id === cardId)
      const currentItem = newItems[itemIndex]
      const settings = Object.assign({}, currentItem.settings)
      let { value } = e.target
      if (!/^[0-9]+$/.test(value)) {
        value = ''
      } else if (Number(value) > 59) {
        value = 59
      }
      settings[e.target.name] = value
      settings.answerTime = Number(settings.timeMinute) * 60 + Number(value)
      newItems[itemIndex].settings = settings
      this.setState(
        {
          items: newItems,
        },
        () => this.onInputChangeApi(cardId, newItems),
      )
    } else {
      const { items } = this.state
      const item = items.find((item) => item.id === cardId)
      const { settings } = item
      settings[e.target.name] = Number(e.target.value)
      this.setState(
        {
          items,
        },
        () => this.onInputChangeApi(cardId, items),
      )
    }
  }

  changeCardInput = (event, cardId) => {
    console.log('event.target.name =======>>>>>', event.target.name, event.target.value)
    const { items } = this.state
    const newValue = [...items]
    const newCardData = newValue.map((card) => {
      if (card.id === cardId) {
        card[event.target.name] = event.target.value
      } else {
        return card
      }
      return card
    })
    this.setState(
      {
        items: newCardData,
      },
      () => this.onInputChangeApi(cardId, items),
    )
  }

  handleInputChange = (event, cardId, inputId) => {
    const { items } = this.state
    const newValue = [...items]
    const currentCardIndex = newValue.findIndex((card) => card.id === cardId)
    const currentInputIndex = newValue[currentCardIndex].content.inputs.findIndex((input) => input.id === inputId)
    newValue[currentCardIndex].content.inputs[currentInputIndex].title = event.target.value
    this.setState(
      {
        items: newValue,
      },
      () => {
        const currentInput = items[currentCardIndex].content.inputs[currentInputIndex]
        // const currentCardId = items[currentCardIndex].id
        this.onApiTextInputChange(currentInput.id, currentInput)
      },
    )
  }

  deleteAnswerInputImage = (cardId, inputId) => {
    const { items } = this.state
    const newValue = [...items]
    const currentCardIndex = newValue.findIndex((card) => card.id === cardId)
    if (inputId) {
      const currentInputIndex = newValue[currentCardIndex].content.inputs.findIndex((input) => input.id === inputId)
      newValue[currentCardIndex].content.inputs[currentInputIndex].photoPath = ''
      this.setState(
        {
          items: newValue,
        },
        async () => {
          const currentInput = items[currentCardIndex].content.inputs[currentInputIndex]
          await this.onApiTextInputChange(currentInput.id, currentInput)
        },
      )
    } else {
      newValue[currentCardIndex].title_image = ''
      this.setState(
        {
          items: newValue,
        },
        () => this.onInputChangeApi(cardId, items),
      )
    }
  }

  onApiTextInputChange = async (id, currentInput) => {
    const { getSecretService } = this.props
    const data = {
      order_id: currentInput.order_id,
      text: currentInput.title,
      checked: currentInput.checked,
      photo_path: currentInput.photoPath,
      points: currentInput.points,
    }
    await getSecretService.fetchData({ action: 'editQuestionPoll', id }, data)
  }

  deleteAnswerInput = (cardId, inputId) => {
    const { items } = this.state
    const { getSecretService } = this.props
    const newValue = [...items]

    const filteredInputs = newValue.map((item) => {
      if (item.id === cardId) {
        if (item.content.inputs.length !== 1) {
          item.content.inputs = item.content.inputs.filter((input) => input.id !== inputId)
        } else {
          return item
        }
      }
      return item
    })

    this.setState(
      {
        items: filteredInputs,
      },
      async () => {
        const id = inputId
        await getSecretService.fetchData({ action: 'deleteAnswerFullPoll', id })
      },
    )
  }

  sendPhoto = async (file, cardId, inputId) => {
    const { getSecretService } = this.props
    const { items } = this.state
    const id = cardId
    const formData = new FormData()
    const newItems = [...items]
    formData.append('photo', file)
    try {
      const res = await getSecretService.uploadImage({ action: 'uploadImage', id }, formData)
      const currentCardIndex = newItems.findIndex((card) => card.id === cardId)
      const currentInputIndex = newItems[currentCardIndex].content.inputs.findIndex((input) => input.id === inputId)
      if (!inputId) {
        const modifiedItem = newItems.map((item) => {
          if (item.id === id) {
            item.title_image = `${SERVER}/${res.path}`
          }
          return item
        })
        this.setState(
          {
            items: modifiedItem,
          },
          () => this.onInputChangeApi(cardId, items),
        )
      } else {
        const currentInput = items[currentCardIndex].content.inputs[currentInputIndex]
        currentInput.photoPath = `${SERVER}/${res.path}`
        this.setState({
          showType: 9,
        })
        await this.onApiTextInputChange(inputId, currentInput)
      }
    } catch {
      const modifiedItem = newItems.map((item) => {
        if (item.id === id) {
          item.title_image = ''
        }
        return item
      })

      this.setState({
        items: modifiedItem,
      })
    }
  }

  handleImageUploadModal = (e, cardId, rowId) => {
    e.preventDefault()
    const { imageUploadModal } = this.state
    this.setState({
      imageUploadModal: !imageUploadModal,
      currentCardIndex: cardId,
      currentInputId: rowId,
    })
  }

  handleModalUpload = async (file) => {
    const { items, currentCardIndex, currentInputId } = this.state
    const newItems = [...items]
    const currentIndex = newItems.findIndex((card) => card.id === currentCardIndex)
    await this.sendPhoto(file, newItems[currentIndex].id, currentInputId)
    this.setState({
      imageUploadModal: false,
      currentInputId: null,
    })
  }

  render() {
    const {
      items,
      settingsOpened,
      constructorTitle,
      currentCardIndex,
      fullConstructorFormat,
      currentSettings,
      imageUploadModal,
      showType,
    } = this.state

    const { toggleConstructorType, isFullConstructor } = this.props

    return (
      <div>
        <ImageUploadDialog
          modal={imageUploadModal}
          handleImageUploadModal={this.handleImageUploadModal}
          handleModalUpload={this.handleModalUpload}
        />
        <div className='d-flex left-panel'>
          <div className={settingsOpened ? 'opened-left-panel left-panel' : 'left-panel'}>
            {settingsOpened ? (
              <SettingsPanel
                handleChangeSettingInput={this.handleChangeSettingInput}
                handleCloseSettings={this.handleCloseSettings}
                handleChangeCountAnswer={this.handleChangeCountAnswer}
                handleChangeShowType={this.handleChangeShowType}
                tooglerActionHandler={this.tooglerActionHandler}
                constructorTypeToggler={this.constructorTypeToggler}
                settings={currentSettings ? currentSettings.settings : {}}
                index={currentCardIndex}
                fullConstructorFormat={fullConstructorFormat}
                type={currentSettings ? currentSettings.type : null}
                handleChangeConstructorType={this.handleChangeConstructorType}
                toggleConstructorType={toggleConstructorType}
                isFullConstructor={isFullConstructor}
                deleteAnswerInput={this.deleteAnswerInput}
                deleteAnswerInputImage={this.deleteAnswerInputImage}
              />
            ) : (
              <></>
            )}
          </div>
          <div
            id='style-4'
            className='scrollbar'
            style={{
              width: settingsOpened ? '60%' : '75%',
              backgroundColor: '#EDEEF4',
            }}
          >
            <div className='card-body'>
              <CheckListBody
                handleChangeSettingInput={this.handleChangeSettingInput}
                deleteAnswerInputImage={this.deleteAnswerInputImage}
                changeCardInput={this.changeCardInput}
                deleteAnswerInput={this.deleteAnswerInput}
                handleImageUploadModal={this.handleImageUploadModal}
                handleInputChange={this.handleInputChange}
                handleHoverCard={this.handleHoverCard}
                cards={items}
                cardTypeSelectHandler={this.cardTypeSelectHandler}
                onCardChange={this.onCardChange}
                onInputChange={this.onInputChange}
                settingsToggler={this.settingsPanelToggler}
                addNewTextInput={this.addNewTextInput}
                deleteCard={this.deleteCard}
                isFullConstructor={isFullConstructor}
                copyCard={this.copyCard}
                createCardProps={this.createCard}
              />
            </div>
          </div>
          <PreviewPanel
            constructorTitle={constructorTitle}
            showType={showType}
            items={items}
            isFullConstructor={isFullConstructor}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    constructorId: state.constructor.constructorId,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    toggleConstructorId: (id, constructorType) => dispatch(Actions.toggleConstructorId(id, constructorType)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FullConstructor)
