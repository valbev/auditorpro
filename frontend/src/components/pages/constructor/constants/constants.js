export const cardTypes = {
  multiple: 'multiple',
  text: 'text',
  section: 'section',
  // oneFromList: 'oneFromList',
  fileUpload: 'fileUpload',
  yesNo: 'yesNo',
  rating: 'rating',
  imageCard: 'imageCard',
}

export const serverCardTypes = {
  multiple: 'ManyFromListQuestion',
  text: 'text',
  section: 'section',
  // oneFromList: 'oneFromList',
  fileUpload: 'fileUpload',
  yesNo: 'yesNo',
  rating: 'rating',
  imageCard: 'imageCard',
}

export const serverSideTypes = {
  ManyFromListQuestion: 'multiple',
  text: 'text',
  section: 'section',
  // oneFromList: 'oneFromList',
  fileUpload: 'fileUpload',
  yesNo: 'yesNo',
  rating: 'rating',
  imageCard: 'imageCard',
}

export const constructorPlaceholders = {
  multiple: {
    header: 'Заголовок интерактивного опроса',
    description: 'Описание интерактивного опроса',
    answer: 'Текст вопроса',
  },
  text: {
    header: 'Заголовок интерактивного опроса',
    description: 'Описание интерактивного опроса',
  },
  section: {
    header: 'Название раздела',
    description: 'Какое-то описание разела, для того чтобы было',
  },
  oneFromList: {
    header: 'Заголовок интерактивного опроса',
    description: 'Описание интерактивного опроса',
    answer: 'Текст вопроса',
  },
  fileUpload: {
    header: 'Заголовок интерактивного опроса',
    description: 'Прикрепите фото из галереи или сделайте снимок',
  },
  yesNo: {
    header: 'Заголовок интерактивного опроса',
    description: 'Описание интерактивного опроса',
  },
  rating: {
    header: 'Заголовок интерактивного опроса',
    description: 'Описание интерактивного опроса',
  },
  imageCard: {
    header: 'Заголовок интерактивного опроса',
    description: 'Описание интерактивного опроса',
  },
}

export const settingsTypes = {
  required: 'required',
  description_mode: 'description_mode',
  testMode: 'testMode',
  description: 'description',
  answers: 'answers',
  points: 'points',
  pointsValue: 'pointsValue',
  fileType: 'fileType',
  audio: 'audio',
  photoVideo: 'photoVideo',
  onlyNumberToggler: 'onlyNumberToggler',
  onlyNumber: 'onlyNumber',
  textCountToggler: 'textCountToggler',
  maxTextLengthFlag: 'maxTextLengthFlag',
  maxTextLength: 'maxTextLength',
  multipleAnswersSelect: 'multipleAnswersSelect',
  multipleAnswers: 'multipleAnswers',
  currentNumber: 'currentNumber',
  from: 'from',
  to: 'to',
  photoAttach: 'photoAttach',
  comments: 'comments',
  showedType: 'showedType',
  dynamicAnswer: 'dynamicAnswer',
  mixAnswers: 'mixAnswers',
  timeForAnswer: 'timeForAnswer',
  stretchImage: 'stretchImage',
}
