import React from 'react'
import './check-component.scss'

export const CheckComponent = ({ type = 'white', text }) => {
  return (
    <div>
      <div className={type === 'white' ? 'check-white-text' : 'check-transparent-text'}>
        <div className='check-text__item'>
          <label htmlFor='' className='check-text__label'>
            <input type='checkbox' />
            <div />
            <span>{text}</span>
          </label>
        </div>
      </div>
    </div>
  )
}
