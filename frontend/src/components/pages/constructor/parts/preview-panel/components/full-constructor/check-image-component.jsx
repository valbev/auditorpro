import React from 'react'
import noImage from '../../images/noImage.png'
import './check-component.scss'

export const CheckImageComponent = ({ image = null, type = 'white' }) => {
  return (
    <div>
      <div
        className={type === 'white' ? 'check-white-image' : 'check-transparent-image'}
        style={{ height: 206, width: 160, marginBottom: 15 }}
      >
        {image ? (
          <div className='check-image__item' style={{ height: 206, width: 160 }}>
            <div className='check-image__image-wrapper' style={{ height: 142 }}>
              <img src={image} alt='' style={{ height: 142, objectFit: 'cover' }} />
            </div>
            <div className='check-image__content'>
              <p className='check-image__text'>Салат из овощей</p>
            </div>
          </div>
        ) : (
          <div className='check-image__item' style={{ height: 206 }}>
            <div className='check-image__image-wrapper check-image__image-wrapper_empty' style={{ height: 142 }}>
              <img src={noImage} alt='' style={{ width: 65.3, height: 49.5 }} />
            </div>
            <div className='check-image__content'>
              <p className='check-image__text'>Салат из овощей</p>
            </div>
          </div>
        )}
      </div>
    </div>
  )
}
