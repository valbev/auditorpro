import React from 'react'
import checkSearch from '../../images/checkSearch.svg'
import './check-component.scss'

export const QuestionImage = ({ image = null }) => {
  return (
    <div>
      <div className='check-image-lg'>
        <div className='check-image-lg__item'>
          <img src={image} alt='' style={{ maxHeight: 295, objectFit: 'cover', width: '100%' }} />
          <div className='check-image-lg__icon'>
            <img src={checkSearch} alt='' />
          </div>
        </div>
      </div>
    </div>
  )
}
