import React from 'react'
import '../preview-panel.scss'

export const ImageCardPreview = ({ item, title, lastCard, id }) => {
  return item.file ? (
    <div className='image-card_container'>
      <img className='image-card_preview' alt='' src={item.file} />
    </div>
  ) : (
    <div className='preview-panel_card' style={{ marginBottom: lastCard === id ? 240 : 0 }} />
  )
}
