import React from 'react'
import '../../preview-panel.scss'
import { CheckComponent } from './check-component'
import { CheckImageComponent } from './check-image-component'
import { QuestionImage } from './quiestion-image'

const heightPage = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)

export const FullCardPreview = ({ title, lastCard, item, id }) => {
  const mode = item.answers.filter((answer) => answer.photoPath !== '' && answer.photoPath !== 'test')
  return (
    <div
      className='preview-panel_card'
      style={{
        width: '100%',
        height: '100%',
        marginBottom: lastCard === id ? 240 : 0,
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <div
        style={{
          color: item.showType === 4 ? '#FFFFFF' : '#161616',
          marginBottom: 42,
          alignSelf: 'flex-start',
          paddingLeft: 20,
          paddingRight: 20,
          maxWidth: 375,
          width: '100%',
          margin: '0 auto 42px',
        }}
      >
        <div style={{ fontSize: 22, fontWeight: '600', marginTop: 30 }}>{title}</div>
        <div style={{ fontSize: 22, fontWeight: '600', marginTop: 40 }}>{item.title}</div>
        {item.description_mode && (
          <div style={{ fontSize: 17, fontWeight: '500', marginTop: 14 }}>{item.description}</div>
        )}
      </div>

      <div
        style={{
          overflowY: 'scroll',
          maxHeight: heightPage - 400,
          maxWidth: 375,
          height: '100%',
          overflowX: 'hidden',
          width: '100%',
          paddingLeft: 12,
          paddingRight: 7,
        }}
      >
        {item.showType !== 4 && item.title_image && <QuestionImage image={item.title_image} />}
        <div className={!mode.length ? 'check-text__wrapper' : 'check-image__wrapper'}>
          {item &&
            item.answers &&
            item.answers.map((answer) => {
              if (!mode.length) {
                return <CheckComponent type={item.showType !== 4 ? 'white' : 'transparent'} text={answer.title} />
              } else {
                return (
                  <CheckImageComponent type={item.showType !== 4 ? 'white' : 'transparent'} image={answer.photoPath} />
                )
              }
            })}
        </div>
      </div>
    </div>
  )
}
