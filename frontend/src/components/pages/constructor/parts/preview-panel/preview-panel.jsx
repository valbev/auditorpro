/* eslint-disable react/no-did-update-set-state */
import React, { PureComponent } from 'react'
import Scroll from 'react-scroll'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { FullCardPreview, ImageCardPreview } from './components'
import './preview-panel.scss'
import { ProgressBarAnswers } from '../../../image-poll-answers/components/main-body'

import newUnion from '../../images/newUnion.svg'
import downArrow from '../../images/downArrow.svg'
import topArrow from '../../images/topArrow.svg'

const height = document.documentElement.clientHeight

const { Link } = Scroll

export class PreviewPanel extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      cards: [],
      activeCard: 0,
      panelHeight: height * 0.8,
      currentPage: 0,
      length: null,
      constructor: '',
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { items, showType, constructorTitle } = this.props
    const { currentPage } = this.state
    const panelHeight = height * 0.8 * items.length
    this.setState({
      panelHeight,
    })
    if (items !== prevProps.items || showType !== prevProps.showType) {
      const newValue = items.map((item, index) => {
        return {
          id: index,
          name: `${item.type}`,
          title_image: item.title_image,
          showType: item.settings.showType === null ? 0 : item.settings.showType,
          title: item.title,
          answers: item.content.inputs,
          description_mode: item.settings.description_mode,
          description: item.description,
        }
      })
      this.setState({
        constructor: constructorTitle,
        currentPage:
          currentPage === 0
            ? 0
            : currentPage === newValue.length || currentPage > newValue.length
            ? currentPage - 1
            : currentPage,
        cards: newValue,
      })
    }
  }

  handleGoToBottomElement = () => {
    const { cards, activeCard } = this.state
    const lastCard = cards[cards.length - 1].id
    const currentCard = activeCard === lastCard ? activeCard : activeCard + 1
    this.setState({
      activeCard: currentCard,
    })
  }

  handleGoToTopElement = () => {
    const { activeCard } = this.state
    this.setState({
      activeCard: activeCard === 0 ? 0 : activeCard - 1,
    })
  }

  handleClickPrev = () => {
    const { currentPage } = this.state
    if (currentPage !== 0) {
      this.setState({
        currentPage: currentPage - 1,
      })
      const commentPart = document.getElementById(`full-${currentPage - 1}`)
      commentPart.scrollIntoView({ inline: 'center', behavior: 'smooth' })
    }
  }

  handleClickNext = () => {
    const { length, currentPage } = this.state
    const { items } = this.props
    this.setState({
      length: items.length,
    })
    if (currentPage !== items.length - 1 && items.length > 0 && items.length !== 1 && currentPage < items.length) {
      this.setState({
        currentPage: currentPage === length - 1 ? currentPage : currentPage + 1,
      })
      const commentPart = document.getElementById(`full-${currentPage + 1}`)
      commentPart.scrollIntoView({ inline: 'center', behavior: 'smooth' })
    }
  }

  render() {
    const { cards, currentPage, constructor } = this.state
    const { isFullConstructor, items } = this.props
    const lastCard = cards && cards.length && cards[cards.length - 1].id

    return (
      <div
        className='w-25 preview-panel'
        style={{
          // maxWidth: isFullConstructor ? '375px' : '100%',
          position: 'relative',
          justifyContent: 'center',
          minWidth: '375px',
        }}
      >
        <div
          id='containerElement'
          className='preview-panel_scrollbar'
          style={{
            overflowX: 'scrollX',
          }}
        >
          {cards.length ? (
            cards.map((item, index) => {
              return (
                <div
                  className='image-card_wrapper'
                  key={index}
                  id={`full-${index}`}
                  style={{
                    width: isFullConstructor ? '100%' : '90%',
                    height: isFullConstructor ? 'calc(100vh - 70px)' : height * 0.75,
                    marginBottom: isFullConstructor ? 0 : 50,
                  }}
                >
                  <div
                    key={index}
                    style={{
                      width: isFullConstructor ? '100%' : '90%',
                      height: isFullConstructor ? 'calc(100vh - 70px)' : height * 0.75,
                      marginBottom: isFullConstructor ? 0 : 50,
                      background:
                        item.showType === 4
                          ? 'linear-gradient(180deg, rgba(0, 0, 0, 0.65) 0%, rgba(0, 0, 0, 0.5) 94.37%), url(' +
                            item.title_image +
                            ') center center no-repeat'
                          : 'transparent',
                    }}
                  >
                    {/* <div className='image-card_header pt-3'>{item.title}</div> */}
                    <div className={isFullConstructor ? 'preview-panel_card-element' : 'image-card_card-element'}>
                      {isFullConstructor ? (
                        <FullCardPreview title={constructor} item={item} id={item.id} lastCard={lastCard} />
                      ) : (
                        <ImageCardPreview item={item} title={item.name} id={item.id} lastCard={lastCard} />
                      )}
                    </div>
                  </div>
                  {!isFullConstructor && (
                    <div className='preview-panel_action-group-wrapper'>
                      <div className='d-flex'>
                        <div className='preview-panel_button'>
                          <span>{index + 1}</span>
                        </div>
                        <div>
                          <button className='preview-panel_button preview-panel_button-preview'>
                            <span className='mr-1'>Просмотр</span>
                            <FontAwesomeIcon icon={['fas', 'eye']} color='#8355E2' size='1x' />
                            {/* <EyeSvg /> */}
                          </button>
                        </div>
                      </div>
                      <div className='d-flex'>
                        <Link
                          activeClass='active'
                          to={`${index + 1}`}
                          spy
                          smooth
                          duration={250}
                          containerId='containerElement'
                          onClick={this.handleClickNext}
                        >
                          <div className='preview-panel_button'>
                            <FontAwesomeIcon icon={['fas', 'angle-down']} color='#666666' size='sm' />
                          </div>
                        </Link>

                        <Link
                          activeClass='active'
                          to={`${index - 1}`}
                          spy
                          smooth
                          duration={250}
                          containerId='containerElement'
                          onClick={this.handleClickPrev}
                        >
                          <div className='preview-panel_button'>
                            <FontAwesomeIcon icon={['fas', 'angle-up']} color='#666666' size='sm' />
                          </div>
                        </Link>
                      </div>
                    </div>
                  )}
                </div>
              )
            })
          ) : (
            <div />
          )}
        </div>
        {isFullConstructor && (
          <div className='full-status-bar'>
            <div className='full-status-content'>
              <ProgressBarAnswers
                height={12}
                color='#8355E2'
                index={currentPage}
                length={items.length}
                width='80%'
                containerWidth='100%'
                type='full'
              />
              <div className='status-btn-container'>
                <div>
                  <button className='full-btn-preview'>
                    <img src={newUnion} style={{ marginRight: 5 }} alt='no image' />
                    <span className='mr-1 full-btn-prev-text'>Просмотр</span>
                  </button>
                </div>
                <div>
                  <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <div className='preview-panel-btn' onClick={this.handleClickPrev}>
                      <img src={downArrow} alt='wait' />
                    </div>
                    <div className='second-prev-btn preview-panel-btn' onClick={this.handleClickNext}>
                      <img src={topArrow} alt='wait' />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    )
  }
}
