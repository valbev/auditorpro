import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Select } from '../../../../../assets/components/select'
import { Input } from '../../../../../assets/components/input'
import { FormGroup, Label } from 'reactstrap'
import { settingsTypes } from '../../constants'
import { Button } from '../../../../../assets/components/button'
import closeSettings from '../../images/closeSettings.svg'
import fullFormat from '../../images/fullFormat.svg'
import incompleteFormat from '../../images/incompleteFormat.svg'
import fullFormat1 from '../../images/fullFormat1.svg'

import incompleteFormat1 from '../../images/incompleteFormat1.svg'
import firstShowTypeDisabled from './images/firstShowTypeDisabled.svg'
import secondShowTypeDisabled from './images/secondShowTypeDisabled.svg'
import thirdShowTypeDisabled from './images/thirdShowTypeDisabled.svg'
import fourShowTypeDisabled from './images/fourShowTypeDisabled.svg'

import firstShowTypeActive from './images/firstShowTypeActive.svg'
import secondShowTypeActive from './images/secondShowTypeActive.svg'
import thirdShowTypeActive from './images/thirdShowTypeActive.svg'
import fourShowTypeActive from './images/fourShowTypeActive.svg'

import firstShowTypeHover from './images/firstShowTypeHover.svg'
import secondShowTypeHover from './images/secondShowTypeHover.svg'
import thirdShowTypeHover from './images/thirdShowTypeHover.svg'
import fourShowTypeHover from './images/fourShowTypeHover.svg'

import '../../constructor.scss'

export const SettingsPanel = ({
  settings,
  index,
  type,
  tooglerActionHandler,
  handleChangeSettingInput,
  handleCloseSettings,
  fullConstructorFormat,
  constructorTypeToggler,
  isFullConstructor,
  welcomePointActive,
  welcomePontToggler,
  toggleConstructorType,
  handleChangeShowType,
  handleChangeCountAnswer,
}) => {
  const [activeHover, setHover] = useState([
    { id: 1, hover: false },
    { id: 2, hover: false },
    { id: 3, hover: false },
    { id: 4, hover: false },
  ])

  const buildSettings = () => {
    const showedImage = [
      { id: 1, notActive: firstShowTypeDisabled, active: firstShowTypeActive, hover: firstShowTypeHover },
      { id: 2, notActive: secondShowTypeDisabled, active: secondShowTypeActive, hover: secondShowTypeHover },
      { id: 3, notActive: thirdShowTypeDisabled, active: thirdShowTypeActive, hover: thirdShowTypeHover },
      { id: 4, notActive: fourShowTypeDisabled, active: fourShowTypeActive, hover: fourShowTypeHover },
    ]

    const handleHoverImage = (index) => {
      const newState = activeHover.map((item) => {
        if (item.id === index) {
          item.hover = !item.hover
          return item
        } else {
          return item
        }
      })
      setHover(newState)
    }

    return (
      <div className='p-3'>
        <div className='d-flex justify-content-between w-100 align-content-center'>
          <div>
            {Object.keys(settings).map((prop) => {
              switch (prop) {
                case settingsTypes.testMode:
                  return (
                    <div key='testMode' className='d-flex justify-content-between align-items-center test-mode-toggle'>
                      <Label>Тест</Label>
                      <Input.Toggler
                        checked={settings.testMode}
                        onChange={(evt) => tooglerActionHandler(index, settingsTypes.testMode, true)}
                      />
                    </div>
                  )
                default:
                  return null
              }
            })}
          </div>
          <Button type='icon' onClick={handleCloseSettings}>
            <img src={closeSettings} alt='' className='close-image' />
          </Button>
        </div>
        {/* { */}
        {/*  type ? */}
        {/*    <FormGroup> */}
        {/*      <label>Тип вопроса</label> */}
        {/*      <Select.Single /> */}
        {/*    </FormGroup> : */}
        {/*    <></> */}
        {/* } */}
        {Object.keys(settings).map((prop) => {
          switch (prop) {
            case settingsTypes.points:
              return (
                <div key='points' className='d-flex align-items-center mb-3'>
                  <Label className='points-left-label'>Баллов</Label>
                  <Input.Text value={settings.pointsValue} />
                </div>
              )
            case settingsTypes.fileType:
              return (
                <div key='fileType'>
                  <span>Тип файла</span>
                  <div className='mb-2'>
                    <Input.RadioGroup
                      titles={['Аудио', 'Фото/Видео']}
                      direction='column'
                      selected={settings.audio ? 0 : 1}
                    />
                  </div>
                </div>
              )
            case settingsTypes.required:
              return (
                <div
                  key='required'
                  className='d-flex justify-content-between align-items-center'
                  style={{
                    borderBottom: '1px solid #EDEEF4',
                    borderTop: '1px solid #EDEEF4',
                  }}
                >
                  <Label style={{ paddingTop: 15, marginBottom: 15 }}>Обязательный</Label>
                  <Input.Toggler
                    style={{ paddingTop: 15, alignItems: 'center', marginBottom: 0 }}
                    checked={settings.required}
                    onChange={(evt) => tooglerActionHandler(index, settingsTypes.required)}
                  />
                </div>
              )
            case settingsTypes.description_mode:
              return (
                <div
                  key='description_mode'
                  className='d-flex justify-content-between align-items-center'
                  style={{
                    marginTop: 10,
                    borderBottom: '1px solid #EDEEF4',
                  }}
                >
                  <Label>Описание</Label>
                  <Input.Toggler
                    checked={settings.description_mode}
                    onChange={(evt) => tooglerActionHandler(index, settingsTypes.description_mode)}
                  />
                </div>
              )
            case settingsTypes.multipleAnswersSelect:
              return (
                <FormGroup key='multipleAnswersSelect' style={{ marginTop: 10 }}>
                  <label>Количество ответов</label>
                  <Select.Single
                    onChange={handleChangeCountAnswer}
                    itemList={settings.countOfAnswer}
                    selectedItem={settings.countOfAnswerValue}
                  />
                </FormGroup>
              )
            case settingsTypes.currentNumber:
              if (settings.currentNumber === true) {
                return (
                  <div key='multipleAnswers' className='d-flex justify-content-between align-items-center'>
                    <div className='d-flex justify-content-between align-items-center'>
                      <div className='d-flex justify-content-between align-items-center'>
                        <Input.Text
                          type='number'
                          name='currentNumberValue'
                          style={{ width: '47%', height: 32 }}
                          placeholder='0 - 2'
                          onChange={(e) => handleChangeSettingInput(e, index)}
                          value={settings.currentNumberValue}
                        />
                      </div>
                    </div>
                  </div>
                )
              } else {
                break
              }
            case settingsTypes.multipleAnswers:
              if (settings.multipleAnswers === true) {
                return (
                  <div key='multipleAnswers' className='d-flex justify-content-between align-items-center'>
                    <div className='d-flex justify-content-between align-items-center'>
                      <div className='d-flex justify-content-between align-items-center'>
                        <Input.Text
                          style={{ width: 70, height: 32, marginRight: 10 }}
                          name='answerFrom'
                          placeholder='0'
                          onChange={(e) => handleChangeSettingInput(e, index)}
                          value={settings.answerFrom}
                        />
                      </div>
                      <div className='d-flex justify-content-between align-items-center'>
                        <Label className='m-0'>До</Label>
                        <Input.Text
                          style={{ width: 70, height: 32, marginLeft: 10 }}
                          name='answerTo'
                          placeholder='2'
                          onChange={(e) => handleChangeSettingInput(e, index)}
                          value={settings.answerTo}
                        />
                      </div>
                    </div>
                  </div>
                )
              } else {
                break
              }
            case settingsTypes.onlyNumberToggler:
              return (
                <div key='onlyNumberToggler'>
                  <div className='d-flex justify-content-between align-items-center'>
                    <Label>Только число</Label>
                    <Input.Toggler
                      checked={settings.onlyNumber}
                      onChange={(evt) => tooglerActionHandler(index, settingsTypes.onlyNumber)}
                    />
                  </div>
                </div>
              )
            case settingsTypes.textCountToggler:
              return (
                <div key='textCountToggler'>
                  <div className='d-flex justify-content-between align-items-center'>
                    <Label>Кол-во символов</Label>
                    <Input.Toggler
                      checked={settings.maxTextLengthFlag}
                      onChange={(evt) => tooglerActionHandler(index, settingsTypes.maxTextLengthFlag)}
                    />
                  </div>
                  <Input.Text value={settings.maxTextLength} />
                </div>
              )
            // case settingsTypes.photoAttach:
            //   return (
            //     <div>
            //       <span>Прикрепить к вопросу</span>
            //       <div className='d-flex justify-content-between align-items-center'>
            //         <Label>Фото</Label>
            //         <Input.Toggler
            //           checked={settings.photoAttach}
            //           onChange={(evt) => tooglerActionHandler(index, settingsTypes.photoAttach)}
            //         />
            //       </div>
            //     </div>
            //   );
            case settingsTypes.comments:
              return (
                <div key='comments' className='d-flex justify-content-between align-items-center'>
                  <Label>Комментарий</Label>
                  <Input.Toggler
                    checked={settings.comments}
                    onChange={(evt) => tooglerActionHandler(index, settingsTypes.comments)}
                  />
                </div>
              )
            case settingsTypes.dynamicAnswer:
              return (
                <div
                  key='dynamicAnswer'
                  className='d-flex justify-content-between align-items-center'
                  style={{
                    borderBottom: '1px solid #EDEEF4',
                    borderTop: '1px solid #EDEEF4',
                    alignItems: 'center',
                  }}
                >
                  <Label style={{ paddingTop: 15, marginBottom: 15 }}>Динамический ответ</Label>
                  <Input.Toggler
                    style={{ paddingTop: 15, alignItems: 'center', marginBottom: 0 }}
                    checked={settings.dynamicAnswer}
                    onChange={(evt) => tooglerActionHandler(index, settingsTypes.dynamicAnswer)}
                  />
                </div>
              )
            case settingsTypes.mixAnswers:
              return (
                <div
                  key='mixAnswers'
                  className='d-flex justify-content-between align-items-center'
                  style={{
                    borderBottom: '1px solid #EDEEF4',
                    alignItems: 'center',
                  }}
                >
                  <Label style={{ paddingTop: 15, marginBottom: 15 }}>Перемешать ответы</Label>
                  <Input.Toggler
                    style={{ paddingTop: 15, alignItems: 'center', marginBottom: 0 }}
                    checked={settings.mixAnswers}
                    onChange={(evt) => tooglerActionHandler(index, settingsTypes.mixAnswers)}
                  />
                </div>
              )
            case settingsTypes.timeForAnswer:
              return (
                <div
                  key='timeForAnswer'
                  className='d-flex justify-content-between align-items-center'
                  style={{
                    borderBottom: '1px solid #EDEEF4',
                    alignItems: 'center',
                    display: 'flex',
                    flexDirection: 'column',
                  }}
                >
                  <div
                    className='d-flex justify-content-between align-items-center'
                    style={{ flexDirection: 'row', width: '100%' }}
                  >
                    <Label style={{ paddingTop: 15, marginBottom: 15 }}>Время на ответ</Label>
                    <Input.Toggler
                      style={{ paddingTop: 15, alignItems: 'center', marginBottom: 0 }}
                      checked={settings.timeForAnswer}
                      onChange={(evt) => tooglerActionHandler(index, settingsTypes.timeForAnswer)}
                    />
                  </div>
                  {settings.timeForAnswer && (
                    <div className='answer-time'>
                      <Input.Text
                        type='text'
                        name='timeMinute'
                        style={{ width: 70, height: 32, marginRight: 20 }}
                        onChange={(e) => handleChangeSettingInput(e, index, '60')}
                        value={settings.timeMinute}
                        placeholder='1 м'
                      />
                      <Input.Text
                        type='text'
                        onChange={(e) => handleChangeSettingInput(e, index, '59')}
                        name='timeSeconds'
                        style={{ width: 70, height: 32 }}
                        value={settings.timeSeconds}
                        placeholder='30 с'
                      />
                    </div>
                  )}
                </div>
              )
            case settingsTypes.showedType:
              return (
                <div key='showedType' style={{ marginTop: 10 }}>
                  <Label>Вид отображения</Label>
                  <div className='d-flex justify-content-start align-items-center' style={{ marginBottom: 10 }}>
                    {settings.showedType.map((id, index) => {
                      return (
                        <div
                          style={{ cursor: 'pointer' }}
                          key={index}
                          onClick={() => handleChangeShowType(index + 1)}
                          onMouseEnter={() => handleHoverImage(index + 1)}
                          onMouseLeave={() => handleHoverImage(index + 1)}
                        >
                          <img
                            src={
                              settings.showType === index + 1
                                ? activeHover[id].hover
                                  ? showedImage[id].hover
                                  : showedImage[id].active
                                : activeHover[id].hover
                                ? showedImage[id].hover
                                : showedImage[id].notActive
                            }
                            className='mr-3'
                          />
                        </div>
                      )
                    })}
                  </div>
                </div>
              )

            case settingsTypes.stretchImage:
              return (
                <div
                  key='stretchImage'
                  className='d-flex justify-content-between align-items-center'
                  style={{ marginTop: 10, marginBottom: 20 }}
                >
                  <Label>Растянуть изображение</Label>
                  <Input.Toggler
                    checked={settings.stretchImage}
                    onChange={(evt) => tooglerActionHandler(index, settingsTypes.stretchImage)}
                  />
                </div>
              )
            default:
              return null
          }
        })}
        <div>
          {
            <div>
              <span>Формат конструктора:</span>
              <div className='d-flex align-items-center mt-2'>
                <div>
                  <div className='d-flex align-items-baseline'>
                    <div className='mr-1 constructor-type-text'>Полный</div>
                    <Button type='icon'>
                      <FontAwesomeIcon icon={['fas', 'question-circle']} color='#8355E2' size='1x' />
                    </Button>
                  </div>
                  <Button type='icon' onClick={() => constructorTypeToggler(true)}>
                    {fullConstructorFormat ? <img src={fullFormat} alt='' /> : <img src={incompleteFormat} alt='' />}
                  </Button>
                </div>
                <div className='settings-constructor-type-separator' />
                <div>
                  <div className='d-flex align-items-baseline'>
                    <div className='mr-1 constructor-type-text'>Image</div>
                    <Button type='icon'>
                      <FontAwesomeIcon icon={['fas', 'question-circle']} color='#8355E2' size='1x' />
                    </Button>
                  </div>
                  <Button type='icon' onClick={() => constructorTypeToggler(false)}>
                    {fullConstructorFormat ? <img src={fullFormat1} alt='' /> : <img src={incompleteFormat1} alt='' />}
                  </Button>
                </div>
              </div>
            </div>
          }
          {/* {!isFullConstructor && */}
          {/*    <div> */}
          {/*        <span>Вопрос-приветствие:</span> */}
          {/*        <div className='d-flex justify-content-between align-items-center mt-2'> */}
          {/*            <Label>Welcome point</Label> */}
          {/*            <Input.Toggler */}
          {/*                checked={welcomePointActive} */}
          {/*                onChange={() => welcomePontToggler(welcomePointActive)} */}
          {/*            /> */}
          {/*        </div> */}
          {/*    </div> */}
          {/* } */}
          {/* <div className='mt-3'> */}
          {/*  <Button type='primary' style={{ width: '100%' }} onClick={() => toggleConstructorType(!isFullConstructor)}>Сменить конструктор</Button> */}
          {/* </div> */}
        </div>
      </div>
    )
  }

  return buildSettings()
}
