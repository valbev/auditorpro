import React, { Component } from 'react'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'
import { AddImageCard, CardBody } from './components'
import { AddCardType } from './components/card-type-buttons'
import '../../constructor.scss'
import { ReactComponent as UnionHorizontal } from '../../images/union-horizontal.svg'
import star from '../../images/star.svg'

const grid = 8

const getItemStyle = (isDragging, draggableStyle, border, active) => {
  return {
    userSelect: 'none',
    paddingTop: active ? 6 : 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: active ? 8 : 15,
    margin: `0 0 ${grid}px 0`,
    background: 'white',
    borderWidth: 2,
    borderRadius: 8,
    border: isDragging || border || active ? '2px solid #8355E2' : '2px solid transparent',
    borderColor: isDragging || border || active ? '#8355E2' : '2px solid transparent',
    ...draggableStyle,
  }
}

export class MainData extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentCard: null,
      border: false,
    }
  }

  handleClickCard = (cardId) => {
    this.setState({
      currentCard: cardId,
    })
  }

  onDragInputEnd = (result) => {
    this.props.onInputChange(result, this.state.currentCard)
  }

  showBorder = (card) => {
    const { handleHoverCard } = this.props
    if (handleHoverCard) {
      handleHoverCard(true, card)
      this.setState({
        border: true,
      })
    }
  }

  hideBorder = (card) => {
    const { handleHoverCard } = this.props
    if (handleHoverCard) {
      handleHoverCard(false, card)
      this.setState({
        border: false,
      })
    }
  }

  onDragEnd = (result) => {
    this.props.onCardChange(result)
  }

  render() {
    const {
      cards,
      settingsToggler,
      deleteCard,
      copyCard,
      cardTypeSelectHandler,
      createCardProps,
      addNewTextInput,
      isFullConstructor,
      createImageCard,
      welcomePointActive,
      welcomePointItem,
      handleInputChange,
      handleImageUploadModal,
      handleDeleteImage,
      deleteAnswerInputImage,
      handleImageUpload,
      handleChangeSettingInput,
      deleteAnswerInput,
      changeCardInput,
    } = this.props

    return (
      <div>
        {!isFullConstructor && welcomePointActive ? (
          <div className='card-constructor' style={getItemStyle(false, {})}>
            <CardBody
              handleChangeSettingInput={handleChangeSettingInput}
              item={welcomePointItem}
              changeCardInput={changeCardInput}
              index={0}
              addNewTextInput={addNewTextInput}
              deleteCard={deleteCard}
              copyCard={copyCard}
              cardTypeSelectHandler={cardTypeSelectHandler}
              settingsToggler={settingsToggler}
              welcomePointItem={welcomePointItem}
              handleInputChange={handleInputChange}
              deleteAnswerInputImage={deleteAnswerInputImage}
              handleImageUploadModal={handleImageUploadModal}
              handleDeleteImage={handleDeleteImage}
              handleImageUpload={handleImageUpload}
              handleClickCard={this.handleClickCard}
              onDragInputEnd={this.onDragInputEnd}
            />
          </div>
        ) : null}
        <DragDropContext onDragEnd={this.onDragEnd}>
          <Droppable droppableId='droppable'>
            {(provided, snapshot) => (
              <div {...provided.draggableProps} ref={provided.innerRef}>
                {cards.map((item, index) => (
                  <Draggable key={item.order_id} draggableId={`${item.order_id}`} index={index}>
                    {(provided, snapshot) => (
                      <div
                        {...provided.draggableProps}
                        ref={provided.innerRef}
                        style={getItemStyle(
                          snapshot.isDragging,
                          provided.draggableProps.style,
                          item.hover,
                          item.active,
                        )}
                        onClick={item.type !== 'imageCard' ? () => settingsToggler(item.id) : () => {}}
                        // onMouseEnter={() => this.showBorder(item)}
                        // onMouseLeave={() => this.hideBorder(item)}
                        className='card-constructor'
                      >
                        {item.active ? (
                          <div className='header-line-container' {...provided.dragHandleProps}>
                            <div className='w-50' />
                            <div className='header-line-content'>
                              <UnionHorizontal />
                              {item.settings.required === true && (
                                <img src={star} className='require-property' alt='no image' />
                              )}
                            </div>
                          </div>
                        ) : (
                          <div style={{ height: 0, width: 0, zIndex: -10 }} {...provided.dragHandleProps} />
                        )}
                        <CardBody
                          handleChangeSettingInput={handleChangeSettingInput}
                          deleteAnswerInput={deleteAnswerInput}
                          provided={provided}
                          changeCardInput={changeCardInput}
                          item={item}
                          index={index}
                          id={item.id}
                          addNewTextInput={addNewTextInput}
                          deleteCard={deleteCard}
                          copyCard={copyCard}
                          cardTypeSelectHandler={cardTypeSelectHandler}
                          settingsToggler={settingsToggler}
                          welcomePointItem={null}
                          handleInputChange={handleInputChange}
                          deleteAnswerInputImage={deleteAnswerInputImage}
                          handleImageUploadModal={handleImageUploadModal}
                          handleDeleteImage={handleDeleteImage}
                          handleImageUpload={handleImageUpload}
                          handleClickCard={this.handleClickCard}
                          onDragInputEnd={this.onDragInputEnd}
                        />
                      </div>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
        {isFullConstructor ? (
          <AddCardType createCardProps={createCardProps} />
        ) : (
          <AddImageCard createImageCard={createImageCard} />
        )}
      </div>
    )
  }
}
