import React, { useState } from 'react'
import { cardTypes } from '../../../../constants'
import { options } from './select-options'
import { cardTypeButtons } from '../card-type-buttons/components'
import { Dropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap'
import './type-select.scss'

export const CardTypeSelect = ({ type, cardTypeSelectHandler, id }) => {
  const [dropdownOpen, setDropdownOpen] = useState(false)

  const toggle = () => {
    return setDropdownOpen((prevState) => !prevState)
  }

  const handleMenuItem = async (id, prop) => {
    await cardTypeSelectHandler(id, prop)
    toggle()
  }

  return (
    <Dropdown isOpen={dropdownOpen} toggle={toggle}>
      <DropdownToggle className='dropdown-toggle' caret>
        {cardTypeButtons[type]}
      </DropdownToggle>
      <DropdownMenu className='drop-down-body'>
        <DropdownItem header> Выберите тип вопроса </DropdownItem>
        {Object.keys(cardTypes).map((prop) => (
          <div key={`${prop}`} onClick={() => handleMenuItem(id, prop)}>
            {options[prop]}
          </div>
        ))}
      </DropdownMenu>
    </Dropdown>
  )
}
