import React from 'react'
import { cardTypes } from '../../../constants'
import { Button } from '../../../../../../assets/components/button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export const AddImageCard = ({ createImageCard }) => {
  return (
    <div className='image-constructor-add-card-container'>
      <div>
        <Button type='icon' className='mr-2 add-image-button' onClick={() => createImageCard(cardTypes.imageCard)}>
          <FontAwesomeIcon icon={['fas', 'plus-square']} color='#8355E2' size='lg' />
          <span className='img-constuctor-add-card-label'> Добавить новую страницу </span>
        </Button>
      </div>
    </div>
  )
}
