import React from 'react'
import { cardTypes } from '../../../../constants'
import { Button } from '../../../../../../../assets/components/button'
import { cardTypeButtons } from './components'

export const AddCardType = ({ createCardProps }) => {
  return (
    <div className='create-card-buttons-panel'>
      <span className='create-card-panel-header'>Добавить новый вопрос</span>
      {Object.keys(cardTypes).map((type, index) => {
        return (
          <Button key={index} type='icon' onClick={() => createCardProps(type)} className='mr-3'>
            {cardTypeButtons[type]}
          </Button>
        )
      })}
    </div>
  )
}
