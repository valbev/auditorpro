import React from 'react'
import { cardTypes } from '../../../../constants'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'
import { AnswerRow } from './answer-row'
import { Button } from '../../../../../../../assets/components/button'
import { ReactComponent as DeletePreviewImageCard } from '../../../../images/delete-image-preview-card.svg'

export const BuildCardBody = ({
  item,
  index,
  cardId,
  handleClickCard,
  deleteAnswerInput,
  onDragInputEnd,
  handleImageUploadModal,
  addNewTextInput,
  deleteAnswerInputImage,
  handleDeleteImage,
  handleImageUpload,
  handleInputChange,
}) => {
  switch (item.type) {
    case cardTypes.multiple:
      return (
        <div>
          {item.settings.testMode && (
            <div className='mt-4 mb-2' style={{ marginLeft: item.active ? 9 : 0 }}>
              <span className='mr-3'>Верно</span>
              <span>Баллы</span>
            </div>
          )}
          <DragDropContext onDragEnd={onDragInputEnd}>
            <Droppable droppableId='droppables'>
              {(provided) => {
                return (
                  <div {...provided.draggableProps} ref={provided.innerRef}>
                    {item.content.inputs.map((row, rowIndex) => (
                      <Draggable key={rowIndex} draggableId={`${rowIndex}`} index={rowIndex}>
                        {(provided, snapshot) => (
                          <div {...provided.draggableProps} ref={provided.innerRef}>
                            <AnswerRow
                              deleteAnswerInput={deleteAnswerInput}
                              provided={provided}
                              cardId={cardId}
                              item={item}
                              settings={item.settings}
                              type={item.type}
                              rowSettings={row}
                              inputId={row.id}
                              rowIndex={rowIndex}
                              deleteAnswerInputImage={deleteAnswerInputImage}
                              handleInputChange={handleInputChange}
                              handleImageUploadModal={handleImageUploadModal}
                              onKeyPress={(target) => {
                                if (target.charCode === 13) {
                                  addNewTextInput(item, row.id)
                                }
                              }}
                            />
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )
              }}
            </Droppable>
          </DragDropContext>
        </div>
      )
    case cardTypes.oneFromList:
      return (
        <div>
          <div className='mt-4 mb-2'>
            <span className='mr-3'>Верно</span>
            <span>Баллы</span>
          </div>

          <DragDropContext onDragEnd={this.onDragInputEnd} onClick={() => this.handleClickCard(index)}>
            <Droppable droppableId='droppables'>
              {(provided, snapshot) => {
                return (
                  <div {...provided.droppableProps} ref={provided.innerRef} onClick={() => this.handleClickCard(index)}>
                    {item.content.inputs.map((row, rowIndex) => (
                      <Draggable
                        key={rowIndex}
                        draggableId={`${rowIndex}`}
                        index={rowIndex}
                        onClick={() => this.handleClickCard(index)}
                      >
                        {(provided) => (
                          <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                            <AnswerRow
                              type={item.type}
                              rowSettings={row}
                              rowIndex={rowIndex}
                              onKeyPress={(target) => {
                                if (target.charCode === 13) {
                                  addNewTextInput(item, row.id)
                                }
                              }}
                            />
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )
              }}
            </Droppable>
          </DragDropContext>
        </div>
      )
    case cardTypes.imageCard:
      return item.file ? (
        <div className='card-image-preview-wrapper'>
          <img className='card-image-preview' alt='' src={item.file} />
          <Button type='icon' className='delete-image-preview-btn' onClick={() => handleDeleteImage(index)}>
            <DeletePreviewImageCard />
          </Button>
        </div>
      ) : (
        <div className='position-relative mt-3'>
          <label className='input-file-upload d-flex align-items-center p-3'>
            <div className='input-file-button-imitation d-flex align-items-center justify-content-center'>
              Прикрепить файл
            </div>
            <div className='input-file-right-text ml-3'>
              <span>
                Выберите файл <br /> или перетащите его сюда
              </span>
            </div>
            <input
              onChange={(e) => handleImageUpload(e, index)}
              onClick={(e) => handleImageUploadModal(e, index)}
              id='file-upload'
              type='file'
              multiple={false}
            />
          </label>
        </div>
      )
    default:
      return <div />
  }
}
