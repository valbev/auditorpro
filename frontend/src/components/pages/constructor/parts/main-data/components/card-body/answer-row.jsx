import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from '../../../../../../../assets/components/button'
import { Input } from '../../../../../../../assets/components/input'
import { ReactComponent as Union } from '../../../../images/union.svg'
import addImage from '../../images/add-image.svg'
import { constructorPlaceholders } from '../../../../constants'
import deleteImage from '../../images/deleteImage.svg'

export const AnswerRow = ({
  type,
  item,
  rowSettings,
  rowIndex,
  onKeyPress,
  cardId,
  inputId,
  provided,
  settings,
  deleteAnswerInputImage,
  handleImageUploadModal,
  handleInputChange,
  deleteAnswerInput,
}) => {
  const answerPlaceholder = constructorPlaceholders[type].answer
  const [activeHeader, setActive] = useState(false)

  let imagesMode = false

  item.content.inputs.map((row) => {
    if ((row.photoPath !== '' || row.photoPath !== 'test') && settings.testMode) {
      imagesMode = true
    }
  })

  const pointInput = {
    width: 36,
    border: 'none',
    borderBottom: '1px solid transparent',
    outline: 'none',
    boxShadow: 'none',
    marginRight: 20,
    marginLeft: imagesMode ? 20 : 30,
    height: 20,
    textAlign: 'center',
    backgroundColor: '#EDEEF4',
    borderRadius: '3px',
  }

  console.log('photoPath =====>>>>', rowSettings.photoPath)

  return (
    <div
      className='d-flex align-items-center justify-content-between'
      ref={provided.innerRef}
      {...provided.droppableProps}
      {...provided.dragHandleProps}
    >
      {item.active && <Union style={{ marginRight: 7 }} />}
      {settings.testMode === true && (
        <label className='container-check' style={{ marginRight: 14 }}>
          <input type='checkbox' />
          <span className='checkmark' />
        </label>
      )}
      {settings.testMode === true && <Input.Text style={pointInput} />}
      {rowSettings.photoPath === '' || rowSettings.photoPath === 'test' ? (
        <Button type='icon' onClick={(e) => handleImageUploadModal(e, cardId, inputId)} style={{ marginRight: 24 }}>
          <img src={addImage} alt='' className='photo-attach' />
        </Button>
      ) : (
        <div
          style={{
            display: 'flex',
            cursor: 'pointer',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <img
            src={rowSettings.photoPath}
            style={{
              width: 22,
              height: 20,
              borderTopLeftRadius: 2,
              borderBottomLeftRadius: 2,
            }}
          />
          <div
            onClick={() => deleteAnswerInputImage(cardId, inputId)}
            style={{
              width: 20,
              height: 20,
              borderTopRightRadius: 2,
              borderBottomRightRadius: 2,
              border: '1px solid #9063ED',
              alignItems: 'center',
              display: 'flex',
              justifyContent: 'center',
            }}
          >
            <img src={deleteImage} />
          </div>
        </div>
      )}
      <Input.Text
        id={`placeholder-color answer-${rowSettings.id}`}
        onKeyPress={onKeyPress}
        placeholder={answerPlaceholder}
        value={rowSettings.title}
        style={{
          fontFamily: 'Manrope',
          fontSize: 14,
          borderBottom: activeHeader ? '1px solid #8355E2' : '1px solid transparent',
          borderTop: 'none',
          borderLeft: 'none',
          borderRight: 'none',
          outline: 'none',
          boxShadow: 'none',
          backgroundColor: '#FFFFFF',
          borderRadius: 0,
        }}
        autoFocus={rowSettings.focus}
        onChange={(event) => handleInputChange(event, cardId, inputId)}
        onFocus={() => setActive(true)}
        onBlur={() => setActive(false)}
      />
      {item.content.inputs.length > 1 && (
        <Button type='icon' onClick={() => deleteAnswerInput(cardId, inputId)}>
          <FontAwesomeIcon icon={['fas', 'times']} color='#666666' size='1x' />
        </Button>
      )}
    </div>
  )
}
