import React, { useState } from 'react'
import { Button } from '../../../../../../../assets/components/button'
import { Input } from '../../../../../../../assets/components/input'
import { CardTypeSelect } from '../type-select'
import { cardTypes, constructorPlaceholders } from '../../../../constants'
import addHeaderImage from '../../images/addHeaderImage.svg'
import deleteImage from '../../images/deleteImage.svg'

const inputIdStyle = {
  border: 'none',
  outline: 'none',
  boxShadow: 'none',
  backgroundColor: '#EDEEF4',
  width: 38,
  textAlign: 'center',
  height: 20,
  borderRadius: 3,
  fontFamily: 'Roboto',
  fontStyle: 'normal',
  fontWeight: '300',
  fontSize: 16,
  color: '#666666',
}

export const CardHeader = ({
  id,
  item,
  type,
  deleteAnswerInputImage,
  settings,
  cardTypeSelectHandler,
  welcomePointItem,
  handleImageUploadModal,
  handleInputChange,
  changeCardInput,
}) => {
  const headerPlaceholder = constructorPlaceholders[type].header
  const [activeHeader, setActive] = useState(false)

  console.log('ITEM =======>>>>', item.title_image)

  console.log('ITEM =======>>>>>>', item.title)

  return (
    <div className='d-flex align-items-center mb-3'>
      {welcomePointItem ? <></> : <Input.Text value={item.order_id} style={inputIdStyle} />}
      {type === cardTypes.imageCard ? (
        <></>
      ) : (
        <CardTypeSelect cardTypeSelectHandler={cardTypeSelectHandler} id={id} type={type} />
      )}
      {item.title_image === '' || item.title_image === 'test' ? (
        <Button type='icon' className='mr-1' onClick={(e) => handleImageUploadModal(e, id, null)}>
          <img src={addHeaderImage} alt='no-image' className='photo-attach' />
        </Button>
      ) : (
        <div
          style={{
            display: 'flex',
            cursor: 'pointer',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <img
            src={item.title_image}
            style={{
              width: 22,
              height: 20,
              borderTopLeftRadius: 2,
              borderBottomLeftRadius: 2,
            }}
            alt='no'
          />
          <div
            onClick={() => deleteAnswerInputImage(item.id, null)}
            style={{
              width: 20,
              height: 20,
              borderTopRightRadius: 2,
              borderBottomRightRadius: 2,
              border: '1px solid #9063ED',
              alignItems: 'center',
              display: 'flex',
              justifyContent: 'center',
            }}
          >
            <img src={deleteImage} alt='wait' />
          </div>
        </div>
      )}
      <Input.Text
        id={`placeholder-color ${id + 1}`}
        name='title'
        style={{
          fontFamily: 'Manrope',
          fontSize: 14,
          borderBottom: activeHeader ? '1px solid #8355E2' : 'none',
          borderTop: 'none',
          borderLeft: 'none',
          borderRight: 'none',
          outline: 'none',
          boxShadow: 'none',
          backgroundColor: '#FFFFFF',
          borderRadius: 0,
        }}
        placeholder={headerPlaceholder}
        onChange={(event) => changeCardInput(event, item.id)}
        value={item.title}
        onFocus={() => setActive(true)}
        onBlur={() => setActive(false)}
      />
    </div>
  )
}
