import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from '../../../../../../../assets/components/button'
import { cardTypes } from '../../../../constants'
import answerTime from '../../images/answerTime.svg'

export const CardFooter = ({ settingsToggler, cardId, deleteCard, copyCard, type, settings }) => {
  const time = `${Number(settings.timeMinute)} м : ${Number(settings.timeSeconds)} сек`
  return (
    <div className='d-flex justify-content-between mt-3 w-100'>
      {settings.timeForAnswer ? (
        <div className='time-container'>
          <img src={answerTime} alt='no-image' className='time-image' />
          <div>{time}</div>
        </div>
      ) : (
        <div style={{ width: 106 }} />
      )}
      <div className='d-flex' style={{ flexDirection: 'row', justifySelf: 'end', zIndex: 1000 }}>
        <Button type='icon' className='mr-4' onClick={() => settingsToggler(cardId)}>
          <FontAwesomeIcon icon={['fas', 'cog']} color='#666666' size='1x' />
        </Button>
        <Button type='icon' className='mr-4'>
          <FontAwesomeIcon icon={['fas', 'plus']} color='#8355E2' size='1x' />
        </Button>
        {type === cardTypes.imageCard ? (
          <></>
        ) : (
          <Button type='icon' className='mr-4' onClick={() => copyCard(cardId)}>
            <FontAwesomeIcon icon={['fas', 'copy']} color='#8355E2' size='1x' />
          </Button>
        )}
        <Button type='icon' className='mr-3' onClick={(e) => deleteCard(e, cardId)}>
          <FontAwesomeIcon icon={['far', 'trash-alt']} color='#8355E2' size='1x' />
        </Button>
      </div>
    </div>
  )
}
