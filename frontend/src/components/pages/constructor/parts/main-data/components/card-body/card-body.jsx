import React, { useState } from 'react'
import { constructorPlaceholders } from '../../../../constants'
import { CardHeader } from './header-card'
import { Input } from '../../../../../../../assets/components/input'
import { BuildCardBody } from './build-card-body'
import { CardFooter } from './footer-card'

export const CardBody = ({
  item,
  index,
  id,
  deleteCard,
  copyCard,
  cardTypeSelectHandler,
  settingsToggler,
  changeCardInput,
  welcomePointItem,
  handleInputChange,
  handleImageUploadModal,
  deleteAnswerInputImage,
  addNewTextInput,
  handleDeleteImage,
  handleImageUpload,
  handleClickCard,
  onDragInputEnd,
  deleteAnswerInput,
}) => {
  const descriptionPlaceholder = constructorPlaceholders[item.type].description
  const [activeHeader, setActive] = useState(false)
  return (
    <>
      <CardHeader
        item={item}
        id={id}
        cardTypeSelectHandler={cardTypeSelectHandler}
        type={item.type}
        settings={item.settings}
        welcomePointItem={welcomePointItem}
        handleInputChange={handleInputChange}
        changeCardInput={changeCardInput}
        deleteAnswerInputImage={deleteAnswerInputImage}
        handleImageUploadModal={handleImageUploadModal}
      />
      {item.settings.description_mode && (
        <div>
          <Input.Text
            id={`placeholder-color ${id}`}
            placeholder={descriptionPlaceholder}
            name='description'
            value={item.description}
            onChange={(event) => changeCardInput(event, item.id)}
            style={{
              fontFamily: 'Manrope',
              fontSize: 14,
              borderBottom: activeHeader ? '1px solid #8355E2' : 'none',
              borderTop: 'none',
              borderLeft: 'none',
              borderRight: 'none',
              outline: 'none',
              boxShadow: 'none',
              backgroundColor: '#FFFFFF',
              borderRadius: 0,
              marginBottom: 5,
            }}
            onFocus={() => setActive(true)}
            onBlur={() => setActive(false)}
          />
        </div>
      )}
      <BuildCardBody
        handleInputChange={handleInputChange}
        deleteAnswerInput={deleteAnswerInput}
        item={item}
        index={index}
        cardId={id}
        handleClickCard={handleClickCard}
        onDragInputEnd={onDragInputEnd}
        deleteAnswerInputImage={deleteAnswerInputImage}
        handleImageUploadModal={handleImageUploadModal}
        addNewTextInput={addNewTextInput}
        handleDeleteImage={handleDeleteImage}
        handleImageUpload={handleImageUpload}
      />
      {welcomePointItem ? (
        <></>
      ) : (
        <CardFooter
          settings={item.settings}
          cardId={item.id}
          settingsToggler={settingsToggler}
          deleteCard={deleteCard}
          copyCard={copyCard}
          type={item.type}
        />
      )}
    </>
  )
}
