import React from 'react'
import typeMultiple from '../images/typeMultiple.svg'
import typeText from '../images/typeText.svg'
import typeSection from '../images/typeSection.svg'
import typeOneFromList from '../images/typeOneFromList.svg'
import typeFileUpload from '../images/typeFileUpload.svg'
import typeYesNo from '../images/typeYesNo.svg'
import typeRating from '../images/typeRating.svg'
import '../card-type-button.scss'

export const cardTypeButtons = {
  multiple: <img src={typeMultiple} alt='' />,
  text: <img src={typeText} alt='' />,
  section: <img src={typeSection} alt='' />,
  oneFromList: <img src={typeOneFromList} alt='' />,
  fileUpload: <img src={typeFileUpload} alt='' />,
  yesNo: <img src={typeYesNo} alt='' />,
  rating: <img src={typeRating} alt='' />,
}
