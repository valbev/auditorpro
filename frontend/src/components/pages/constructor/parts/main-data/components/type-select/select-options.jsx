import React from 'react'
import { cardTypeButtons } from '../card-type-buttons/components'
import { Button } from '../../../../../../../assets/components'
import './type-select.scss'

export const options = {
  multiple: (
    <Button className='drop-down-menu-item'>
      <div>{cardTypeButtons.multiple}</div>
      <span className='ml-2'>Несколько из списка</span>
    </Button>
  ),
  text: (
    <Button className='drop-down-menu-item'>
      <div>{cardTypeButtons.text}</div>
      <span className='ml-2'>Текст</span>
    </Button>
  ),
  section: (
    <Button className='drop-down-menu-item'>
      <div>{cardTypeButtons.section}</div>
      <span className='ml-2'>Раздел</span>
    </Button>
  ),
  // oneFromList: (
  //   <Button className='drop-down-menu-item'>
  //     <div>{cardTypeButtons.oneFromList}</div>
  //     <span className='ml-2'>Один из списка</span>
  //   </Button>
  // ),
  fileUpload: (
    <Button className='drop-down-menu-item'>
      <div>{cardTypeButtons.fileUpload}</div>
      <span className='ml-2'>Загрузка файла</span>
    </Button>
  ),
  yesNo: (
    <Button className='drop-down-menu-item'>
      <div>{cardTypeButtons.yesNo}</div>
      <span className='ml-2'>Да / Нет</span>
    </Button>
  ),
  rating: (
    <Button className='drop-down-menu-item'>
      <div>{cardTypeButtons.rating}</div>
      <span className='ml-2'>Рейтинг</span>
    </Button>
  ),
}
