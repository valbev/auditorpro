import React from 'react'
import { Tabs } from '../../../assets/components'
import { MainData } from './parts/main-data'

export const CheckListBody = ({
  cards,
  onCardChange,
  settingsToggler,
  handleChangeSettingInput,
  changeCardInput,
  deleteAnswerInput,
  deleteCard,
  copyCard,
  cardTypeSelectHandler,
  createCardProps,
  addNewTextInput,
  isFullConstructor,
  createImageCard,
  welcomePointItem,
  welcomePointActive,
  onInputChange,
  handleImageUpload,
  handleInputChange,
  handleDeleteImage,
  handleHoverCard,
  deleteAnswerInputImage,
  handleImageUploadModal,
}) => {
  const createCheckListData = () => {
    return [
      {
        title: 'Форма',
        content: (
          <MainData
            handleChangeSettingInput={handleChangeSettingInput}
            deleteAnswerInputImage={deleteAnswerInputImage}
            changeCardInput={changeCardInput}
            deleteAnswerInput={deleteAnswerInput}
            welcomePointActive={welcomePointActive}
            welcomePointItem={welcomePointItem}
            handleImageUploadModal={handleImageUploadModal}
            handleHoverCard={handleHoverCard}
            cards={cards}
            onCardChange={onCardChange}
            settingsToggler={settingsToggler}
            deleteCard={deleteCard}
            handleDeleteImage={handleDeleteImage}
            addNewTextInput={addNewTextInput}
            copyCard={copyCard}
            handleImageUpload={handleImageUpload}
            isFullConstructor={isFullConstructor}
            cardTypeSelectHandler={cardTypeSelectHandler}
            createCardProps={createCardProps}
            createImageCard={createImageCard}
            onInputChange={onInputChange}
            handleInputChange={handleInputChange}
          />
        ),
      },
      {
        title: 'Результаты',
        content: <div>Отчеты</div>,
      },
      {
        title: 'Настройки',
        content: <div>Результаты</div>,
      },
      {
        title: 'Доступ',
        content: <div>Доступ</div>,
      },
    ]
  }

  return <Tabs tabList={createCheckListData()} />
}
