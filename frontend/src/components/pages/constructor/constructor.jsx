import React, { Component } from 'react'
import { ModalConstructor } from './components/modal'
import FullConstructor from './full-constructor/full-constructor'
import ImageConstructor from './image-constructor/image-constructor'
import { withAuth, withGsapiService } from '../../hoc'
import { withRouter } from 'react-router-dom'
import { Sidebar } from '../../sidebar'
import { Button, ModalBody, ModalFooter, ModalHeader } from 'reactstrap'

import { connect } from 'react-redux'
import { Actions } from './redux/actions'

@withGsapiService()
@withAuth()
@withRouter
class Constructor extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false,
      isFullConstructor: false,
      imageConstructorData: null,
      fullConstructorData: null,
      openedSettings: true,
    }
  }

  handleOpenModal = () => {
    const { modal } = this.state
    this.setState({
      modal: !modal,
    })
  }

  closeSettingsPanel = () => {
    this.setState({
      openedSettings: false,
    })
  }

  async componentDidMount() {
    const {
      match: {
        params: { id },
      },
      getSecretService,
    } = this.props
    const localeFullId = localStorage.getItem('id_image_poll', null)
    const localeImageId = localStorage.getItem('id_full_poll', null)
    const localeType = localStorage.getItem('poll_type', null)
    const currentId = id || (localeFullId !== 'null' ? localeFullId : localeImageId !== 'null' ? localeImageId : null)
    if (currentId) {
      try {
        if (this.props.constructorType !== undefined && localeType !== 'null' && localeType !== 'full_poll') {
          const response = await getSecretService.fetchData({ action: 'getPolls', id: currentId, slug: '-' })
          if (response.result[0].type_poll === 'image_poll') {
            this.setState({
              modal: false,
              isFullConstructor: false,
              imageConstructorData: response.result[0],
              openedSettings: false,
            })
          }
        } else if (currentId !== 'null' && localeType !== 'null') {
          const poll = await getSecretService.fetchData({ action: 'getFullPoll', id: currentId })
          this.props.toggleConstructorId(poll.result[0].poll_id, 'full_poll')
          if (poll) {
            this.setState({
              modal: false,
              isFullConstructor: true,
              openedSettings: false,
              fullConstructorData: poll.result[0],
            })
          }
        } else {
          this.setState({
            imageConstructorData: [],
            fullConstructorData: [],
            openedSettings: true,
            isFullConstructor: true,
          })
        }
      } catch (err) {
        this.setState({
          modal: true,
        })
      }
    } else {
      this.setState({
        imageConstructorData: [],
        fullConstructorData: [],
        isFullConstructor: true,
        openedSettings: true,
      })
    }
  }

  toggleConstructorType = async (type, items) => {
    const { getSecretService } = this.props
    this.setState({
      isFullConstructor: type,
    })
    if (!type) {
      if (items.length === 0) {
        try {
          const response = await getSecretService.fetchData({ action: 'createImageConstructor' })
          if (response && response.id_image_poll) {
            localStorage.setItem('id_image_poll', response.id_image_poll)
            localStorage.setItem('poll_type', 'image_poll')
            this.props.toggleConstructorId(response.id_image_poll)
          }
        } catch (err) {
          return err
        }
      }
    } else if (type) {
      const data = {
        questions: [],
      }
      if (items.length === 0) {
        try {
          const response = await getSecretService.fetchData({ action: 'createFullConstructor' }, data)
          if (response && response.poll_id) {
            localStorage.setItem('id_full_poll', response.poll_id)
            localStorage.setItem('poll_type', 'full_poll')
            this.props.toggleConstructorId(response.poll_id)
          }
        } catch (err) {
          return err
        }
      }
    }
  }

  handleChangeType = (type) => {
    this.toggleConstructorType(type)
    this.handleOpenModal()
  }

  constructorTypeToggler = (value) => {
    this.setState({
      isFullConstructor: value,
    })
  }

  render() {
    const { modal, isFullConstructor, imageConstructorData, fullConstructorData, openedSettings } = this.state
    return (
      <div style={{ paddingLeft: 50, paddingTop: 0 }}>
        <Sidebar />
        <ModalConstructor
          modal={modal}
          handleOpenModal={this.handleOpenModal}
          toggleConstructorType={this.toggleConstructorType}
        >
          <ModalHeader toggle={this.handleOpenModal}>Modal title</ModalHeader>
          <ModalBody>
            <div onClick={() => this.handleChangeType(true)}>full constructor</div>
            <div onClick={() => this.handleChangeType(false)}> image constructor</div>
          </ModalBody>
          <ModalFooter>
            <Button color='secondary' onClick={this.handleOpenModal}>
              Cancel
            </Button>
          </ModalFooter>
        </ModalConstructor>
        {isFullConstructor ? (
          <div>
            {fullConstructorData !== null ? (
              <FullConstructor
                constructorTypeToggler={this.constructorTypeToggler}
                isFullConstructor={isFullConstructor}
                toggleConstructorType={this.toggleConstructorType}
                fullConstructorData={fullConstructorData}
              />
            ) : null}
          </div>
        ) : (
          <div>
            {imageConstructorData !== null ? (
              <ImageConstructor
                openedSettings={openedSettings}
                isFullConstructor={isFullConstructor}
                closeSettingsPanel={this.closeSettingsPanel}
                constructorTypeToggler={this.constructorTypeToggler}
                toggleConstructorType={this.toggleConstructorType}
                imageConstructorData={imageConstructorData}
              />
            ) : null}
          </div>
        )}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    constructorId: state.constructor.constructorId,
    constructorType: state.constructor.constructorType,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    toggleConstructorId: (id) => dispatch(Actions.toggleConstructorId(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Constructor)
