import React from 'react'
import { Modal } from 'reactstrap'

export const ModalConstructor = ({ modal, handleOpenModal, toggleConstructorType, children, style }) => {
  return (
    <div>
      <Modal isOpen={modal} style={style} toggle={handleOpenModal}>
        {children}
      </Modal>
    </div>
  )
}
