import React from 'react'
import { ModalConstructor } from '../modal'
import { Button, ModalBody, ModalFooter } from 'reactstrap'
import { Button as UiButton } from '../../../../../assets/components/button'
import { ReactComponent as UploadImage } from './images/upload.svg'
import { ReactComponent as DeletePreviewImageCard } from '../../images/delete-image-preview-card.svg'
import './image-upload-dialog.scss'

const style = {
  maxWidth: '757px',
}

export class ImageUploadDialog extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      uploadedImage: null,
    }
  }

  handleChange = (e) => {
    const uploadedImage = e.target.files[0]
    this.setState({
      uploadedImage,
    })
  }

  closeMiddleware = (e) => {
    this.props.handleImageUploadModal(e, null)
    this.setState({
      uploadedImage: null,
    })
  }

  saveCloseMiddleware = () => {
    const { uploadedImage } = this.state
    this.props.handleModalUpload(uploadedImage)
    this.setState({
      uploadedImage: null,
    })
  }

  deleteImage = () => {
    this.setState({
      uploadedImage: null,
    })
  }

  render() {
    const { uploadedImage } = this.state
    const { modal } = this.props
    return (
      <ModalConstructor modal={modal} style={style}>
        <ModalBody className={uploadedImage ? 'modal_body-image' : 'modal_body-upload'}>
          {uploadedImage ? (
            <div className='h-100'>
              <UiButton type='icon' className='modal_image-upload_delete-icon' onClick={() => this.deleteImage()}>
                <DeletePreviewImageCard />
              </UiButton>
              <img
                alt=''
                className='modal_image-upload_preview'
                style={{ objectFit: 'contain' }}
                src={URL.createObjectURL(uploadedImage)}
              />
            </div>
          ) : (
            <div className='d-flex flex-column align-items-center h-100 justify-content-center'>
              <UploadImage />
              <div className='mt-3'>
                <span className='modal_image-upload_span'>Выберите изображение для загрузки</span>
              </div>
              <input onChange={(e) => this.handleChange(e)} type='file' multiple={false} />
            </div>
          )}
        </ModalBody>
        <ModalFooter className='d-flex justify-content-between'>
          <div>
            <Button
              type='primary'
              className='modal_btn-save mr-2'
              onClick={uploadedImage ? () => this.saveCloseMiddleware() : () => {}}
            >
              Сохранить
            </Button>
            <Button className='modal_btn-cancel' onClick={(e) => this.closeMiddleware(e)}>
              Отмена
            </Button>
          </div>
          <Button onClick={() => this.deleteImage()} className='modal_btn-delete'>
            Удалить изображение
          </Button>
        </ModalFooter>
      </ModalConstructor>
    )
  }
}
