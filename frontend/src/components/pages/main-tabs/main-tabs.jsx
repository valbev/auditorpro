import React from 'react'
import './main-tabs.scss'
import { Tabs } from '../../../assets/components/tabs'
import { Results } from '../results/results'
import { Sidebar } from '../../sidebar'

export const MainTabs = () => {
  const DemoContentForTabs = ({ title }) => <div>{title}</div>
  const tabList = [
    { title: 'Форма', content: <DemoContentForTabs title='Контент "Форма"' /> },
    { title: 'Результаты', content: <Results /> },
    { title: 'Настройки', content: <DemoContentForTabs title='Контент "Настройки"' /> },
    { title: 'Доступ', content: <DemoContentForTabs title='Контент "Доступ"' /> },
  ]
  return (
    <div>
      <Sidebar />
      <div className='main-tabs'>
        <Tabs tabList={tabList} classTabsHeader='tabs-center' activeTab={1} />
      </div>
    </div>
  )
}
