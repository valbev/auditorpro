import React from 'react'
// import { Col, Row } from 'reactstrap'
import 'bootstrap/scss/bootstrap.scss'
// import { Button } from '../../../assets/components';
// import {Card, Slider, Tabs, Table} from '../../assets/components';
// import {cardsDemo, demoTable, sliderDemo, tabList, tooltipContent} from './demo';
// import { cardsDemo } from '../../ui-kit/demo';
// import { Card } from '../../../assets/components/card';
// import defaultImg from './img/company-pic.jpg';
import './results.scss'
import { Tabs } from '../../../assets/components/tabs'
import { Answers } from './../answers/answers'

export const Results = () => {
  const DemoContentForTabs = ({ title }) => <div>{title}</div>
  const SummaryReport = ({ title }) => {
    const d = new Date()
    const dn = Date.now()
    console.log('dn', dn)
    const millisec = +d
    // .getTime()
    const ddn = new Date(dn)

    const dd = new Date(millisec)
    // const strDate = dd.toLocaleString()
    // console.log("strDate", strDate)
    // const ddd = new Date(strDate)
    const jsonStr = '2020-06-23 10:18:52.946955'
    const jsonDate = new Date(jsonStr)
    console.log('jsonStr', jsonStr)
    console.log('jsonDate.toLocaleString()', jsonDate.toLocaleString())
    console.log('jsonDate.toUTCString()', jsonDate.toUTCString())
    const ms = 950000
    const msDate = new Date(ms)
    console.log('msDate.toLocaleString()', msDate.toLocaleString())
    console.log('msDate.toUTCString()', msDate.toUTCString())

    return (
      <div>
        <div>{title}</div>
        <p>toLocaleDateString() = {d.toLocaleDateString()}</p>
        <p>toLocaleTimeString() = {d.toLocaleTimeString()}</p>
        <p>toLocaleString() = {d.toLocaleString()}</p>
        <p>-------------------------------------------------</p>
        <p>millisec = {millisec}</p>
        <p>-------------------------------------------------</p>
        <p>toLocaleDateString() = {dd.toLocaleDateString()}</p>
        <p>toLocaleTimeString() = {dd.toLocaleTimeString()}</p>
        <p>toLocaleString() = {dd.toLocaleString()}</p>
        <p>-------------------------------------------------</p>
        <p>ddn = {ddn.toLocaleString()}</p>
      </div>
    )
  }
  const tabList = [
    { title: 'Ответы', content: <Answers /> },
    { title: 'Сводный отчет', content: <SummaryReport title='Контент "Сводный отчет"' /> },
    { title: 'Неверные ответы', content: <DemoContentForTabs title='Контент "Неверные ответы"' /> },
  ]
  return (
    <div>
      <div className='results'>
        {/* <h2 className='results__title'>Результаты</h2> */}
        <Tabs tabList={tabList} />
      </div>
    </div>
  )
}
