export const SURVEY_TYPE = {
  firstShowType: 0,
  secondShowType: [
    { type: 1, mode: 'text' },
    { type: 1, mode: 'image' },
  ],
  thirdShowType: [
    { type: 2, mode: 'text' },
    { type: 2, mode: 'image' },
  ],
  fourthShowType: [
    { type: 3, mode: 'text' },
    { type: 3, mode: 'image' },
  ],
}
