import React from 'react'
import { AnswerText } from '../answer-text'
import { AnswerImage } from '../answer-image'

export const SecondSurveyType = ({ cardIndex, card, mode, chooseAnswer, widthPage, heightPage }) => {
  return (
    <div
      id={`question-${cardIndex}`}
      style={{
        paddingTop: 120,
        width: widthPage,
        height: heightPage,
        color: '#161616',
        fontWeight: 500,
        textAlign: 'left',
        backgroundColor: '#fff',
      }}
    >
      <div
        style={{
          width: widthPage,
          paddingRight: 30,
          paddingLeft: 0,
        }}
      >
        <div
          style={{
            width: '100%',
            paddingLeft: 30,
            paddingRight: 30,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          {card.title_image !== '' && card.title_image !== 'test' && (
            <div style={{ width: '41%' }}>
              <h1 className='without-main-img__title'>{card.caption}</h1>
              <p className='without-main-img__description'>{card.description}</p>
              <div
                className='survey-type2__img'
                style={{
                  width: '100%',
                  display: 'block',
                  maxHeight: '100%',
                  height: '100%',
                  maxWidth: 400,
                  borderRadius: 10,
                }}
              >
                <img
                  src={card.title_image}
                  alt='image'
                  style={{
                    borderRadius: 10,
                    maxWidth: 400,
                    // maxHeight: 475,
                    maxHeight: heightPage - 385,
                  }}
                />
              </div>
            </div>
          )}
          <div
            className='survey-type2'
            style={{
              width: card.title_image === '' || card.title_image === 'test' ? '100%' : '65%',
              paddingTop: card.title_image === '' || card.title_image === 'test' ? 10 : 100,
            }}
          >
            {(card.title_image === '' || card.title_image === 'test') && (
              <div>
                <h1 className='without-main-img__title'>{card.caption}</h1>
                <p className='without-main-img__description'>{card.description}</p>
              </div>
            )}
            <div className='survey-type2__content'>
              <p className='without-main-img__hint'>Осталось выбрать еще 2</p>
              <div
                className='without-main-img__answers'
                style={{
                  overflow: 'auto',
                  paddingBottom: 50,
                  alignItems: 'flex-start',
                  height: '100%',
                  maxHeight: heightPage - 385,
                  overflowX: 'hidden',
                  overflowY: 'auto',
                }}
              >
                {card &&
                  card.items &&
                  card.items.map((item) => {
                    if (!mode.length) {
                      return (
                        <AnswerText
                          key={item.item_question_id}
                          classNames='cols1'
                          title={item.text}
                          check={item.userChecked}
                          answerId={item.item_question_id}
                          cardId={card.question_id}
                          chooseAnswer={chooseAnswer}
                        />
                      )
                    } else {
                      return (
                        <AnswerImage
                          key={item.item_question_id}
                          classNames='cols-img-4'
                          src={item.photo_path}
                          check={item.userChecked}
                          title={item.text}
                          answerId={item.item_question_id}
                          cardId={card.question_id}
                          chooseAnswer={chooseAnswer}
                        />
                      )
                    }
                  })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
