import React from 'react'
import { SurveyHeader } from '../survey-header'
import { SurveyFooter } from '../survey-footer/survey-footer'
import videoIcon from './video.svg'
import photoIcon from './photo.svg'
import audioIcon from './audio.svg'
import noteIcon from './note.svg'
import fileIcon from './file.svg'
import '../without-main-img/without-main-img.scss'
import './survey-load-file.scss'

export const SurveyLoadFile = () => {
  const arrFileTypes = [
    { title: 'Видео', img: videoIcon, count: 10 },
    { title: 'Фото', img: photoIcon, count: null },
    { title: 'Аудио', img: audioIcon, count: null },
    { title: 'Заметка', img: noteIcon, count: 5 },
    { title: 'Файл', img: fileIcon, count: null },
  ]
  const fileTypesCards = arrFileTypes.map((item, i) => (
    <div key={i} className='file-type-card'>
      <img src={item.img} className='file-type-card__img' alt='' />
      {item.count && <span className='file-type-card__count'>{item.count}</span>}
      <span className='file-type-card__title'>{item.title}</span>
    </div>
  ))

  return (
    <>
      <div className='without-main-img'>
        <SurveyHeader />
        <div className='without-main-img__container'>
          <h1 className='without-main-img__title'>Загрузите файл</h1>
          <p className='without-main-img__description'>Выберите несколько хищных зверей из списка ниже</p>
          <p className='without-main-img__hint'>Выберите 1 способ из предложенных</p>
        </div>
        <div className='without-main-img__container without-main-img__scroll'>
          <div className='without-main-img__answers'>{fileTypesCards}</div>
        </div>
        <SurveyFooter />
      </div>
    </>
  )
}
