import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from '../../../../../assets/components'
import { ProgressBlock } from '../progress-block'
import '../without-main-img/without-main-img.scss'

export const SurveyFooter = ({ handleClickNext, handleClickPrev, type, fourth, third = false, total, count }) => {
  return (
    <div
      className='without-main-img__footer'
      style={{
        display: 'flex',
        width: '100%',
        flexDirection: third ? 'row-reverse' : 'row',
        alignItems: 'center',
        background: 'transparent',
      }}
    >
      <div style={{ width: third ? '50%' : 0 }} />
      <div
        className='without-main-img__footer'
        style={{
          width: third ? '50%' : '100%',
          background: type ? 'transparent' : '#FFFFFF',
        }}
      >
        <div
          className='without-main-img__container flex-block'
          style={{
            padding: fourth ? '0 93px' : '0 20px',
          }}
        >
          <div className='without-main-img__footer-btns'>
            <Button
              className={`${
                type ? (third ? 'without-main-img__btn-back' : 'btn-back-second') : 'without-main-img__btn-back'
              }`}
              type='outline'
              onClick={handleClickPrev}
            >
              <FontAwesomeIcon
                icon={['fas', 'angle-left']}
                color={type ? (third ? '#666666' : '#FFFFFF') : '#666666'}
                size='1x'
              />
              <span>Назад</span>
            </Button>
            <Button className='without-main-img__btn-next' onClick={handleClickNext}>
              <span>Далее</span>
              <FontAwesomeIcon icon={['fas', 'angle-right']} color='#FFFFFF' size='1x' />
            </Button>
          </div>
          <ProgressBlock count={count} total={total} third={third} type={type} />
        </div>
      </div>
    </div>
  )
}
