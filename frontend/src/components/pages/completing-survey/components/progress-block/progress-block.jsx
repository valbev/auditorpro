import React from 'react'
import './progress-block.scss'

export const ProgressBlock = ({ count, total, type, third }) => {
  const divCountStyle = { width: `${Math.round((100 * count) / total)}%` }

  return (
    <div className='progress-block'>
      <div className='progress-block__line'>
        <div className='progress-block__line-done' style={divCountStyle} />
      </div>
      <div className='progress-block__text' style={{ color: !type ? '#161616' : third ? '#161616' : '#FFFFFF' }}>
        <span className='progress-block__count'>{count}</span>/<span className='progress-block__total'>{total}</span>
      </div>
    </div>
  )
}
