import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './answer-image.scss'

export const AnswerImage = ({
  classNames = null,
  src,
  answerId,
  cardId,
  chooseAnswer = () => {},
  title,
  fourth,
  check,
  imageClass,
  color = '#333333',
}) => {
  let classes = 'answer-image ' + classNames
  if (check) {
    classes += ' answer-image_check'
  }
  const imgStyle = src
    ? {
        background: 'rgba(0, 0, 0, 0.15) url(' + src + ') center center no-repeat',
        backgroundSize: 'cover',
        opacity: check ? 1 : 1,
      }
    : {
        background: '#333333',
        opacity: 0.6,
      }

  return (
    <div
      className={classes}
      style={{ background: fourth ? 'rgba(255, 255, 255, 0.25)' : '#FFFFFF' }}
      onClick={() => chooseAnswer(cardId, answerId)}
    >
      <div className={`answer-image__img ${imageClass}`} style={imgStyle}>
        {!src && <FontAwesomeIcon icon={['fas', 'image']} color='#666666' size='5x' />}
      </div>
      {check && (
        <div className='answer-image__icon'>
          <FontAwesomeIcon icon={['fas', 'check']} color='#ffffff' size='1x' />
        </div>
      )}
      <div className='answer-image__title' style={{ color }}>
        {title}
      </div>
    </div>
  )
}
