import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './answer-text.scss'

export const AnswerText = ({ classNames = null, chooseAnswer = () => {}, title, cardId, answerId, check }) => {
  const classes = 'answer-text ' + classNames
  let classesIcon = 'answer-text__icon'
  if (!check) {
    classesIcon += ' no-check-color'
  }
  return (
    <div className={classes} onClick={() => chooseAnswer(cardId, answerId)}>
      <div className={classesIcon}>
        {check && <FontAwesomeIcon icon={['fas', 'check']} color='#FFFFFF' size='1x' />}
      </div>
      <span>{title}</span>
    </div>
  )
}
