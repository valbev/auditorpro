import React from 'react'
import { AnswerText } from '../answer-text/answer-text'
import { AnswerImage } from '../answer-image/answer-image'
import { SurveyHeader } from '../survey-header'
import { SurveyFooter } from '../survey-footer/survey-footer'
import './without-main-img.scss'

export const WithoutMainImg = () => {
  const srcImg = 'https://10619-2.s.cdn12.com/rests/small/w320/h220/101_243969130.jpg'
  const srcImg1 = 'https://i.pinimg.com/200x150/4e/be/15/4ebe1528c2ed5cbb7b6c6c22668af328.jpg'
  return (
    <>
      {/* AnswersText */}
      <div className='without-main-img'>
        <SurveyHeader />
        <div className='without-main-img__container'>
          <h1 className='without-main-img__title'>Выберите хищных зверей</h1>
          <p className='without-main-img__description'>Выберите несколько хищных зверей из списка ниже</p>
          <p className='without-main-img__hint'>Осталось выбрать еще 2</p>
        </div>
        <div className='without-main-img__container without-main-img__scroll'>
          <div className='without-main-img__answers'>
            <AnswerText classNames='cols3' title='Амурский Тигр' check />
            <AnswerText classNames='cols3' title='Амурский Тигр' check={false} />
            <AnswerText classNames='cols3' title='Амурский Тигр' check />
            <AnswerText classNames='cols3' title='Амурский Тигр' check />
            <AnswerText classNames='cols3' title='Амурский Тигр' />
            <AnswerText classNames='cols3' title='Амурский Тигр' check />
            <AnswerText classNames='cols3' title='Амурский Тигр' check />
            <AnswerText classNames='cols3' title='Амурский Тигр' />
          </div>
        </div>
        <SurveyFooter />
      </div>

      {/* AnswersImage */}
      <div className='without-main-img'>
        <SurveyHeader />
        <div className='without-main-img__container'>
          <h1 className='without-main-img__title'>Выберите хищных зверей</h1>
          <p className='without-main-img__description'>Выберите несколько хищных зверей из списка ниже</p>
          <p className='without-main-img__hint'>Осталось выбрать еще 2</p>
        </div>
        <div className='without-main-img__container without-main-img__scroll'>
          <div className='without-main-img__answers-img'>
            <AnswerImage classNames='cols-img-6' src={srcImg} check title='Пицца с ломтиками сыра' />
            <AnswerImage classNames='cols-img-6' src={srcImg1} check={false} title='Пицца с ломтиками сыра' />
            <AnswerImage classNames='cols-img-6' check title='Пицца с ломтиками сыра' />
            <AnswerImage classNames='cols-img-6' src={null} check={false} title='Пицца с ломтиками сыра' />
            <AnswerImage classNames='cols-img-6' src={srcImg} check title='Пицца с ломтиками сыра' />
            <AnswerImage classNames='cols-img-6' check={false} title='Пицца с ломтиками сыра' />
            <AnswerImage classNames='cols-img-6' src={srcImg1} check title='Пицца с ломтиками сыра' />
            <AnswerImage classNames='cols-img-6' check={false} title='Пицца с ломтиками сыра' />
          </div>
        </div>
        <SurveyFooter />
      </div>
    </>
  )
}
