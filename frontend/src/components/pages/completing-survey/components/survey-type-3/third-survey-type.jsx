import React from 'react'
import { AnswerText } from '../answer-text'
import { AnswerImage } from '../answer-image'

export const ThirdSurveyType = ({ cardIndex, card, mode, widthPage, chooseAnswer, heightPage }) => {
  return (
    <div>
      <div
        id={`question-${cardIndex}`}
        style={{
          width: widthPage,
          height: heightPage,
          color: '#161616',
          fontWeight: 500,
          textAlign: 'left',
          backgroundColor: '#fff',
        }}
      >
        <div
          style={{
            width: widthPage,
            paddingRight: 0,
            paddingLeft: 0,
          }}
        >
          <div
            style={{
              width: '100%',
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <div className='survey-type3__img' style={{ width: '50%' }}>
              <img
                src={card.title_image}
                alt='image'
                style={{ width: '100%', height: heightPage, objectFit: 'cover' }}
              />
            </div>
            <div className='survey-type3__right' style={{ width: '50%', paddingTop: 120 }}>
              <div className='without-main-img'>
                <div style={{ paddingLeft: 81 }}>
                  <h1 className='without-main-img__title'>{card.caption}</h1>
                  <p className='without-main-img__description'>{card.description}</p>
                  <p className='without-main-img__hint'>Осталось выбрать еще 2</p>
                </div>

                <div
                  className='without-main-img__scroll'
                  style={{
                    paddingLeft: 81,
                    height: '100%',
                    maxHeight: heightPage - 385,
                    overflowX: 'hidden',
                    overflowY: 'auto',
                  }}
                >
                  <div className='without-main-img__answers'>
                    {card &&
                      card.items &&
                      card.items.map((item) => {
                        if (!mode.length) {
                          return (
                            <AnswerText
                              classNames='cols1'
                              title={item.text}
                              check={item.userChecked}
                              key={item.item_question_id}
                              answerId={item.item_question_id}
                              cardId={card.question_id}
                              chooseAnswer={chooseAnswer}
                            />
                          )
                        } else {
                          return (
                            <AnswerImage
                              key={item.item_question_id}
                              classNames='cols-img-3'
                              src={item.photo_path}
                              check={item.userChecked}
                              title={item.text}
                              answerId={item.item_question_id}
                              cardId={card.question_id}
                              chooseAnswer={chooseAnswer}
                            />
                          )
                        }
                      })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
