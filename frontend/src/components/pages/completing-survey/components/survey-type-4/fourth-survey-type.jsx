import React from 'react'
import { AnswerText } from '../answer-text'
import { AnswerImage } from '../answer-image'
import '../../completing-survey.scss'

export const FourthSurveyType = ({ cardIndex, chooseAnswer, card, mode, widthPage, heightPage }) => {
  return (
    <div>
      <div
        id={`question-${cardIndex}`}
        className='without-main-img survey-type4 background'
        style={{
          background:
            'linear-gradient(180deg, rgba(0, 0, 0, 0.65) 0%, rgba(0, 0, 0, 0.5) 94.37%), url(' +
            card.title_image +
            ') center center no-repeat',
          backgroundSize: 'cover',
          width: widthPage,
          height: heightPage,
        }}
      >
        <div
          style={{
            width: '100%',
            paddingLeft: 0,
            paddingRight: 0,
            height: heightPage,
            paddingTop: 120,
          }}
        >
          <div className='without-main-img__container' style={{ color: '#FFFFFF', paddingLeft: 91, paddingRight: 93 }}>
            <h1 className='without-main-img__title'>{card.caption}</h1>
            <p className='without-main-img__description'>{card.description}</p>
            <p
              style={{
                color: 'C7C1D2',
                fontSize: 16,
                fontWeight: 400,
                marginBottom: 10,
              }}
            >
              Осталось выбрать еще 2
            </p>
          </div>
          <div
            className='without-main-img__container without-main-img__scroll'
            style={{
              paddingLeft: 91,
              paddingRight: 93,
              height: '100%',
              maxHeight: heightPage - 385,
              overflowX: 'hidden',
              overflowY: 'auto',
            }}
          >
            <div className='without-main-img__answers'>
              {card &&
                card.items &&
                card.items.map((item) => {
                  if (!mode.length) {
                    return (
                      <AnswerText
                        answerId={item.item_question_id}
                        cardId={card.question_id}
                        chooseAnswer={chooseAnswer}
                        classNames='cols3 blur-answers'
                        title={item.text}
                        key={item.item_question_id}
                        check={item.userChecked}
                      />
                    )
                  } else {
                    return (
                      <AnswerImage
                        answerId={item.item_question_id}
                        cardId={card.question_id}
                        chooseAnswer={chooseAnswer}
                        fourth
                        key={item.item_question_id}
                        classNames='cols-img-6 image-blur image-blur-active'
                        color='#FFFFFF'
                        imageClass='image-blur-img'
                        src={item.photo_path}
                        check={item.userChecked}
                        title={item.text}
                      />
                    )
                  }
                })}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
