import React from 'react'
import { AnswerText } from '../answer-text'
import { AnswerImage } from '../answer-image'

export const FirstSurveyType = ({ cardIndex, card, mode, chooseAnswer, widthPage, heightPage }) => {
  return (
    <div>
      <div
        id={`question-${cardIndex}`}
        style={{
          overflow: 'auto',
          paddingTop: 120,
          paddingBottom: 120,
          width: widthPage,
          height: heightPage,
          color: '#161616',
          fontWeight: 500,
          textAlign: 'left',
          backgroundColor: '#fff',
        }}
      >
        <div
          style={{
            width: widthPage,
            paddingRight: 30,
            paddingLeft: 0,
          }}
        >
          <div
            style={{
              width: '100%',
              paddingLeft: 30,
              paddingRight: 30,
              overflowX: 'scroll',
            }}
          >
            <h1 className='without-main-img__title'>{card.caption}</h1>
            <p className='without-main-img__description'>{card.description}</p>
            {card.title_image && (
              <div className='survey-type1__main-img'>
                <img src={card.title_image} alt='' />
              </div>
            )}
            <p className='without-main-img__hint'>Выбрано 2 из 4</p>

            <div className='without-main-img__answers'>
              {card &&
                card.items &&
                card.items.map((item) => {
                  if (!mode.length) {
                    return (
                      <AnswerText
                        key={item.item_question_id}
                        classNames='cols3'
                        title={item.text}
                        check={item.userChecked}
                        answerId={item.item_question_id}
                        cardId={card.question_id}
                        chooseAnswer={chooseAnswer}
                      />
                    )
                  } else {
                    return (
                      <AnswerImage
                        key={item.item_question_id}
                        classNames='cols-img-6'
                        src={item.photo_path}
                        check={item.userChecked}
                        title={item.text}
                        answerId={item.item_question_id}
                        cardId={card.question_id}
                        chooseAnswer={chooseAnswer}
                      />
                    )
                  }
                })}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
