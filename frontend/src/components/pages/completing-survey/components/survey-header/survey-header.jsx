import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import logo from './logo.svg'
import '../without-main-img/without-main-img.scss'

export const SurveyHeader = ({ type, title, fourth, third = false, showType }) => {
  return (
    <div
      className='without-main-img__header'
      style={{
        padding: 0,
        color: type ? '#FFFFFF' : third ? '#FFFFFF' : '#161616',
        backgroundColor: type ? 'transparent' : '#ffffff',
        position: 'fixed',
        width: '100%',
        zIndex: 100,
      }}
    >
      <div
        className='without-main-img__container flex-block'
        style={{
          display: 'flex',
          padding: fourth ? '0 91px' : '0 20px',
          flexDirection: third ? 'row-reverse' : 'row',
        }}
      >
        <div
          className='without-main-img__header-left'
          style={{
            // marginRight: third ? widthPage * 0.21 : 0,
            position: 'relative',
            right: third ? '28%' : 0,
          }}
        >
          <div className='without-main-img__header-icon'>
            <FontAwesomeIcon icon={['far', 'file-alt']} color='#8355E2' size='lg' />
          </div>
          <span style={{ color: !fourth ? '#161616' : '#FFFFFF' }}>{title}</span>
        </div>
        <div
          className='without-main-img__header-right'
          style={{
            display: 'flex',
            flexDirection: third ? 'row-reverse' : 'row',
          }}
        >
          <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <div className='without-main-img__header-icon'>
              <FontAwesomeIcon icon={['far', 'clock']} color='#8355E2' size='lg' />
            </div>
            <span>Осталось: 1:36</span>
          </div>
          <div className='without-main-img__header-img' style={{ marginRight: third ? 76 : 0 }}>
            <img src={logo} alt='' />
          </div>
        </div>
      </div>
    </div>
  )
}
