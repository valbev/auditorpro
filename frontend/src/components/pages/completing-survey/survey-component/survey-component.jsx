import React from 'react'
import { SurveyHeader } from '../components/survey-header'
import { SurveyFooter } from '../components/survey-footer'
import '../components/without-main-img/without-main-img.scss'
import './survey-component.scss'
import { FourthSurveyType } from '../components/survey-type-4/fourth-survey-type'
import { ThirdSurveyType } from '../components/survey-type-3/third-survey-type'
import { SecondSurveyType } from '../components/survey-type-2/second-survey-tupe'
import { FirstSurveyType } from '../components/survey-type-1/first-survey-type'

const widthPage = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
const heightPage = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)

export const SurveyComponent = ({
  display,
  currentPage,
  chooseAnswer,
  title,
  width,
  currentPageData,
  flexDirection,
  handleClickPrev,
  handleClickNext,
  items,
}) => {
  const type = currentPageData.type_for_show === 4 || currentPageData.type_for_show === 3
  const fourth = currentPageData.type_for_show === 4
  const third = currentPageData.type_for_show === 3

  return (
    <div
      style={{
        display,
        width,
        flexDirection,
        overflowX: width <= 768 ? 'scroll' : 'auto',
        height: width <= 768 ? heightPage * items.length : heightPage,
      }}
    >
      <SurveyHeader title={title} fourth={fourth} type={type} third={third} showType={currentPageData.type_for_show} />
      <SurveyFooter
        fourth={fourth}
        type={type}
        handleClickPrev={handleClickPrev}
        handleClickNext={handleClickNext}
        third={third}
        showType={currentPageData.type_for_show}
        total={items.length}
        count={currentPage + 1}
      />

      {items &&
        items.length &&
        items.map((card, cardIndex) => {
          const mode = card.items.filter((answer) => answer.photo_path !== '' && answer.photo_path !== 'test')
          const showType = card.type_for_show === 0 ? 1 : card.type_for_show
          switch (showType) {
            case 1:
              return (
                <FirstSurveyType
                  mode={mode}
                  chooseAnswer={chooseAnswer}
                  cardIndex={cardIndex}
                  card={card}
                  widthPage={widthPage}
                  heightPage={heightPage}
                />
              )
            case 2:
              return (
                <SecondSurveyType
                  mode={mode}
                  chooseAnswer={chooseAnswer}
                  cardIndex={cardIndex}
                  card={card}
                  widthPage={widthPage}
                  heightPage={heightPage}
                />
              )
            case 3:
              return (
                <ThirdSurveyType
                  mode={mode}
                  chooseAnswer={chooseAnswer}
                  cardIndex={cardIndex}
                  card={card}
                  widthPage={widthPage}
                  heightPage={heightPage}
                />
              )
            case 4:
              return (
                <FourthSurveyType
                  mode={mode}
                  fourth={fourth}
                  chooseAnswer={chooseAnswer}
                  cardIndex={cardIndex}
                  card={card}
                  widthPage={widthPage}
                  heightPage={heightPage}
                />
              )
          }
        })}
    </div>
  )
}
