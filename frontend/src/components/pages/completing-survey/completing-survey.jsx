import React, { Component } from 'react'
import './completing-survey.scss'
import { SurveyComponent } from './survey-component'
import { withRouter } from 'react-router-dom'
import { withAuth, withGsapiService } from '../../hoc'
import { Loader } from '../../../assets/components/loader'

const widthScreen = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)

@withRouter
@withGsapiService()
@withAuth()
class CompletingSurvey extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: '',
      items: [],
      type: null,
      width: widthScreen,
      display: 'flex',
      flexDirection: 'row',
      currentPage: 0,
      length: null,
      loading: true,
      currentPageData: null,
    }
  }

  async componentDidMount() {
    const { getSecretService, location } = this.props
    const pollId = location.pathname.split('/')[location.pathname.split('/').length - 1]
    const idPoll = pollId || null

    if (idPoll) {
      try {
        const poll = await getSecretService.fetchData({ action: 'getFullPoll', id: idPoll })
        if (poll && poll.result[0]) {
          console.log('poll.result[0] =============>>>>>', poll.result[0])
          const pageWidth =
            poll.result[0].questions.length > 0 ? widthScreen * poll.result[0].questions.length : widthScreen
          if (poll.result[0] && poll.result[0].questions) {
            poll.result[0].questions.map((question) => {
              if (question && question.items) {
                question.items.map((item) => {
                  return {
                    order_id: item.order_id,
                    text: item.title,
                    checked: item.checked,
                    photo_path: item.photoPath,
                    points: item.points,
                    userChecked: false,
                  }
                })
              } else {
                return question
              }
            })
          }
          this.setState(
            {
              width: widthScreen <= 768 ? widthScreen : pageWidth,
              currentPage: 0,
              flexDirection: widthScreen <= 768 ? 'column' : 'row',
              items: poll.result[0].questions,
              title: poll.result[0].title,
              currentPageData: poll.result[0].questions[0],
            },
            () => {
              console.log('width =====>>>>>', this.state.width)
              setTimeout(() => {
                this.setState(
                  {
                    loading: false,
                  },
                  () => {
                    const questionPage = document.getElementById(`question-${0}`)
                    questionPage.scrollIntoView({ inline: 'center', behavior: 'smooth' })
                  },
                )
              }, 500)
            },
          )
        }
      } catch (err) {
        console.log('error', err)
      }
    }
  }

  chooseAnswer = (cardId, answerId) => {
    const { items } = this.state
    const newItems = [...items]
    const currentCardIndex = newItems.findIndex((card) => card.question_id === cardId)
    const currentAnswerIndex = newItems[currentCardIndex].items.findIndex(
      (input) => input.item_question_id === answerId,
    )
    newItems[currentCardIndex].items[currentAnswerIndex].userChecked = !newItems[currentCardIndex].items[
      currentAnswerIndex
    ].userChecked
    this.setState({
      items: newItems,
    })
  }

  handleClickPrev = () => {
    const { currentPage, items } = this.state
    if (currentPage !== 0) {
      this.setState({
        currentPage: currentPage - 1,
        currentPageData: items[currentPage - 1],
      })
      const questionPage = document.getElementById(`question-${currentPage - 1}`)
      questionPage.scrollIntoView({ inline: 'center', behavior: 'smooth' })
    }
  }

  handleClickNext = () => {
    const { length, currentPage, items } = this.state
    this.setState({
      length: items.length,
    })
    if (currentPage !== length - 1 && items.length !== 1) {
      this.setState({
        currentPage: currentPage === length - 1 ? currentPage : currentPage + 1,
        currentPageData: items[currentPage === length - 1 ? currentPage : currentPage + 1],
      })
      const questionPage = document.getElementById(`question-${currentPage + 1}`)
      questionPage.scrollIntoView({ inline: 'center', behavior: 'smooth' })
    }
  }

  render() {
    const { width, display, flexDirection, loading, currentPageData, items, title, currentPage } = this.state
    return (
      <div className='completing-survey'>
        {loading ? (
          <Loader />
        ) : (
          <SurveyComponent
            chooseAnswer={this.chooseAnswer}
            title={title}
            currentPage={currentPage}
            currentPageData={currentPageData}
            handleClickPrev={this.handleClickPrev}
            handleClickNext={this.handleClickNext}
            width={width}
            display={display}
            items={items}
            flexDirection={flexDirection}
          />
        )}
      </div>
    )
  }
}

export default CompletingSurvey
