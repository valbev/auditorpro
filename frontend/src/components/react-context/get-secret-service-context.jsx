import { createContext } from 'react'

const { Provider: GetSecretServiceProvider, Consumer: GetSecretServiceConsumer } = createContext()

export { GetSecretServiceProvider, GetSecretServiceConsumer }
