import React from 'react'
import { ModalLayout } from '../../../assets/components'
import { Header, PersonalData, Footer } from '../parts'

export const RegNext = ({
  handleCloseClick,
  userType,
  handleChangeInput,
  status,
  formAction,
  changeAuthType,
  onAddressInput,
}) => {
  return (
    <ModalLayout
      style={{ width: '352px', backgroundColor: '#fff', padding: '25px' }}
      header={<Header status={status} />}
      content={
        <PersonalData
          userType={userType}
          handleChangeInput={handleChangeInput}
          formAction={formAction}
          onAddressInput={onAddressInput}
        />
      }
      footer={<Footer changeAuthType={changeAuthType} />}
      handleCloseClick={handleCloseClick}
    />
  )
}
