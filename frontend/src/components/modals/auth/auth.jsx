import React from 'react'
import { ModalLayout } from '../../../assets/components'
import { Header, Credentials, Footer } from '../parts'

export const Auth = ({
  handleCloseClick,
  userType,
  handleChangeInput,
  handleChangeUserType,
  formAction,
  status,
  changeAuthType,
}) => {
  return (
    <ModalLayout
      style={{ width: '352px', backgroundColor: '#fff', padding: '25px' }}
      header={<Header userType={userType} handleChange={handleChangeUserType} status={status} />}
      content={
        <Credentials
          handleChangeInput={handleChangeInput}
          action={formAction}
          status={status}
          // TODO remove prop socialAuth
          socialAuth={false}
          changeAuthType={changeAuthType}
        />
      }
      footer={<Footer status={status} changeAuthType={changeAuthType} />}
      handleCloseClick={handleCloseClick}
    />
  )
}
