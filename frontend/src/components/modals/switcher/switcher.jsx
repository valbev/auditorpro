import React from 'react'
import { Input } from '../../../assets/components'
import './switcher.scss'

export const Switcher = ({ selected, titles, direction, labelText, handleChange }) => {
  return (
    <div className='switcher'>
      <Input.RadioGroup
        selected={selected}
        titles={titles}
        direction={direction}
        labelText={labelText}
        handleChange={handleChange}
      />
    </div>
  )
}
