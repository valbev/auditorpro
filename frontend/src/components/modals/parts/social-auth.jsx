import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export const SocialAuth = () => (
  <>
    <div className='modal__title'>Войти с помощью:</div>
    <ul className='social-links'>
      <li>
        <a href='/'>
          <FontAwesomeIcon icon={['fab', 'vk']} color='#8355E2' size='2x' />
        </a>
      </li>
      <li>
        <a href='/'>
          <FontAwesomeIcon icon={['fab', 'facebook-f']} color='#8355E2' size='2x' />
        </a>
      </li>
      <li>
        <a href='/'>
          <FontAwesomeIcon icon={['fab', 'yandex']} color='#8355E2' size='2x' />
        </a>
      </li>
      <li>
        <a href='/'>
          <FontAwesomeIcon icon={['fab', 'google']} color='#8355E2' size='2x' />
        </a>
      </li>
      <li>
        <a href='/'>
          <FontAwesomeIcon icon={['fas', 'at']} color='#8355E2' size='2x' />
        </a>
      </li>
    </ul>
  </>
)
