import React, { PureComponent } from 'react'
import { Button, Input } from '../../../assets/components'
import { GENDERS, FIELDS, USER_TYPES } from '../../global-constants'
import { formatDate } from '../../utils'

export class PersonalData extends PureComponent {
  state = {
    phone: '',
    sex: 0,
    date: new Date(),
    address: [],
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { phone, sex, date } = this.state
    const { handleChangeInput } = this.props
    if (prevState.phone !== phone) {
      handleChangeInput({ state: FIELDS.PHONE, value: phone })
    }
    if (prevState.sex !== sex) {
      handleChangeInput({ state: FIELDS.GENDER, value: GENDERS[sex] })
    }
    if (prevState.date !== date) {
      handleChangeInput({ state: FIELDS.BIRTHDAY, value: formatDate(date) })
    }
  }

  render() {
    // TODO onAddressInput from props
    const { handleChangeInput, userType, formAction } = this.props
    const { sex, phone, date } = this.state
    const isSecretGuest = userType === USER_TYPES[0]
    const isBusiness = userType === USER_TYPES[1]

    return (
      <>
        <Input.Text
          labelText='Имя'
          name={FIELDS.FIRST_NAME}
          onChange={(event) => handleChangeInput(event)}
          placeholder='Павел'
        />
        {isSecretGuest && (
          <Input.Text
            labelText='Фамилия'
            name={FIELDS.LAST_NAME}
            onChange={(event) => handleChangeInput(event)}
            placeholder='Павлов'
          />
        )}
        {isBusiness && (
          <Input.Text
            labelText='Должность'
            name={FIELDS.POSITION}
            onChange={(event) => handleChangeInput(event)}
            placeholder='Менеджер'
          />
        )}
        <Input.PhoneInput
          onlyCountries={['ua', 'by', 'ru']}
          placeholder='+7-999-999-99-99'
          masks={{
            ru: '+.-...-...-..-..',
            by: '+...-..-...-..-..',
            ua: '+...-..-...-..-..',
          }}
          labelText='Контактный телефон'
          value={phone}
          onChange={(phone) => this.setState({ phone })}
        />
        {isSecretGuest && (
          <>
            <Input.Date
              labelText='Дата рождения'
              name={FIELDS.BIRTHDAY}
              selected={date}
              handleChange={(date) => this.setState({ date })}
              placeholder='06 июня 1990'
            />
            <Input.Text
              labelText='Адрес проживания'
              name={FIELDS.ADDRESS}
              onChange={(event) => handleChangeInput(event)}
              placeholder='Калининград, Маршала борзова 21'
            />
            <Input.RadioGroup
              labelText='Пол'
              titles={GENDERS}
              direction='row'
              selected={sex}
              handleChange={(id) => this.setState({ sex: id })}
            />
          </>
        )}
        <Button onClick={formAction} block>
          Зарегистрироваться
        </Button>
      </>
    )
  }
}
