import React from 'react'
import { Switcher } from '../switcher'
import { USER_TYPES } from '../../global-constants'
import { getUserTypeId } from '../../utils'

export const Header = ({ userType, status, handleChange }) => (
  <>
    {status === 'regNext' ? (
      <p>Заполните личные данные: </p>
    ) : (
      <Switcher titles={USER_TYPES} direction='row' selected={getUserTypeId(userType)} handleChange={handleChange} />
    )}
  </>
)
