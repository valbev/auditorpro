import React from 'react'
import { Input, Button } from '../../../assets/components'
import { SocialAuth } from './social-auth'

export const Credentials = ({ handleChangeInput, action, socialAuth, status }) => {
  const buttonText = status === 'auth' ? 'Войти' : 'Продолжить регистрацию'
  return (
    <>
      {socialAuth && <SocialAuth />}
      <Input.Text name='email' labelText='Email' onChange={(event) => handleChangeInput(event)} />
      <Input.Password name='password' labelText='Пароль' onChange={(event) => handleChangeInput(event)} />
      {status !== 'auth' && (
        <Input.Password
          name='confirmPassword'
          labelText='Подтвердите пароль'
          onChange={(event) => handleChangeInput(event)}
        />
      )}
      <Button block onClick={action}>
        {buttonText}
      </Button>
    </>
  )
}
