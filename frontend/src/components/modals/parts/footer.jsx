import React from 'react'
import { Button } from '../../../assets/components'

export const Footer = ({ status, changeAuthType }) => (
  <>
    <div className='d-flex justify-content-around mb-3'>
      {status === 'auth' ? (
        <>
          <Button type='link'>Забыли пароль?</Button>
          <Button type='link' onClick={() => changeAuthType('regFirst', 0)}>
            Регистрация
          </Button>
        </>
      ) : (
        <Button type='link' onClick={() => changeAuthType('auth', 0)}>
          Назад к авторизации
        </Button>
      )}
    </div>
    <div className='text-center privacy'>
      Создавая аккаунт, вы соглашаетесь с нашими <Button type='link'>Правилами и условиями</Button>
    </div>
  </>
)
