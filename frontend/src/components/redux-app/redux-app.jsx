import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import { ErrorBoundary } from '../error-boundary'
import { GetSecretServiceProvider } from '../react-context'
import { UserProvider } from '../../context'
import { GetSecretService } from '../../services'
import { Authorization } from '../authorization'
import { App } from '../app'
import { store } from '../../redux/store'

const getSecretService = new GetSecretService()

export const ReduxApp = () => {
  return (
    <Provider store={store}>
      <ErrorBoundary>
        <GetSecretServiceProvider value={getSecretService}>
          <UserProvider>
            <Authorization>
              <Router>
                <App />
              </Router>
            </Authorization>
          </UserProvider>
        </GetSecretServiceProvider>
      </ErrorBoundary>
    </Provider>
  )
}
