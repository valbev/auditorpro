import React from 'react'
import { LeftSide, RightSide } from './parts'
import './header.scss'
import { Route } from 'react-router-dom'
import LeftSideConstructor from './parts/left-side-constructor'

export const Header = () => {
  return (
    <Route
      path='/passing-the-survey'
      children={(props) =>
        props.match ? (
          <></>
        ) : (
          <>
            <header className='header d-flex justify-content-between align-items-center'>
              <Route
                path='/constructor'
                children={(props) => (props.match ? <LeftSideConstructor {...props} /> : <LeftSide {...props} />)}
              />
              <RightSide />
            </header>
          </>
        )
      }
    />
  )
}
