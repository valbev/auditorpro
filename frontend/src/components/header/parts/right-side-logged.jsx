import React, { useContext } from 'react'
import { Dropdown } from 'react-bootstrap'
import { Link } from 'react-router-dom'

import { ICONS } from './icons'
import profileIcon from './images/profileIcon.svg'
import exitIcon from './images/exitIcon.svg'
import '../header.scss'
import { Context } from '../../../context'

export const RightSideLogged = ({ user }) => {
  const { id, firstName, lastName, userType } = user
  const { logout } = useContext(Context)

  if (!userType) {
    return null
  }

  return (
    <>
      <div className='header-info'>
        <Dropdown className='dropdown-btn'>
          <Dropdown.Toggle
            variant='success'
            id='dropdown-basic'
            style={{ color: '#666666', display: 'flex', flexDirection: 'row' }}
          >
            <div className='header-info__logo'>{ICONS[userType]}</div>
            <div
              className='header-info__about'
              style={{ display: 'flex', alignItems: 'flex-start', flexDirection: 'column' }}
            >
              <div className='header-info__name'>{`${firstName}${lastName ? ` ${lastName}` : ''}`}</div>
              <div className='header-info__id'>{`id ${id}`}</div>
            </div>
            <div className='header-info__more arrow arrow-bottom' />
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item href='#/action-1' style={{ borderBottom: '1px solid #DADADA' }}>
              <Link to='/profile' style={{ padding: 0 }}>
                <div className='item-container'>
                  <img src={profileIcon} className='my-profile-image' />
                  <div className='item-text' style={{ marginTop: 2 }}>
                    Мой профиль
                  </div>
                </div>
              </Link>
            </Dropdown.Item>
            <Dropdown.Item href='#/action-2' style={{ border: 'none' }}>
              <div className='item-container' onClick={logout}>
                <img className='my-profile-image' src={exitIcon} />
                <div className='item-text'>Выйти</div>
              </div>
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </>
  )
}
