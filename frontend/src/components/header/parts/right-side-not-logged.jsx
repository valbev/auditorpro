import React from 'react'
import { useDispatch } from 'react-redux'
import { Button } from '../../../assets/components'

import { Actions } from '../../authorization/redux/actions'

export const RightSideNotLogged = () => {
  const dispatch = useDispatch()
  const { toggleModalOpened } = Actions

  const handleOpenAuthorizationModal = () => {
    dispatch(toggleModalOpened('authorizationModal', true))
  }

  const handleShowRegisterFirstStepModal = () => {
    dispatch(toggleModalOpened('registerFirstStepModal', true))
  }

  return (
    <>
      <Button onClick={handleShowRegisterFirstStepModal} type='link'>
        Зарегистрироваться
      </Button>
      <Button onClick={handleOpenAuthorizationModal}>Войти</Button>
    </>
  )
}
