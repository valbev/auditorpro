import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export const ICONS = {
  'secret-guest': <FontAwesomeIcon icon={['fas', 'user-secret']} color='#666666' size='lg' />,
  business: <FontAwesomeIcon icon={['fas', 'user-tie']} color='#666666' size='lg' />,
}
