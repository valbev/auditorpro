import React, { useContext } from 'react'
import { Context } from '../../../context'
import { RightSideLogged } from './right-side-logged'
import { RightSideNotLogged } from './right-side-not-logged'

export const RightSide = () => {
  const { user, logout } = useContext(Context)

  return (
    <div className='header-user d-flex'>
      {user.isLoggedIn ? <RightSideLogged user={user} logout={logout} /> : <RightSideNotLogged />}
    </div>
  )
}
