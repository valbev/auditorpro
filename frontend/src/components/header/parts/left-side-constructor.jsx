import React, { Component } from 'react'
import { ReactComponent as LeftArrow } from '../images/left-arrow.svg'
import { ReactComponent as LinkBtn } from '../images/link.svg'
import { ReactComponent as VkIcon } from '../images/vk-icon.svg'
import { ReactComponent as FbIcon } from '../images/fb-icon.svg'
import { ReactComponent as MailIcon } from '../images/mail-icon.svg'
import { ReactComponent as CopyIcon } from '../images/copy-icon.svg'
import { ReactComponent as QrIcon } from '../images/qr-icon.svg'
import { ReactComponent as CloseIcon } from '../images/modal-close.svg'
import { Input } from '../../../assets/components/input'
import { Button } from '../../../assets/components/button'
import { Link, withRouter } from 'react-router-dom'
import { withGsapiService } from '../../hoc'
import debounce from 'lodash/debounce'
import { ModalConstructor } from '../../pages/constructor/components/modal'
import { ModalFooter, ModalHeader } from 'reactstrap'
import QRCode from 'qrcode.react'
import Snackbar from '@material-ui/core/Snackbar'

import { connect } from 'react-redux'

const InputStyle = {
  maxWidth: 378,
  width: '100%',
  background: '#EDEEF4',
  padding: '5px 8px',
  border: '1px solid #DADADA',
  borderRadius: '3px',
  fontSize: '16px',
  lineHeight: '1.5',
  outline: 'none',
}

@withGsapiService()
@withRouter
class LeftSideConstructor extends Component {
  constructor(props) {
    super(props)
    this.state = {
      checkListName: '',
      idName: null,
      modal: false,
      secondModal: false,
      url: '',
      open: false,
      vertical: 'top',
      horizontal: 'center',
    }

    this.onTextNameChange = debounce(this.onTextNameChange, 500)
  }

  handleOpenModal = () => {
    const { modal } = this.state
    this.setState({
      modal: !modal,
    })
  }

  handleOpenSecondModal = () => {
    const { secondModal } = this.state
    this.setState({
      secondModal: !secondModal,
    })
  }

  async componentDidMount() {
    const { getSecretService, constructorType } = this.props
    const pollId = this.props.location.pathname.split('/')[this.props.location.pathname.split('/').length - 1]
    const imagePollId = localStorage.getItem('id_image_poll')

    const localeType = localStorage.getItem('poll_type', null)
    const type = constructorType || localeType

    if (pollId !== 'constructor') {
      try {
        let response = null
        if (type !== 'full_poll') {
          response = await getSecretService.fetchData({ action: 'getPolls', id: pollId, slug: '-' })
        } else {
          response = await getSecretService.fetchData({ action: 'getFullPoll', id: pollId })
        }
        this.setState({
          idName: pollId,
          checkListName: response.result[0].title,
        })
      } catch (err) {
        this.setState({
          idName: pollId,
          checkListName: '',
        })
      }
    } else {
      this.setState({
        idName: imagePollId,
      })
    }
  }

  handleChangeName = (event) => {
    let imageId = null
    const pollId = this.props.location.pathname.split('/')[this.props.location.pathname.split('/').length - 1]
    const imagePollId = localStorage.getItem('id_image_poll')
    if (pollId !== 'constructor') {
      this.setState({
        idName: pollId,
      })
      imageId = pollId
    } else {
      this.setState({
        idName: Number(imagePollId),
      })
      imageId = Number(imagePollId)
    }

    this.setState(
      {
        checkListName: event.target.value,
      },
      async () => {
        const data = {
          title: this.state.checkListName,
        }
        this.onTextNameChange(imageId, data)
      },
    )
  }

  writeToClipboard = async (text) => {
    try {
      if (navigator.clipboard) {
        await navigator.clipboard.writeText(text)
        // const bufferText = await navigator.clipboard.readText()
        this.showPopUp()
      } else {
        this.selectUrl()
      }
    } catch (error) {
      alert(`Разрешите доступ к буферу обмена в браузере', ${error}`)
    }
  }

  selectUrl = () => {
    const input = document.getElementById('copy-link')
    input.select()
    document.execCommand('copy')
    this.showPopUp()
  }

  showPopUp = () => {
    this.setState(
      {
        open: true,
      },
      () => {
        setTimeout(() => {
          this.handleClose()
        }, 1500)
      },
    )
  }

  handleClose = () => {
    this.setState({
      open: false,
    })
  }

  onTextNameChange = (id, data) => {
    const { getSecretService, constructorType } = this.props
    const localeType = localStorage.getItem('poll_type', null)
    const type = constructorType || localeType

    if (data.title !== '') {
      if (type === 'full_poll') {
        // TODO delete test params after update API
        const params = {
          title: data.title,
          test_mode_global: false,
        }
        getSecretService.fetchData({ action: 'updateFullPoll', id }, params)
      } else {
        getSecretService.fetchData({ action: 'updateImagePoll', id }, data)
      }
    }
  }

  render() {
    const { checkListName, modal, secondModal, vertical, horizontal, open } = this.state
    const { constructorId, constructorType, location } = this.props
    const pollId = location.pathname.split('/')[location.pathname.split('/').length - 1]
    const idPoll = pollId !== 'constructor' ? pollId : constructorId || ''

    let link = ''
    const localeType = localStorage.getItem('poll_type', null)
    const type = constructorType || localeType
    const strnigLink = type === 'full_poll' ? 'completing-survey' : 'passing-the-survey'
    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
      link = `http://localhost:3000/${strnigLink}/${idPoll}`
    } else {
      link = `${process.env.REACT_APP_FRONTEND}/${strnigLink}/${idPoll}`
    }
    return (
      <div className='header-constructor header-company d-flex align-items-center'>
        <ModalConstructor
          modal={modal}
          handleOpenModal={this.handleOpenModal}
          toggleConstructorType={this.toggleConstructorType}
        >
          <ModalHeader className='d-flex justify-content-end'>
            <Button type='icon' onClick={() => this.handleOpenModal()}>
              <CloseIcon />
            </Button>
          </ModalHeader>
          <div className='modal-input_label'>Ссылка на чек-лист</div>
          <div className='modal-body-custom'>
            <Input.Text id='copy-link' name={checkListName} value={link} readOnly style={InputStyle} />
            <Snackbar
              anchorOrigin={{ vertical, horizontal }}
              open={open}
              onClose={this.handleClose}
              message='Ссылка скопирована в буфер обмена'
              key={vertical + horizontal}
            />
            <div className='d-flex flex-direction-row'>
              <div className='preview-panel_button cursor-pointer' onClick={() => this.writeToClipboard(link)}>
                <CopyIcon />
              </div>
              <div className='preview-panel_button' onClick={this.handleOpenSecondModal}>
                <QrIcon />
                <ModalConstructor className='qr-modal' modal={secondModal} handleOpenModal={this.handleOpenSecondModal}>
                  <QRCode style={{ margin: 20, marginLeft: '25%' }} value={`${link}`} size={250} />
                </ModalConstructor>
              </div>
            </div>
          </div>
          <ModalFooter className='modal-link-footer'>
            <VkIcon />
            <FbIcon className='fb-icon' />
            <MailIcon />
          </ModalFooter>
        </ModalConstructor>
        <Link to='/check-lists' className='preview-panel_button'>
          <LeftArrow />
        </Link>
        <div className='header-city' style={{ width: 250 }}>
          <Input.Text
            name={checkListName}
            placeholder={`Название чек листа № ${idPoll}`}
            value={checkListName}
            onChange={(event) => this.handleChangeName(event)}
          />
        </div>
        <Button type='icon' className='preview-panel_button mr-3' onClick={this.handleOpenModal}>
          <LinkBtn />
        </Button>
        <Button>Сохранить</Button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    constructorId: state.constructor.constructorId,
    constructorType: state.constructor.constructorType,
  }
}

export default connect(mapStateToProps, null)(LeftSideConstructor)
