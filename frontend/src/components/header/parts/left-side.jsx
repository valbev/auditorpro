import React from 'react'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import logo from '../images/logo-temp.png'

export const LeftSide = () => {
  return (
    <div className='header-company d-flex'>
      <div className='logo'>
        <Link to='/'>
          <img src={logo} alt='Логотип' />
        </Link>
      </div>
      <div className='header-city'>
        <FontAwesomeIcon icon={['fas', 'map-marker-alt']} color='#666666' size='sm' />
        <span>Калининград</span>
      </div>
      <a href='tel:+78009999999' className='header-phone hover-grey'>
        <FontAwesomeIcon icon={['fas', 'phone']} color='#666666' size='sm' />
        <span>8 800 999 99 99</span>
      </a>
    </div>
  )
}
