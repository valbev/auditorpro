import React, { Component } from 'react'
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas, faCoffee, faStar as fasStar } from '@fortawesome/free-solid-svg-icons'
import { far, faStar as farStar, faCalendarAlt } from '@fortawesome/free-regular-svg-icons'
import { library } from '@fortawesome/fontawesome-svg-core'
import { UiKit } from '../ui-kit'
import { Header } from '../header'
import { Content } from '../content'
import * as Pages from '../pages'
import { Menu, Auth } from '../dev-components'
import './app.scss'
import Constructor from '../pages/constructor/constructor'
import CheckLists from '../pages/check-lists/check-lists'
import CompletingSurvey from '../pages/completing-survey/completing-survey'

library.add(fab, fas, far, farStar, fasStar, faCoffee, faCalendarAlt)

@withRouter
class App extends Component {
  render() {
    const link = this.props.location.pathname.split('/')[this.props.location.pathname.split('/').length - 2]
    return (
      <div className='application'>
        <Route
          path={['/completing-survey', '/passing-the-survey']}
          children={(props) => {
            return props.match ? (
              // eslint-disable-next-line no-constant-condition
              props.match.path === '/passing-the-survey' || '/completing-survey' ? (
                <></>
              ) : (
                <Header />
              )
            ) : props.match ? (
              <></>
            ) : (
              <Header />
            )
          }}
        />

        <Content style={{ paddingTop: link === 'completing-survey' ? 0 : 57 }}>
          <Switch>
            <Route path='/' exact render={() => <Pages.Home />} />
            <Route path='/ui-kit' render={() => <UiKit />} />
            <Route path='/profile' component={() => <Pages.Profile />} />
            <Route path='/company' render={() => <Pages.Company />} />
            <Route path='/create-company' render={() => <Pages.EditableCompany />} />
            <Route path='/temp-auth' render={() => <Auth />} />
            <Route path='/login' render={() => <Pages.LoginPage />} />
            <Route path='/main-tabs' render={() => <Pages.MainTabs />} />
            <Route path='/completing-survey/:id' render={() => <CompletingSurvey />} />
            <Route path='/check-lists' component={() => <CheckLists />} />
            <Route path='/image-answers/:id' component={() => <Pages.ImagePollAnswers />} />
            <Route path='/page-not-found' render={() => <Pages.PageNotFount />} />
            <Route path='/constructor/:id?' component={() => <Constructor />} />
            <Route path='/create-poll' render={() => <Pages.CreatePoll />} />
            <Route path='/passing-the-survey/:id' render={() => <Pages.QuestionPage />} />
            <Route path='/forms-list' render={() => <Pages.FormsList />} />
            <Route path='/landing/:token' render={() => <Pages.LandingVtb />} />
            <Route path='/respondProfile' render={() => <Pages.RespondProfile />} />
            <Route path='/unauthorized' render={() => <Pages.Unauthorized />} />
            <Route path='/settings' render={() => <Pages.Settings />} />
            <Route path='*'>
              <Redirect to='/page-not-found' />
            </Route>
          </Switch>
        </Content>
        <Route
          path={['/constructor', '/completing-survey', '/passing-the-survey']}
          children={(props) => {
            return props.match ? (
              // eslint-disable-next-line no-constant-condition
              props.match.path === '/constructor' || '/passing-the-survey' || '/completing-survey' ? (
                <></>
              ) : (
                <Menu {...props} />
              )
            ) : props.match ? (
              <></>
            ) : (
              <Menu {...props} />
            )
          }}
        />
      </div>
    )
  }
}

export { App }
