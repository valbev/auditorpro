import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { links } from './links'
import arrow from './images/arrow.png'

import './sidebar.scss'

export class Sidebar extends Component {
  state = {
    opened: false,
    isAnimating: false,
  }

  animationStatus = () => this.setState({ isAnimating: false })

  toggleSidebar = () => this.setState(({ opened }) => ({ opened: !opened, isAnimating: true }))

  render() {
    const { opened, isAnimating } = this.state
    const expandLink = opened && !isAnimating

    return (
      <div className='sidebar' style={{ width: opened ? '195px' : '56px' }} onTransitionEnd={this.animationStatus}>
        <div className='sidebar-wrapper'>
          <div className='sidebar-wrapper'>
            <ul className='sidebar-list d-flex flex-column'>
              {links.map(({ icon, href, text }, i) => (
                <li key={i} className='sidebar-list-item'>
                  <Link to={href} className={expandLink ? '' : 'justify-content-center'}>
                    <span className={expandLink ? 'mr-2' : ''}>{icon}</span>
                    {expandLink ? <span>{text}</span> : null}
                  </Link>
                </li>
              ))}
            </ul>
          </div>

          <div className='sidebar-toggle'>
            <button onClick={this.toggleSidebar}>
              <span>
                <img src={arrow} alt='toggle' style={{ transform: opened ? 'rotate(180deg)' : '' }} />
              </span>
            </button>
          </div>
        </div>
      </div>
    )
  }
}
