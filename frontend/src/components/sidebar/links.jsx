import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import usersSecret from './images/usersSecret.svg'

export const links = [
  {
    icon: <FontAwesomeIcon icon={['fas', 'chart-bar']} color='#ffffff' size='lg' />,
    href: '/',
    text: 'Аналитика',
  },
  {
    icon: <FontAwesomeIcon icon={['fas', 'building']} color='#ffffff' size='lg' />,
    href: '/',
    text: 'Мои компании',
  },
  {
    icon: <FontAwesomeIcon icon={['fas', 'clipboard-check']} color='#ffffff' size='lg' />,
    href: '/',
    text: 'Чек листы',
  },
  {
    icon: <FontAwesomeIcon icon={['fas', 'calendar-alt']} color='#ffffff' size='lg' />,
    href: '/',
    text: 'График проверок',
  },
  {
    icon: <FontAwesomeIcon icon={['far', 'file-alt']} color='#ffffff' size='lg' />,
    href: '/',
    text: 'Отчеты/Заявки',
  },
  {
    icon: <FontAwesomeIcon icon={['fas', 'receipt']} color='#ffffff' size='lg' />,
    href: '/',
    text: 'Генератор купонов',
  },
  {
    icon: <FontAwesomeIcon icon={['fas', 'users']} color='#ffffff' size='lg' />,
    href: '/',
    text: 'Пользователи',
  },
  {
    icon: <img src={usersSecret} alt='' />,
    href: '/',
    text: 'База Тайных гостей',
  },
  {
    icon: <FontAwesomeIcon icon={['fas', 'user-cog']} color='#ffffff' size='lg' />,
    href: '/',
    text: 'Настройка профиля',
  },
  {
    icon: <FontAwesomeIcon icon={['fas', 'sign-out-alt']} color='#ffffff' size='lg' />,
    href: '/',
    text: 'Выход',
  },
]
