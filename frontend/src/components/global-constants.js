export const API_BASE = process.env.REACT_APP_API_ENDPOINT
export const SECOND_API_BASE = process.env.REACT_APP_API_ENDPOINT

export const SERVER = process.env.REACT_APP_API_SERVER

export const USER_TYPES = ['secret-guest', 'business']
export const GENDERS = ['male', 'female']
export const FIELDS = {
  EMAIL: 'email',
  PASSWORD: 'password',
  CONFIRM_PASSWORD: 'confirmPassword',
  USER_TYPE: 'type',
  FIRST_NAME: 'first_name',
  POSITION: 'position',
  PHONE: 'phone',
  LAST_NAME: 'last_name',
  BIRTHDAY: 'birthday',
  ADDRESS: 'address',
  GENDER: 'sex',
}

export const REGISTER_FIELDS = {
  'secret-guest': ['email', 'password', 'type', 'first_name', 'phone', 'last_name', 'birthday', 'address', 'sex'],
  business: ['email', 'password', 'type', 'first_name', 'position', 'phone'],
}

export const CHECK_INDUSTRIES = [
  'Автосалон',
  'Агентство недвижимости',
  'АЗС',
  'Банк (Финансовые учреждения)',
  'Ресторан и кафе',
  'Закусочные и фаст-фуды',
  'Мебельные салоны',
  'Магазины одежды и обуви',
  'Магазины электроники',
  'Парфюмерия, косметика',
  'Салоны красоты (Мед. учреждения)',
  'Супермаркеты',
  'Страховые компании',
  'Ювелирные салоны',
  'Кинотеатр, боулинг',
  'Книжный магазин',
  'Салон сотовой связи',
  'Оптика',
  'Аптека',
  'Новостройка',
  'Гостиницы',
  'Туристические агентства',
  'Спортивные клубы',
  'Колл-центры',
  'Государственные учреждения',
  'Другое',
]
