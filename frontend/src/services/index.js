import { ApiClient } from './api-client'
const { REACT_APP_API_ENDPOINT } = process.env

export const api = new ApiClient(REACT_APP_API_ENDPOINT, true)

export { GetSecretService } from './gsapi-service'
export { endpointPrefixes } from './endpoint-prefixes'
