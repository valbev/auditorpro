export const endpointPrefixes = {
  authentication: '/authentication/token',
  users: '/users',
}
