export const strUTCTime = (date) => {
  let strTime = ''
  const strHours = date.getUTCHours()
  const strMinutes = date.getUTCMinutes()
  const strSeconds = date.getUTCSeconds()
  strTime += strHours > 0 ? `${strHours}:` : ''
  strTime += strMinutes > 9 ? `${strMinutes}:` : strMinutes > 0 ? `0${strMinutes}:` : '0:'
  strTime += strSeconds > 9 ? strSeconds : strSeconds > 0 ? `0${strSeconds}` : '00'
  return strTime
}

export const Visible = function(target, wrapper) {
  // Все позиции элемента
  const targetPosition = {
    // top: window.pageYOffset + target.getBoundingClientRect().top, top: window.pageYOffset + target.getBoundingClientRect().top, top: window.pageYOffset + target.getBoundingClientRect().top,
    // left: window.pageXOffset + target.getBoundingClientRect().left,
    // right: window.pageXOffset + target.getBoundingClientRect().right,
    // bottom: window.pageYOffset + target.getBoundingClientRect().bottom
    top: target.getBoundingClientRect().top,
    // left: target.getBoundingClientRect().left,
    // right: target.getBoundingClientRect().right,
    bottom: target.getBoundingClientRect().bottom,
  }
  // Получаем позиции окна
  // windowPosition = {
  // top: window.pageYOffset,
  // left: window.pageXOffset,
  // right: window.pageXOffset + document.documentElement.clientWidth,
  // bottom: window.pageYOffset + document.documentElement.clientHeight
  // };

  // Получаем позиции wrapper
  const wrapperPosition = {
    // top: window.pageYOffset,
    // left: window.pageXOffset,
    // right: window.pageXOffset + document.documentElement.clientWidth,
    // bottom: window.pageYOffset + document.documentElement.clientHeight
    top: wrapper.getBoundingClientRect().top,
    // left: wrapper.getBoundingClientRect().left,
    // right: wrapper.getBoundingClientRect().right,
    bottom: wrapper.getBoundingClientRect().bottom,
  }

  // if (targetPosition.bottom > wrapperPosition.top &&
  // Если позиция нижней части элемента больше позиции верхней чайти окна, то элемент виден сверху
  //   targetPosition.top < windowPosition.bottom && // Если позиция верхней части элемента меньше позиции нижней чайти окна, то элемент виден снизу
  //   targetPosition.right > windowPosition.left && // Если позиция правой стороны элемента больше позиции левой части окна, то элемент виден слева
  //   targetPosition.left < windowPosition.right) { // Если позиция левой стороны элемента меньше позиции правой чайти окна, то элемент виден справа
  // Если позиция нижней части элемента больше позиции верхней чайти окна, то элемент виден сверху
  if (targetPosition.bottom <= wrapperPosition.bottom && targetPosition.top >= wrapperPosition.top) {
    // && // Если позиция верхней части элемента меньше позиции нижней чайти окна, то элемент виден снизу
    // targetPosition.right > wrapperPosition.left && // Если позиция правой стороны элемента больше позиции левой части окна, то элемент виден слева
    // targetPosition.left < wrapperPosition.right) { // Если позиция левой стороны элемента меньше позиции правой чайти окна, то элемент виден справа
    // Если элемент полностью видно, то запускаем следующий код
    // console.clear();
    // console.log('Вы видите элемент :)')
    return true
  } else {
    // Если элемент не видно, то запускаем этот код
    // console.clear();
    // console.log('targetPosition', targetPosition)
    // console.log('wrapperPosition', wrapperPosition)
    // console.log('target.getBoundingClientRect()', target.getBoundingClientRect())
    return false
  }
}
