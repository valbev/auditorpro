export const routes = {
  register: {
    url: '/users/registration/',
    method: 'POST',
  },
  obtainToken: {
    url: '/authentication/token/obtaining/',
    method: 'POST',
  },
  refreshToken: {
    url: '/authentication/token/refreshing/',
    method: 'POST',
  },
  verifyToken: {
    url: '/authentication/token/verification/',
    method: 'POST',
  },
  checkAddress: {
    url: '/users/check_address/',
    method: 'GET',
  },
  getUserData: {
    url: '/users/',
    method: 'GET',
  },
  updateUserData: {
    url: '/users/',
    method: 'POST',
  },
  deleteUser: {
    url: '/users/',
    method: 'DELETE',
  },
  uploadImage: {
    url: '/misc/upload-image/',
    method: 'POST',
  },
  getPolls: {
    url: '/image-polls/',
    method: 'GET',
  },
  createFullConstructor: {
    url: '/polls/',
    method: 'POST',
  },
  updateFullPoll: {
    url: '/polls/poll/',
    method: 'PUT',
  },
  getFullConstructorPolls: {
    url: '/polls/',
    method: 'GET',
  },
  deleteFullPoll: {
    url: '/polls/poll/',
    method: 'DELETE',
  },
  createPollQuestion: {
    firstUrl: '/polls/',
    secondUrl: '/questions/',
    method: 'POST',
  },
  getFullPoll: {
    url: '/polls/poll/',
    method: 'GET',
  },
  deleteFullPollQuestion: {
    url: '/polls/questions/',
    method: 'DELETE',
  },
  editFullQuestion: {
    url: '/polls/questions/',
    method: 'PUT',
  },
  createQuestionPoll: {
    url: '/polls/questions/items/',
    method: 'POST',
  },
  editQuestionPoll: {
    url: '/polls/questions/items/',
    method: 'PUT',
  },
  deleteAnswerFullPoll: {
    url: '/polls/questions/items/',
    method: 'DELETE',
  },
}
