import { getToken } from './utils'

export class ApiClient {
  constructor(apiBase, auth = true) {
    this.headers = {
      'Content-Type': 'application/json',
    }

    this.fetch = async (...args) => {
      const token = getToken()
      if (auth && token) {
        this.headers.Authorization = `JWT ${token}`
      }
      return await fetch(...args)
    }

    this._apiBase = apiBase
  }

  get = async ({ endpoint, params }) => {
    try {
      const res = await this.fetch(`${this._apiBase}${endpoint}/`, {
        method: 'GET',
        headers: this.headers,
        ...params,
      })
      const json = await res.json()
      if (!res.ok) {
        throw Error(json.non_field_errors)
      }
      return json
    } catch (e) {
      // TODO error handler
      console.error(`Error text: ${e}`)
    }
  }

  post = async ({ endpoint, data, params }) => {
    try {
      const res = await this.fetch(`${this._apiBase}${endpoint}/`, {
        method: 'POST',
        headers: this.headers,
        body: JSON.stringify(data),
        ...params,
      })
      const json = await res.json()
      if (!res.ok) {
        throw Error(json.non_field_errors)
      }
      return json
    } catch (e) {
      // TODO error handler
      console.error(`Error text: ${e}`)
    }
  }
}
