import { useCallback } from 'react'

export const getToken = () => {
  const user = localStorage.getItem('user')
  if (user) {
    const { token } = JSON.parse(user || {})
    if (token) {
      return token
    }
  }
  return null
}

/**
 * Хук для валидации
 * validateField - функция, которая возвращает текст ошибки валидации, если она есть
 * validateAll - функция, которая возвращает объект ошибок валидации
 * validation - объект, где ключами являются имена полей формы, а их значениями
 * функции валидации
 */

export const useValidation = (validation) => {
  const validateField = useCallback(
    (type, value, data) => {
      return typeof validation[type] === 'function' ? validation[type](value, data) : ''
    },
    [validation],
  )

  const validateAll = useCallback(
    (data) => {
      const res = Object.entries(data).reduce((acc, [key, value]) => {
        acc[key] = validateField(key, value, data)
        return acc
      }, {})
      return res
    },
    [validation],
  )

  const hasErrors = useCallback((errorsObj) => {
    return Object.values(errorsObj).some((errorText) => Boolean(errorText))
  }, [])

  return { validateField, validateAll, hasErrors }
}
