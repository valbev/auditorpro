import { applyMiddleware, createStore } from 'redux'
import { rootReducer } from './reducer'
import { composeWithDevTools } from 'redux-devtools-extension'
// import rootSaga from './saga'
import createSagaMiddleware from 'redux-saga'

const sagaMiddleware = createSagaMiddleware()
const middleware = [sagaMiddleware]
let store
if (process.env.REACT_APP_DEBUG === 'true') {
  store = createStore(rootReducer, composeWithDevTools(applyMiddleware(...middleware)))
} else {
  store = createStore(rootReducer, applyMiddleware(...middleware))
}
// sagaMiddleware.run(rootSaga)
export { store }
