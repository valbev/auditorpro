import { combineReducers } from 'redux'
import { authorizationReducer } from '../components/authorization/redux/reducer'
import { constructorReducer } from '../components/pages/constructor/redux/reducer'

const appReducer = combineReducers({
  authorization: authorizationReducer,
  constructor: constructorReducer,
})

export const rootReducer = (state, action) => {
  return appReducer(state, action)
}
