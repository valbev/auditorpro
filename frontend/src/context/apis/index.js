import { endpointPrefixes, api } from '../../services'

const { authentication, users } = endpointPrefixes

export const obtainToken = async (data) => {
  return await api.post({
    endpoint: `${authentication}/obtaining`,
    data,
  })
}

export const refreshToken = async (data) => {
  return await api.post({
    endpoint: `${authentication}/refreshing`,
    data,
  })
}

export const verifyToken = async (data) => {
  return await api.post({
    endpoint: `${authentication}/verification`,
    data,
  })
}

export const registerUser = async (data) => {
  return await api.post({
    endpoint: `${users}/registration`,
    data,
  })
}

export const changePassword = async (id, data) => {
  return await api.post({
    endpoint: `${users}/${id}/password`,
    data,
  })
}

export const changeEmail = async (id, data) => {
  return await api.post({
    endpoint: `${users}/${id}/email`,
    data,
  })
}

export const getCityFromAddress = async (address) => {
  return await api.get({
    endpoint: `${users}/check_address/${address}/true`,
  })
}

export const getUserByToken = async () => {
  return await api.get({
    endpoint: `${users}/get_by_token`,
  })
}

export const getUserById = async (token, id) => {
  return await api.get({
    endpoint: `${users}/${id}`,
    params: {
      headers: {
        Authorization: `JWT ${token}`,
      },
    },
  })
}

export const loginWithSoc = async (data) => {
  return await api.post({
    endpoint: `${users}/login_with_soc`,
    data,
  })
}
