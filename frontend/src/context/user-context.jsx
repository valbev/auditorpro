import React, { createContext, useEffect, useState } from 'react'
import { useGoogleLogout } from 'react-google-login'

import { keysToCamel } from '../helpers'
import * as api from './apis'

export const Context = createContext()

const user = localStorage.getItem('user')
const initialState = {
  user: (user && JSON.parse(user)) || {},
}

export const UserProvider = ({ children }) => {
  const [user, setUser] = useState(initialState.user)

  const { signOut: signOutFromGoogle } = useGoogleLogout({
    clientId: '589270471984-lvja2c09vif54lqim16hiobpmhnu7htr.apps.googleusercontent.com',
    cookiePolicy: 'single_host_origin',
  })

  const handleSetUser = (data) => {
    localStorage.setItem('user', JSON.stringify(data))
    setUser(data)
  }

  const handleLogOut = () => {
    signOutFromGoogle()
    handleSetUser({})
  }

  const handleObtainToken = async (data) => {
    const response = await api.obtainToken(data)

    console.log('handleObtainToken', response)

    if (response) {
      handleSetUser({ isLoggedIn: true, ...keysToCamel(response) })
    }
  }

  const handleVerifyToken = async () => {
    const { token } = user
    const response = await api.verifyToken({ token })
    if (!response) {
      handleLogOut()
    }
  }

  const handleGetUserData = async () => {
    const response = await api.getUserByToken()
    if (response) {
      handleSetUser({
        ...user,
        ...keysToCamel(response.result),
      })
    }
  }

  const handleRegister = async (data) => {
    const response = await api.registerUser(data)
    if (response) {
      const { email, password } = data
      handleObtainToken({ email, password })
    }
  }

  const handleLoginWithSoc = async (data) => {
    const response = await api.loginWithSoc(data)

    if (response) {
      handleSetUser({ isLoggedIn: true, ...keysToCamel(response) })
    }
  }

  useEffect(() => {
    if (user.token) {
      handleVerifyToken()
    } else {
      handleLogOut()
    }
  }, [])

  useEffect(() => {
    if (user.token) {
      handleGetUserData()
    }
  }, [user.token])

  return (
    <Context.Provider
      value={{
        user,
        login: handleObtainToken,
        logout: handleLogOut,
        register: handleRegister,
        loginWithSoc: handleLoginWithSoc,
      }}
    >
      {children}
    </Context.Provider>
  )
}

export const UserConsumer = Context.Consumer
