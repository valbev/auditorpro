import React from 'react'

export const TabsContent = ({ content }) => {
  return <div className='tabs-content'>{content}</div>
}
