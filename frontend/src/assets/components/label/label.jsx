import React from 'react'

export const Label = ({ labelText, children }) => {
  return (
    <label>
      <span>{labelText}</span>
      {children}
    </label>
  )
}
