import React, { Component } from 'react'
import './slider.scss'

export class Slider extends Component {
  state = {
    activeSlide: 0,
    slides: this.props.slides || [],
  }

  changeSlide = (param) => {
    const { slides } = this.state

    this.setState(({ activeSlide }) => {
      let newValue = activeSlide + param

      if (newValue > slides.length - 1) {
        newValue = 0
      }
      if (newValue < 0) {
        newValue = slides.length - 1
      }

      return { activeSlide: newValue }
    })
  }

  render() {
    const { slides, activeSlide } = this.state

    return (
      <div id='carouselExampleControls' className='carousel slide' data-ride='carousel'>
        <div className='carousel-inner'>
          {slides.map((slide, i) => (
            <div key={i} className={`carousel-item${activeSlide === i ? ' active' : ''}`}>
              <img src={slide.src} className='d-block w-100' alt={slide.src} />
            </div>
          ))}
        </div>
        <button className='carousel-control-prev' onClick={() => this.changeSlide(-1)}>
          <span className='carousel-control-prev-icon' />
          <span className='sr-only'>Previous</span>
        </button>
        <button className='carousel-control-next' onClick={() => this.changeSlide(1)}>
          <span className='carousel-control-next-icon' />
          <span className='sr-only'>Next</span>
        </button>
      </div>
    )
  }
}
