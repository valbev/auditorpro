import React, { Component } from 'react'
import './tooltip.scss'

export class Tooltip extends Component {
  state = {
    opened: false,
  }

  showTooltip = () => this.setState({ opened: true })
  hideTooltip = () => this.setState({ opened: false })

  render() {
    const { content, position = 'left' } = this.props
    const { opened } = this.state

    return (
      <div className='tooltips'>
        <div className='tooltips-icon'>
          <span className='tooltips-question' onMouseEnter={this.showTooltip} onMouseLeave={this.hideTooltip}>
            ?
          </span>
          {opened ? (
            <div className={`tooltips-content${position === 'left' ? ' tooltips-content--left' : ''}`}>{content}</div>
          ) : null}
        </div>
      </div>
    )
  }
}
