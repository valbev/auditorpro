import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Slider } from '../slider'
import './card.scss'

export class Card extends Component {
  state = {
    favorite: this.props.favorite,
    slides: this.props.slides,
  }

  toggleFavorite = () => {
    this.setState(({ favorite }) => ({ favorite: !favorite }))
  }

  renderIcon = (status) => {
    switch (status) {
      case 'new':
        return <span className='indicator__icon--new'>new</span>
      case 'common':
        return (
          <span className='indicator__icon--ideal'>
            <FontAwesomeIcon icon={['fas', 'pencil-alt']} color='#DA9D00' size='xs' />
          </span>
        )
      case 'top':
        return (
          <span className='indicator__icon--fire'>
            <FontAwesomeIcon icon={['fas', 'fire-alt']} color='#B60202' size='xs' />
          </span>
        )
      default:
        return null
    }
  }

  render() {
    const { favorite, slides } = this.state
    const { seen, status = null } = this.props

    return (
      <div className={`card ${seen ? 'card--seen' : ''}`}>
        <div className='card-header d-flex justify-content-between'>
          <span className='card-header__text indicator'>
            Card header
            <span className='indicator__icon'>{this.renderIcon(status)}</span>
          </span>
          <span className='card-header__favorite' onClick={this.toggleFavorite}>
            <FontAwesomeIcon icon={[favorite ? 'fas' : 'far', 'star']} color='#8355E2' size='1x' />
          </span>
        </div>
        <div className='card-slider'>
          <Slider slides={slides} />
          <div className='card-slider--seen'>Просмотрено</div>
        </div>
        <div className='card-content'>
          <div className='card-content__title'>
            <span className='card__icon'>
              <FontAwesomeIcon icon={['fas', 'money-bill-alt']} color='#8355E2' size='1x' />
            </span>
            Купон на пиццу
          </div>
          <div className='card-content__body'>
            <span className='card__icon'>
              <FontAwesomeIcon icon={['fas', 'map-marker-alt']} color='#8355E2' size='1x' />
            </span>
            <p>Литовский вал 2</p>
          </div>
          <div className='card-content__body'>
            <span className='card__icon'>
              <FontAwesomeIcon icon={['fas', 'briefcase']} color='#8355E2' size='1x' />
            </span>
            <p>Рестораны, Кинотеатр, Боулинг</p>
          </div>
        </div>
        <div className='card-footer'>
          <span className='card__icon'>
            <FontAwesomeIcon icon={['far', 'clock']} color='#8355E2' size='sm' />
          </span>
          <ul className='card__dates'>
            <li className='card__date'>20 окт.</li>
            <li className='card__date'>21 окт.</li>
            <li className='card__date'>23 окт.</li>
          </ul>
        </div>
      </div>
    )
  }
}
