import React from 'react'
import './overlay.scss'

export const Overlay = ({ children }) => {
  return <div className='overlay'>{children}</div>
}
