/* eslint-disable no-return-assign */
import React, { Component } from 'react'
import { createPortal } from 'react-dom'
import { POSITION_STYLES } from './constants'
import './portal-legacy.scss'

export class PortalLegacy extends Component {
  state = {
    container: document.createElement('div'),
    root: document.querySelector('#root'),
  }

  componentDidMount() {
    const { container, root } = this.state
    const { position = 'center', style } = this.props
    const portalStyle = {
      ...POSITION_STYLES[position],
      ...style,
    }

    container.classList.add('portal')

    Object.keys((key) => (container.style[key] = portalStyle[key]))

    root.appendChild(container)

    document.addEventListener('keydown', this.onKeyPress)
  }

  componentWillUnmount() {
    this.state.root.removeChild(this.state.container)
    document.removeEventListener('keydown', this.onKeyPress)
  }

  onKeyPress = (event) => {
    if (event.keyCode === 27) {
      this.removePortal()
    }
  }

  removePortal = () => {
    this.props.handleClose()
  }

  render() {
    const { container } = this.state
    const { children } = this.props

    return createPortal(<>{children}</>, container)
  }
}
