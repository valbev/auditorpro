import React from 'react'
import './outline-group.scss'

export const OutlineGroup = ({ title, children }) => {
  return (
    <div className='outline-group'>
      <div className='outline-group-title'>{title}</div>
      <div className='outline-group-content'>{children}</div>
    </div>
  )
}
