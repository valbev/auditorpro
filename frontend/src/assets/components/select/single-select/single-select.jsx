import React, { Component } from 'react'
import { Label } from '../../label'
import { SelectOutput } from '../parts/select-output'
import { Dropdown } from '../parts/dropdown'
import '../select.scss'

export class SingleSelect extends Component {
  state = {
    itemList: [],
    selectedItem: -1,
    isOpened: false,
  }

  componentDidMount() {
    const { selectedItem = -1, itemList = [] } = this.props
    this.setState({
      selectedItem,
      itemList: [...itemList].sort((a, b) => a - b),
    })
  }

  handleClose = (event) => {
    if (event.key === 'Escape') {
      this.toggleDropdown()
    }
  }

  onSelectItem = (id) => {
    const { onChange, name = '' } = this.props
    this.setState(
      { selectedItem: id },
      () => onChange && onChange(this.state.selectedItem, name), // TODO line 32: refactoring
    )
    this.toggleDropdown()
  }

  toggleDropdown = () => {
    this.setState(({ isOpened }) => ({ isOpened: !isOpened })) // TODO create two functions: openDropdown and closeDropdown
  }

  render() {
    const { labelText, disabled } = this.props
    const { itemList, isOpened, selectedItem } = this.state
    const selectedValue = itemList[selectedItem]

    return (
      <div className='select'>
        <Label labelText={labelText}>
          <SelectOutput
            disabled={disabled}
            toggleDropdown={this.toggleDropdown}
            value={selectedValue}
            imgInvert={isOpened}
          />
        </Label>
        {isOpened && (
          <Dropdown
            top={this.props.top}
            itemList={itemList}
            onSelectItem={this.onSelectItem}
            handleClose={this.handleClose}
          />
        )}
      </div>
    )
  }
}
