import React from 'react'
import classNames from 'classnames'
import { Button } from '../../../button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './select-output.scss'

export const SelectOutput = ({ value, toggleDropdown, disabled, imgInvert }) => (
  <div className={classNames('d-flex', 'output', { disabled })}>
    <div className='output-text'>{value}</div>
    <div className='dropdown-button'>
      <Button type='icon' onClick={toggleDropdown} disabled={disabled}>
        <FontAwesomeIcon icon={['fas', 'angle-down']} color='#666666' size='1x' flip={imgInvert ? 'vertical' : null} />
        {/* <img src={dropdownIcon} alt='V' style={{transform: imgInvert ? 'rotateZ(180deg)' : 'none'}} /> */}
      </Button>
    </div>
  </div>
)
