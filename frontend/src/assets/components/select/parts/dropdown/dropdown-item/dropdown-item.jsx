import React from 'react'
import './dropdown-item.scss'

export const DropdownItem = ({ item, onSelectItem }) => {
  return <li onClick={onSelectItem}>{item}</li>
}
