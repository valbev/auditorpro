import React, { Component } from 'react'
import { DropdownItem } from './dropdown-item'
import './dropdown.scss'

export class Dropdown extends Component {
  componentDidMount() {
    document.addEventListener('keydown', this.props.handleClose)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.props.handleClose)
  }

  render() {
    const { itemList, onSelectItem, top } = this.props

    return (
      <ul className='select__dropdown' style={{ top: top || 34 }}>
        {itemList.map((item, id) => (
          <DropdownItem key={item} item={item} onSelectItem={() => onSelectItem(id)} />
        ))}
      </ul>
    )
  }
}
