import React, { Component } from 'react'
import { SingleSelect } from './single-select'
import { MultipleSelect } from './multiple-select'

export class Select extends Component {
  static Single = SingleSelect
  static Multiple = MultipleSelect

  render() {
    return <SingleSelect />
  }
}
