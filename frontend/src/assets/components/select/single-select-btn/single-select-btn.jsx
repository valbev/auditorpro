import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Label } from '../../label'
import { SelectOutput } from '../parts/select-output'
import { Dropdown } from '../parts/dropdown'
import { Button } from '../../button'
import '../select.scss'
import './single-select-btn.scss'

export class SingleSelectBtn extends Component {
  state = {
    itemList: [],
    selectedItem: -1,
    isOpened: false,
  }

  componentDidMount() {
    const { selectedItem = -1, itemList = [] } = this.props

    this.setState({
      selectedItem,
      itemList: [...itemList].sort((a, b) => a - b),
    })
  }

  handleClose = (event) => {
    if (event.key === 'Escape') {
      this.toggleDropdown()
    }
  }

  onSelectItem = (id) => {
    const { onChange, name } = this.props

    this.setState(
      { selectedItem: id },
      () => onChange && onChange(this.state.selectedItem, name), // TODO line 32: refactoring
    )
    this.toggleDropdown()
  }

  toggleDropdown = () => {
    this.setState(({ isOpened }) => ({ isOpened: !isOpened })) // TODO create two functions: openDropdown and closeDropdown
  }

  render() {
    const { itemList, isOpened, selectedItem } = this.state
    const { labelText, disabled } = this.props
    const selectedValue = itemList[selectedItem]

    return (
      <div className='select single-select-btn'>
        <div className='single-select-btn__left-btn'>
          <Button
            type='icon'
            // onClick={onSearch}
            // disabled={disabled}
          >
            <FontAwesomeIcon icon={['far', 'calendar-alt']} color='#666666' size='1x' />
          </Button>
        </div>
        <Label labelText={labelText}>
          <SelectOutput
            disabled={disabled}
            toggleDropdown={this.toggleDropdown}
            value={selectedValue}
            imgInvert={isOpened}
          />
        </Label>
        {isOpened && <Dropdown itemList={itemList} onSelectItem={this.onSelectItem} handleClose={this.handleClose} />}
      </div>
    )
  }
}
