import React from 'react'
import { Table as TableTemplate } from 'reactstrap'
import { Button } from '../button'
import { Icons } from './btn-icons'
import './table.scss'
import { Link } from 'react-router-dom'

export const Table = ({ table, data, tableBtnActionHandler }) => {
  const { head, settings } = table
  const parsedHead = [...head]

  Object.keys(settings.tableRowButtons).forEach((prop) => {
    parsedHead.push({
      title: prop,
      type: 'button',
      name: '',
    })
  })

  const linkBuilder = (item, headItem) => {
    if (headItem.title === 'answers') {
      switch (item.type) {
        case 'image_poll':
          return <Link to={`/image-answers/${item.id}`}>{item[headItem.title]}</Link>
        default:
          return item[headItem.title]
      }
    } else {
      return item[headItem.title]
    }
  }

  return (
    <TableTemplate striped>
      <thead>
        <tr>
          {parsedHead.map((item) => (
            <th key={item.title}>{item.name}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map((item) => (
          <tr key={item.id}>
            {parsedHead.map((headItem, id) => {
              switch (headItem.type) {
                case 'button':
                  return (
                    <td key={id}>
                      <Button
                        onClick={() => tableBtnActionHandler(data, item.id, headItem.title, item.type)}
                        type='icon'
                      >
                        {Icons[headItem.title].icon}
                      </Button>
                    </td>
                  )
                default:
                  return <td key={id}>{linkBuilder(item, headItem)}</td>
              }
            })}
          </tr>
        ))}
      </tbody>
    </TableTemplate>
  )
}
