import React from 'react'

export const IconButton = ({ children, onClick }) => {
  return (
    <div className='icon-button' onClick={onClick}>
      {children}
    </div>
  )
}
