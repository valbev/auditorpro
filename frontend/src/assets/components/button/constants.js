export const BUTTON_TYPE = {
  primary: 'btn-primary',
  outline: 'btn-outline-primary',
  icon: 'btn-icon',
  iconBorder: 'btn-icon-border',
  link: 'btn-link',
  dashedLink: 'btn-link btn-link--dashed',
}
