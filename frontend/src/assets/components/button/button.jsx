import React from 'react'
import classNames from 'classnames'
import { BUTTON_TYPE } from './constants'
import './button.scss'

export const Button = ({
  children,
  onClick,
  type = 'primary',
  block = false,
  className = '',
  disabled = false,
  ...restProps
}) => (
  <button
    className={classNames('btn', className, BUTTON_TYPE[type], { 'btn-block': block })}
    onClick={onClick}
    disabled={disabled}
    {...restProps}
  >
    {children}
  </button>
)
