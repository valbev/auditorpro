import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './pagination.scss'

export const Pagination = ({
  current,
  all = 10,
  prevBtn = () => {
    if (current > 1) {
      current--
    }
  },
  nextBtn = () => {
    if (current < all) {
      current++
    }
  },
  onChangeValuePagination = (event) => {
    if (current >= 1 <= all) {
      current = event.target.value
    }
  },
  classPagination = '',
}) => {
  // const [currentItem, setCurrentItem] = useState(current)
  // const currentItem = current
  // console.log('current-Pagination', currentItem, all)
  // console.log('currentItem-Pagination', currentItem, all)

  const stylesPagination = 'pagination ' + classPagination

  return (
    <div className={stylesPagination.trim()}>
      <FontAwesomeIcon icon={['fas', 'angle-left']} color='#333333' size='lg' onClick={() => prevBtn()} />
      <span>
        <input
          className='pagination__input'
          type='number'
          // defaultValue={current}
          value={current}
          // min="1"
          // max={all}
          onChange={onChangeValuePagination}
        />
        из {all}
      </span>
      <FontAwesomeIcon icon={['fas', 'angle-right']} color='#333333' size='lg' onClick={() => nextBtn()} />
    </div>
  )
}
