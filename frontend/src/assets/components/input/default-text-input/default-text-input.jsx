import React from 'react'
import { Label } from '../../label'

export const DefaultTextInput = ({
  disabled,
  inputRef,
  autoFocus,
  onChange = () => {},
  onFocus = () => {},
  onBlur = () => {},
  onKeyPress = () => {},
  placeholder,
  labelText,
  name,
  type,
  value,
  style,
  id,
  readOnly,
}) => {
  const input = (
    <input
      autoFocus={autoFocus}
      id={id}
      value={value}
      type={type || 'text'}
      disabled={disabled}
      readOnly={readOnly}
      ref={inputRef}
      onChange={() => onChange(event)}
      onFocus={() => onFocus(true)}
      onBlur={() => onBlur(false)}
      onKeyPress={() => onKeyPress(event)}
      placeholder={placeholder}
      name={name}
      style={style}
    />
  )

  return labelText ? <Label labelText={labelText}>{input}</Label> : input
}
