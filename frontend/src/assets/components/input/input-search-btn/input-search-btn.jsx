import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { DefaultTextInput } from '../default-text-input'
import { Button } from '../../button'
import './input-search-btn.scss'

export class InputSearchBtn extends Component {
  state = {
    value: '',
  }

  handleChange = (e) => {
    this.setState({ value: e.target.value })
  }

  render() {
    const { ...props } = this.props

    return (
      <div className='text-input-btn'>
        <DefaultTextInput
          placeholder={props.placeholder}
          // value={this.state.value}
          // onChange={this.handleChange} {...props}
        />
        <div className='search-button'>
          <Button
            type='icon'
            // onClick={onSearch}
            // disabled={disabled}
          >
            <FontAwesomeIcon icon={['fas', 'search']} color='#666666' size='1x' />
          </Button>
        </div>
      </div>
    )
  }
}
