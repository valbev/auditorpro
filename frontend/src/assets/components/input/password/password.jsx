import React from 'react'
import { Label } from '../../label'

export const Password = ({ inputRef, onChange, placeholder, labelText, name }) => {
  const input = <input type='password' ref={inputRef} onChange={onChange} placeholder={placeholder} name={name} />

  return labelText ? <Label labelText={labelText}>{input}</Label> : input
}
