import React from 'react'
import './toggler.scss'

export const Toggler = ({ checked, onChange, name, style }) => {
  return (
    <label className='toggler' style={style}>
      <input type='checkbox' checked={checked} onChange={onChange} name={name} />
      <span />
    </label>
  )
}
