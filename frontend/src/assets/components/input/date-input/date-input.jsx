import React, { useState } from 'react'
import DatePicker, { registerLocale } from 'react-datepicker'
import { ReactComponent as CalendarIcon } from './resources/calendar.svg'
import ru from 'date-fns/locale/ru'
import 'react-datepicker/dist/react-datepicker.css'
import './date-input.scss'

registerLocale('ru', ru)

export const DateInput = ({ handleChange, date, labelText, placeholder, disabled, required }) => {
  const [selectedDate, setSelectedDate] = useState(date || (placeholder ? '' : new Date()))

  const ExampleCustomInput = (data) => (
    <div className='datepicker-trigger' onClick={data.onClick}>
      <CalendarIcon className='datepicker-trigger__icon' />
      <input
        className='datepicker-trigger__input'
        readOnly
        type='text'
        value={data.value || data.placeholder}
        onFocus={data.onFocus}
        onBlur={data.onBlur}
        onKeyDown={data.onKeyDown}
      />
      <div className='arrow arrow-bottom' />
    </div>
  )

  const onChange = (date) => {
    setSelectedDate(date)
    handleChange(date)
  }

  return (
    <div>
      {labelText && <div className='datepicker-label'>{labelText}</div>}
      <DatePicker
        selected={selectedDate}
        onChange={onChange}
        disabled={disabled}
        required={required}
        customInput={<ExampleCustomInput />}
        locale='ru'
        dateFormat='d MMMM yyyy'
        placeholderText={placeholder}
      />
    </div>
  )
}
