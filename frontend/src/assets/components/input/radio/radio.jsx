import React from 'react'
import './radio.scss'

export const Radio = ({ reverse = false, title, checked, onChange, name = '', value }) => (
  <label className={`radio${reverse ? ' radio--reverse' : ''}`}>
    <input className='d-none' type='radio' checked={checked} onChange={onChange} name={name} value={value} />
    <span className='radio-custom' />
    <span className='radio-text'>{title}</span>
  </label>
)
