import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './checkbox.scss'

export const CheckBox = ({
  id = null,
  checked = false,
  onToggleCheck = () => {},
  labelText = null,
  classLabel = '',
}) => {
  const stylesLabel = 'label-checkbox ' + classLabel
  let nameIcon = ''
  if (checked === true) {
    nameIcon = 'check-square'
  } else {
    if (checked === false) {
      nameIcon = 'square'
    } else {
      if (checked === null) {
        nameIcon = 'minus-square'
      }
    }
  }
  const icon = <FontAwesomeIcon icon={['far', nameIcon]} color='#8355E2' size='lg' />

  return (
    <label className={stylesLabel.trim()} onClick={() => onToggleCheck(id)}>
      {icon}
      <span>{labelText}</span>
    </label>
  )
}
