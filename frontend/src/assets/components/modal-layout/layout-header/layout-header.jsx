import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from '../../button'

export const LayoutHeader = ({ children, handleClick }) => {
  return (
    <div className='layout-header'>
      <Button type='icon' className='layout-close' onClick={handleClick}>
        <FontAwesomeIcon icon={['fas', 'times']} color='#8355E2' size='lg' />
      </Button>
      {children}
    </div>
  )
}
