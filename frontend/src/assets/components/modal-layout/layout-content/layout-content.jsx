import React from 'react'

export const LayoutContent = (props) => {
  return <div className='layout-content'>{props.children}</div>
}
