import React from 'react'

export const LayoutFooter = (props) => {
  return <div className='layout-footer'>{props.children}</div>
}
