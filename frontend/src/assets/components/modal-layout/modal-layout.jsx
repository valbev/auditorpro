import React from 'react'
import { LayoutHeader } from './layout-header'
import { LayoutContent } from './layout-content'
import { LayoutFooter } from './layout-footer'
import './modal-layout.scss'

export const ModalLayout = ({ header, content, footer, handleCloseClick }) => {
  return (
    <div className='layout'>
      <LayoutHeader handleClick={handleCloseClick}>{header}</LayoutHeader>
      <LayoutContent>{content}</LayoutContent>
      <LayoutFooter>{footer}</LayoutFooter>
    </div>
  )
}
