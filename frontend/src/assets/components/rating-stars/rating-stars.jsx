import React from 'react'
import './rating-stars.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export const RatingStars = ({ percent = 0, onChangeRatingStars }) => {
  const countColored = Math.round(+percent / 20)
  const colored = []
  for (let i = 1; i < 6; i++) {
    colored.push(i <= countColored)
  }
  const stars = colored.map((item, i) => (
    <FontAwesomeIcon
      key={i}
      icon={['fas', 'star']}
      color={item ? '#8355E2' : '#DADADA'}
      size='lg'
      onClick={() => onChangeRatingStars(i + 1)}
    />
  ))
  return <div className='rating-answer__stars'>{stars}</div>
}
