import React from 'react'
import ReactDOM from 'react-dom'
import { ReduxApp } from './components/redux-app'
import 'bootstrap/scss/bootstrap.scss'
import WebFont from 'webfontloader'

WebFont.load({
  google: {
    families: ['Manrope:300,400,500,700, 800', 'sans-serif'],
  },
})

ReactDOM.render(<ReduxApp />, document.getElementById('root'))
