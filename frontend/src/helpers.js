const isArray = function(a) {
  return Array.isArray(a)
}

const isObject = function(o) {
  return o === Object(o) && !isArray(o) && typeof o !== 'function'
}

export function toCamel(string) {
  return string.replace(/(_\w)/g, (m) => {
    return m[1].toUpperCase()
  })
}

export function toSnake(string) {
  return string
    .replace(/[\w]([A-Z])/g, (m) => {
      return m[0] + '_' + m[1]
    })
    .toLowerCase()
}

export const keysToCamel = function(o) {
  if (isObject(o)) {
    const n = {}

    Object.keys(o).forEach((k) => {
      n[toCamel(k)] = keysToCamel(o[k])
    })

    return n
  } else if (isArray(o)) {
    return o.map((i) => {
      return keysToCamel(i)
    })
  }

  return o
}

export const keysToSnake = function(o) {
  if (isObject(o)) {
    const n = {}

    Object.keys(o).forEach((k) => {
      n[toSnake(k)] = keysToSnake(o[k])
    })

    return n
  } else if (isArray(o)) {
    return o.map((i) => {
      return keysToSnake(i)
    })
  }

  return o
}

export const isObjectEmpty = (o) => Object.keys(o).length === 0
