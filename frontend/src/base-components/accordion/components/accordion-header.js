import React from 'react'
import PropTypes from 'prop-types'

export const AccordionHeader = ({ handleClick, children }) => {
  return (
    <div className='accordion__header' onClick={handleClick}>
      {children}
    </div>
  )
}

AccordionHeader.propTypes = {
  children: PropTypes.node,
  handleClick: PropTypes.func,
}
