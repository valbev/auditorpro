import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'

export const AccordionContent = ({ children, opened }) => {
  const containerRef = useRef(null)
  const [height, setHeight] = useState(0)

  useEffect(() => {
    const contHeight = containerRef.current.scrollHeight
    setHeight(opened ? contHeight : 0)
  }, [opened])

  return (
    <div className='accordion__content' style={{ height }} ref={containerRef}>
      {children}
    </div>
  )
}

AccordionContent.propTypes = {
  children: PropTypes.node,
  opened: PropTypes.bool,
}
