import React, { useState } from 'react'
import classNames from 'classnames'
import { AccordionHeader, AccordionContent } from './components'
import './component.scss'

export const Accordion = ({
  header: Header,
  content: Content,
  opened: defaultOpened = false,
  collapsible = true,
  onClickTitle,
}) => {
  const [opened, setOpened] = useState(defaultOpened)

  const handleToggleAccordion = () => {
    if (onClickTitle) {
      onClickTitle()
    }
    if (!collapsible) {
      return false
    }
    setOpened((prev) => !prev)
  }

  return (
    <div
      className={classNames('accordion', {
        opened,
      })}
    >
      <AccordionHeader handleClick={handleToggleAccordion}>
        <Header />
      </AccordionHeader>
      <AccordionContent opened={opened}>
        <Content />
      </AccordionContent>
    </div>
  )
}
