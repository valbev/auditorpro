from django.contrib import admin
from django.urls import (
    include,
    path,
)
from django.views.generic import TemplateView

from rest_framework_jwt.views import (
    ObtainJSONWebToken,
    refresh_jwt_token,
    verify_jwt_token,
)

from generic.jwt import CustomJWTSerializer
from check.endpoints import check_schedule_endpoints
from company.endpoints import company_endpoints
from misc.endpoints import rootapp_endpoints
#from neural_network.endpoints import neural_network_endpoints
from poll.endpoints import poll_endpoints
from user.endpoints import user_endpoints
from image_poll.endpoints import image_poll_endpoints
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls.static import static
import settings

schema_view = get_swagger_view(title='Mystery guest API')


authentication_endpoints = [
    path('token/obtaining/', ObtainJSONWebToken.as_view(serializer_class=CustomJWTSerializer)),
    path('token/refreshing/', refresh_jwt_token),
    path('token/verification/', verify_jwt_token),
]

v1 = [
    path('authentication/', include(authentication_endpoints)),
    path('check-schedules/', include(check_schedule_endpoints)),
    path('companies/', include(company_endpoints)),
    path('users/', include(user_endpoints)),
    path('openapi/', TemplateView.as_view(template_name="index.html")),
    path('misc/', include(rootapp_endpoints)),
    path('polls/', include(poll_endpoints)),
    path('image-polls/', include(image_poll_endpoints)),
    #path('neural-network/', include(neural_network_endpoints))
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('v1/', include(v1)),
] + static(settings.STATIC_URL)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
