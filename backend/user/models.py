"""
Provide database models for user.
"""
from __future__ import unicode_literals
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import ugettext_lazy as _

from generic.enums import UserType
from generic.models import SocialNetworks, Industry, Notifications
from user.managers import UserManager

SEX_MALE_STATUS = 'male'
SEX_FEMALE_STATUS = 'female'

SEXES_STATUSES = (
    (SEX_MALE_STATUS, SEX_MALE_STATUS),
    (SEX_FEMALE_STATUS, SEX_FEMALE_STATUS),
)


class User(AbstractBaseUser, PermissionsMixin):
    """
    User database model.
    """

    email = models.EmailField(unique=True, blank=False)

    created = models.DateTimeField(auto_now_add=True)
    is_email_confirmed = models.BooleanField(default=False)

    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    is_deleted = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        """
        Meta.
        """

        ordering = ('created',)
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        """
        Return the first name plus the last name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Return the short name for the user.
        """
        return self.email

    @classmethod
    def create_with_email(cls, email, password, profile_data):
        """
        Create a user with specified e-mail address and password.
        """
        user = cls.objects.create_user(email=email, password=password)
        return SecretGuestProfile.objects.create(user=user, **profile_data).user_id

    @classmethod
    def does_exist_by_email(cls, email):
        """
        Check if user exists by e-mail address.
        """
        if cls.objects.filter(email=email).exists():
            return True

        return False

    @classmethod
    def verify_password(cls, email, password):
        """
        Check if the user's password is equal to the encrypted user password.
        """
        encrypted_user_password = cls.objects.get(email=email).password
        return check_password(password=password, encoded=encrypted_user_password)

    @classmethod
    def set_new_password(cls, email, password):
        """
        Set new user password by specified e-mail.
        """
        user = cls.objects.get(email=email)
        user.set_password(password)
        user.save()

    @classmethod
    def set_new_email(cls, old_email, new_email):
        """
        Set new user e-email.
        """
        user = cls.objects.get(email=old_email)
        user.email = new_email
        user.save()

    @classmethod
    def delete(cls, email):
        """
        Delete user.
        """
        user = cls.objects.get(email=email)
        user.is_deleted = True
        user.save()

    @classmethod
    def get_user_by_email(cls, email):
        """
        Get user by email.
        """
        user = cls.objects.get(email=email)
        return user


class SecretGuestProfile(SocialNetworks, Industry, Notifications):
    """
    Secret guest profile database model.
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    first_name = models.CharField(max_length=30, blank=True, null=True, default='')
    last_name = models.CharField(max_length=100, blank=True, null=True, default='')

    phone = models.CharField(max_length=100, blank=True, null=True, default='')
    birthday = models.DateField(blank=True, null=True)

    address = models.CharField(max_length=100, blank=True, null=True, default='')
    language = models.CharField(max_length=2, null=True)

    sex = models.CharField(max_length=10, choices=SEXES_STATUSES, blank=True, null=True)

    avatar_url = models.URLField(max_length=200, blank=True, null=True)
    cv_url = models.URLField(max_length=200, blank=True, null=True)  # Link to CV

    has_auto = models.BooleanField(default=False)
    car_make = models.CharField(max_length=100, blank=True, null=True, default='')

    photo_path = models.CharField(max_length=256, blank=True, null=True, default='')

    time_zone_name = models.CharField(max_length=100, blank=True, null=True)
    time_zone_offset = models.FloatField(blank=True, null=True)

    city = models.CharField(max_length=100, blank=True, null=True)

    country = models.CharField(max_length=100, blank=True, null=True)

    tag = models.CharField(max_length=500, blank=True, null=True)

    last_ip_address = models.CharField(max_length=100, blank=True, null=True, default='0.0.0.0')

    @property
    def user_type(self):
        return 'secret-guest'

    @property
    def full_name(self):
        return (self.last_name + ' ' + self.first_name).strip()

    def __str__(self):
        """
        Get string representation of an object.
        """
        return self.user.email


class BusinessUserProfile(models.Model):
    """
    Business user profile database model.
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    first_name = models.CharField(max_length=30, blank=False, null=False)
    last_name = models.CharField(max_length=100, blank=True)

    address = models.CharField(max_length=100, blank=True, null=True, default='')
    language = models.CharField(max_length=2, null=True)

    phone = models.CharField(max_length=100, blank=False, null=False)
    position = models.CharField(max_length=100, blank=False, null=False)

    @property
    def user_type(self):
        return 'business'

    @property
    def full_name(self):
        return self.first_name + ' ' + self.last_name

    def __str__(self):
        """
        Get string representation of an object.
        """
        return self.user.email
