"""
Provide implementation of user endpoints .
"""
from django.urls import path
from user.views.email import UserEmailSingle
from user.views.location import UserLocation
from user.views.registration import UserRegistrationSingle
from user.views.password import UserPasswordSingle
from user.views.user import UserSingle, UserSingleToken
from user.views.social import SocialSignUp

user_endpoints = [
    path('<int:pk>/', UserSingle.as_view()),
    path('get_by_token/', UserSingleToken.as_view()),
    path('registration/', UserRegistrationSingle.as_view()),
    path('<int:pk>/password/', UserPasswordSingle.as_view()),
    path('<int:pk>/email/', UserEmailSingle.as_view()),
    path('check_address/<str:address>/<str:only_cities>/', UserLocation.get_valid_addresses),
    path('get_timezone/<str:address>/', UserLocation.get_time_zone),
    path('login_with_soc/', SocialSignUp.as_view())
]
