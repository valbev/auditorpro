"""
Provide implementation of single user registration endpoint.
"""
from http import HTTPStatus
from django.http import JsonResponse
from rest_framework import permissions
from rest_framework.views import APIView
from generic.enums import UserType
from user.domain.errors import UserWithSpecifiedEmailAddressAlreadyExistsError
from user.domain.objects import RegisterUser
from user.forms import (
    BusinessProfileRegistrationForm,
    SecretGuestProfileRegistrationForm,
    UserRegistrationForm,
)
from user.models import User


class UserRegistrationSingle(APIView):
    """
    Single user registration endpoint implementation.
    """

    permission_classes = (permissions.AllowAny,)

    def __init__(self):
        """
        Constructor.
        """
        self.user = User()

    def post(self, request):
        """
        Create a user with e-mail address and password.
        """

        email = request.data.get('email')
        password = request.data.get('password')

        form = UserRegistrationForm({
            'email': email,
            'password': password,
        })

        if not form.is_valid():
            return JsonResponse(form.errors, status=HTTPStatus.BAD_REQUEST)

        first_name = request.data.get('first_name')
        last_name = request.data.get('last_name')
        phone = request.data.get('phone')
        birthday = request.data.get('birthday')
        address = request.data.get('address')

        form = SecretGuestProfileRegistrationForm({
            'first_name': first_name,
            'last_name': last_name,
            'phone': phone,
            'birthday': birthday,
            'address': address,
        }, use_required_attribute=True)

        if not form.is_valid():
            return JsonResponse(form.errors, status=HTTPStatus.BAD_REQUEST)

        profile_data = {
            'first_name': first_name,
            'last_name': last_name,
            'phone': phone,
            'birthday': birthday,
            'address': address,
        }

        try:
            user_id = RegisterUser(user=self.user).by_email(email=email, password=password, profile_data=profile_data)
        except UserWithSpecifiedEmailAddressAlreadyExistsError as error:
            return JsonResponse({'error': error.message}, status=HTTPStatus.BAD_REQUEST)

        return JsonResponse({'result': 'User has been created.', 'user_id': user_id}, status=HTTPStatus.OK)
