"""
Provide implementation of single user email endpoint.
"""
from http import HTTPStatus

from django.http import JsonResponse
from rest_framework.decorators import (
    authentication_classes,
    permission_classes,
)
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from generic.view import user_identifier_only_permission
from user.domain.errors import UserWithSpecifiedEmailAddressDoesNotExistError

from user.domain.objects import ChangeUserEmail
from user.forms import ChangeUserEmailForm
from user.models import User


class UserEmailSingle(APIView):
    """
    Single user password endpoint implementation.
    """

    def __init__(self):
        """
        Constructor.
        """
        self.user = User()

    @user_identifier_only_permission
    @authentication_classes((JSONWebTokenAuthentication, ))
    def post(self, request, pk):
        """
        Change user password.
        """

        old_email = request.data.get('old_email')
        new_email = request.data.get('new_email')

        form = ChangeUserEmailForm({
            'old_email': old_email,
            'new_email': new_email,
        })

        if not form.is_valid():
            return JsonResponse({'errors': form.errors}, status=HTTPStatus.BAD_REQUEST)

        try:
            ChangeUserEmail(user=self.user).do(old_email=old_email, new_email=new_email)

        except UserWithSpecifiedEmailAddressDoesNotExistError as error:
            return JsonResponse({'error': error.message}, status=HTTPStatus.NOT_FOUND)

        return JsonResponse({'result': 'Email has been changed.'}, status=HTTPStatus.OK)
