import datetime
from http import HTTPStatus

import pytz
from django.http import JsonResponse
from django.conf import settings
import urllib3
import json
from tzwhere import tzwhere


class UserLocation:
    """
    Geo and language location of users.
    """
    cyrillic_alphabet = set('абвгдеёжзийклмнопрстуфхцчшщъыьэюя')
    http_url = urllib3.PoolManager()
    url_api_yandex = 'https://geocode-maps.yandex.ru/1.x/?geocode={address}&apikey={apikey}&format=json'
    url_api_google = 'https://maps.googleapis.com/maps/api/geocode/json?address={address}&key={apikey}&language=en'
    url_open_street_map = 'https://nominatim.openstreetmap.org/search/{address}?format=json'  #&addressdetails=1

    @staticmethod
    def has_cyrillic(s):
        return UserLocation.cyrillic_alphabet.intersection(s.lower()) != set()

    @staticmethod
    def get_openstreetmap_addresses(address, extractor, only_cities=False):
        """
        Request to Open street map
        :param address:
        :return:
        """
        output = set()

        if only_cities:
            UserLocation.url_open_street_map += '&addressdetails=1'

        request_url = UserLocation.url_open_street_map.format(address=address)

        r = UserLocation.http_url.request('GET', request_url, headers={'Content-Type': 'application/json',
                                                                       'User-Agent': 'PostmanRuntime/7.21.0',
                                                                       'Host': 'nominatim.openstreetmap.org'
                                                                       })
        if r.status == HTTPStatus.OK:
            if only_cities:
                for address in json.loads(r.data, encoding='utf-8'):
                    if address['address'].get('city'):
                        output.add(address['address']['city'])
            else:
                for address in json.loads(r.data, encoding='utf-8'):
                    output.add(extractor(address))
        return list(output)

    @staticmethod
    def get_google_addresses(address, extractor):
        """
        Request to Google
        :param address: list of addresses
        :return:
        """
        output = []
        request_url = UserLocation.url_api_google.format(address=address, apikey=settings.GOOGLE_GEOCODING_KEY)

        r = UserLocation.http_url.request('GET', request_url)
        if r.status == HTTPStatus.OK:
            for address in json.loads(r.data, encoding='utf-8')['results']:
                output.append(extractor(address))
        return output

    @staticmethod
    def get_yandex_addresses(address, extractor):
        """
        Request to Yandex
        :param address:
        :return: list of addresses
        """
        output = []
        request_url = UserLocation.url_api_yandex.format(address=address, apikey=settings.YANDEX_GEOCODING_KEY)

        r = UserLocation.http_url.request('GET', request_url)
        if r.status == HTTPStatus.OK:
            for address in json.loads(r.data, encoding='utf-8')['response']['GeoObjectCollection']['featureMember']:
                output.append(extractor(address))
        return output

    @staticmethod
    def get_valid_addresses(request, address, only_cities='false'):
        """
        Get valid addresses.
        """
        valid_addresses = []
        try:
            extractor = lambda response_address: response_address['display_name']
            valid_addresses = UserLocation.get_openstreetmap_addresses(address, extractor, only_cities == 'true')
            if not valid_addresses:
                if UserLocation.has_cyrillic(address):
                    extractor = lambda response_address: response_address['GeoObject']['metaDataProperty']['GeocoderMetaData']['text']
                    valid_addresses = UserLocation.get_yandex_addresses(address, extractor)
                else:
                    extractor = lambda response_address: response_address['formatted_address']
                    valid_addresses = UserLocation.get_google_addresses(address, extractor)
        except Exception as er:
            return JsonResponse({'error': er.message}, status=HTTPStatus.INTERNAL_SERVER_ERROR)

        if valid_addresses:
            return JsonResponse({'result': valid_addresses}, status=HTTPStatus.OK)
        else:
            return JsonResponse({'result': 'Not found valid addresses '}, status=HTTPStatus.NOT_FOUND)

    @staticmethod
    def get_gps_coordinate_address(address):
        extractor = lambda response_address: (response_address['lat'], response_address['lon'])
        gps_coords = UserLocation.get_openstreetmap_addresses(address, extractor)
        if gps_coords:
            lat, lon = gps_coords[0]
            return float(lat), float(lon)
        extractor = lambda response_address: response_address['GeoObject']['Point']['pos']
        gps_coords = UserLocation.get_yandex_addresses(address, extractor)
        if gps_coords:
            lat, lon = gps_coords[0].split(' ')
            return lon, lat
        extractor = lambda response_address: (response_address['geometry']['location']['lat'], response_address['geometry']['location']['lng'])
        gps_coords = UserLocation.get_google_addresses(address, extractor)
        if gps_coords:
            lat, lon = gps_coords[0]
            return lat, lon
        return None

    @staticmethod
    def get_time_zone(request, address):
        """
        GMT offset
        :param request:
        :param address:
        :return: offset hours
        """
        try:
            gps = UserLocation.get_gps_coordinate_address(address)

            if gps:
                lat, lon = gps
                tz = tzwhere.tzwhere(forceTZ=True)
                res_tz = tz.tzNameAt(lat, lon, forceTZ=True)

                if res_tz:
                    local_now = datetime.datetime.now(pytz.timezone(res_tz))
                    offset = local_now.utcoffset().total_seconds() / 60 / 60
                    return JsonResponse({'result': {'timezone_name': res_tz,
                                                    'offset_h': offset,
                                                    'lat': lat, 'lon': lon}
                                         }, status=HTTPStatus.OK)

            return JsonResponse({'result': 'Not found timezone'}, status=HTTPStatus.NOT_FOUND)

        except Exception as er:
            return JsonResponse({'error': er.message}, status=HTTPStatus.INTERNAL_SERVER_ERROR)
