from http import HTTPStatus
from django.http import JsonResponse
from rest_framework import permissions
from rest_framework.views import APIView
from generic.enums import UserType
from user.domain.errors import UserWithSpecifiedEmailAddressAlreadyExistsError
from user.domain.objects import RegisterUser
from user.forms import (
    BusinessProfileRegistrationForm,
    SecretGuestProfileRegistrationForm,
    UserRegistrationForm,
)
from user.models import User
from rest_framework_jwt.settings import api_settings
from django.utils.crypto import get_random_string

import requests
import json


class SocialSignUp(APIView):
    """
      List all snippets, or create a new snippet.
      """
    permission_classes = (permissions.AllowAny,)

    def __init__(self):
        """
        Constructor.
        """
        self.user = User()

    def post(self, request):
        provider = request.data.get('provider')

        if (provider == 'google'):
            access_token = request.data.get('access_token')

            validate_payload = {'access_token': access_token}
            v = requests.get(
                'https://www.googleapis.com/oauth2/v1/tokeninfo', params=validate_payload)

            if (v.ok):
                email = request.data.get('email')
                email_by_token = v.json().get('email')

                if (email_by_token == email):
                    is_user_exists = self.user.does_exist_by_email(email=email)

                    if (is_user_exists):
                        user = self.user.get_user_by_email(email=email)

                        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

                        payload = jwt_payload_handler(user=user)
                        token = jwt_encode_handler(payload)
                        token_lifetime = payload['exp'] - payload['orig_iat']

                        return JsonResponse({'token': token, 'token_lifetime': token_lifetime}, status=HTTPStatus.OK)
                    else:
                        password = get_random_string(length=32)

                        form = UserRegistrationForm({
                            'email': email,
                            'password': password,
                        })

                        if not form.is_valid():
                            return JsonResponse(form.errors, status=HTTPStatus.BAD_REQUEST)

                        first_name = request.data.get('first_name')
                        last_name = request.data.get('last_name')
                        phone = '79999999999'
                        birthday = '1970-01-01'
                        address = 'None'

                        form = SecretGuestProfileRegistrationForm({
                            'first_name': first_name,
                            'last_name': last_name,
                            'phone': phone,
                            'birthday': birthday,
                            'address': address,
                        }, use_required_attribute=True)

                        if not form.is_valid():
                            return JsonResponse(form.errors, status=HTTPStatus.BAD_REQUEST)

                        profile_data = {
                            'first_name': first_name,
                            'last_name': last_name,
                            'phone': phone,
                            'birthday': birthday,
                            'address': address,
                        }

                        RegisterUser(user=self.user).by_email(
                            email=email, password=password, profile_data=profile_data)
                        user = self.user.get_user_by_email(email=email)

                        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

                        payload = jwt_payload_handler(user=user)
                        token = jwt_encode_handler(payload)
                        token_lifetime = payload['exp'] - payload['orig_iat']

                        return JsonResponse({'token': token, 'token_lifetime': token_lifetime}, status=HTTPStatus.OK)
                else:
                    return JsonResponse({'error': 'Invalid token'}, status=HTTPStatus.BAD_REQUEST)
            else:
                return JsonResponse({'error': 'Invalid token'}, status=HTTPStatus.BAD_REQUEST)
        else:
            return JsonResponse({'error': 'Wrong social provider'}, status=HTTPStatus.BAD_REQUEST)
