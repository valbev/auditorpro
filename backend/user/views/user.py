"""
Provide implementation of single user endpoint.
"""
import logging
from http import HTTPStatus
from os.path import join
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.decorators import authentication_classes
from rest_framework.response import Response
from rest_framework.views import APIView
import uuid
from generic.enums import UserType
from generic.models import names_to_code_industries
from generic.view import user_identifier_only_permission
from user.domain.errors import UserWithSpecifiedEmailAddressDoesNotExistError
from user.domain.objects import DeleteUser, RegisterUser
from user.forms import SecretGuestProfileRegistrationForm, BusinessProfileRegistrationForm
from user.serializers.user import (
    BusinessUserProfileSerializer,
    SecretGuestProfileSerializer,
    UserSerializer,
)
from user.models import (
    User,
    SecretGuestProfile,
    BusinessUserProfile,
)

logger = logging.getLogger('api.user')


class UserSingleToken(APIView):
    """
    List all snippets, or create a new snippet.
    """

    def __init__(self):
        """
        Constructor.
        """
        self.user = User()

    @authentication_classes((JSONWebTokenAuthentication,))
    def get(self, request, format=None):
        """
        Get user.
        """
        try:
            profile_object, profile_serializer = UserSingle.get_profile(user_identifier=request.user.pk)
            serialized_user = UserSerializer(request.user)
            serialized_profile = profile_serializer(profile_object)

            response = serialized_user.data
            response.update(serialized_profile.data)
        except Exception as er:
            logger.error(er)
            return Response({'error': str(er)}, status=HTTPStatus.INTERNAL_SERVER_ERROR)

        return Response({'result': response}, status=HTTPStatus.OK)


class UserSingle(APIView):
    """
    List all snippets, or create a new snippet.
    """

    def __init__(self):
        """
        Constructor.
        """
        self.user = User()

    @staticmethod
    def get_profile(user_identifier):
        """
        Get ser profile object.
        """
        try:
            return SecretGuestProfile.objects.get(user__pk=user_identifier), SecretGuestProfileSerializer
        except SecretGuestProfile.DoesNotExist:
            pass

        try:
            return BusinessUserProfile.objects.get(user__pk=user_identifier), BusinessUserProfileSerializer
        except BusinessUserProfile.DoesNotExist:
            pass

    @user_identifier_only_permission
    @authentication_classes((JSONWebTokenAuthentication,))
    def get(self, request, pk, format=None):
        """
        Get user.
        """
        try:
            profile_object, profile_serializer = self.get_profile(user_identifier=pk)
            serialized_user = UserSerializer(request.user)
            serialized_profile = profile_serializer(profile_object)

            response = serialized_user.data
            response.update(serialized_profile.data)
        except Exception as er:
            logger.error(er)
            return Response({'error': str(er)}, status=HTTPStatus.INTERNAL_SERVER_ERROR)

        return Response({'result': response}, status=HTTPStatus.OK)

    @user_identifier_only_permission
    @authentication_classes((JSONWebTokenAuthentication,))
    def delete(self, request, pk, format=None):
        """
        Delete user.
        """
        email = request.user.email

        try:
            DeleteUser(user=self.user).do(email=email)

        except UserWithSpecifiedEmailAddressDoesNotExistError as error:
            return Response({'error': error.message}, status=HTTPStatus.NOT_FOUND)

        return Response({'result': 'User has been deleted.'}, status=HTTPStatus.OK)

    def add_prefix(self, filename):
        return "%s_%s" % (str(uuid.uuid4()), filename)

    @user_identifier_only_permission
    @authentication_classes((JSONWebTokenAuthentication, ))
    def post(self, request, pk):
        """
        Change user profile.
        """

        type_ = request.data.get('type')

        if type_ is None:
            return JsonResponse({'error': 'User types isn\'t specified.'}, status=HTTPStatus.BAD_REQUEST)

        try:
            if type_ == UserType.secret_guest.value:

                profile_data = get_secret_guest_profile(request.data)

                # TODO Сократить допроверки одного ключа
                non_emtpy_request_data = {key: request.data[key] for key in request.data}
                if 'check_industry' in non_emtpy_request_data:
                    non_emtpy_request_data['check_industry'] = names_to_code_industries(non_emtpy_request_data['check_industry'])

                SecretGuestProfile.objects.filter(user=request.user).update(**profile_data)

            if type_ == UserType.business.value:

                profile_data = get_business_user_profile(request.data)

                BusinessUserProfile.objects.filter(user=request.user).update(**profile_data)

        except Exception as er:
            return JsonResponse({'error': str(er)}, status=HTTPStatus.BAD_REQUEST)
        return JsonResponse({'result': 'User has been updated.'}, status=HTTPStatus.OK)


def get_business_user_profile(request_data):
    first_name = request_data.get('first_name')
    position = request_data.get('position')
    phone = request_data.get('phone')

    profile_data = {}

    if first_name:
        profile_data['first_name'] = first_name
    if position:
        profile_data['position'] = position
    if phone:
        profile_data['phone'] = phone

    return profile_data


def get_secret_guest_profile(request_data):
    first_name = request_data.get('first_name')
    last_name = request_data.get('last_name')
    phone = request_data.get('phone')
    birthday = request_data.get('birthday')
    address = request_data.get('address')
    sex = request_data.get('sex')
    has_auto = request_data.get('has_auto')
    car_make = request_data.get('car_make')
    cv_url = request_data.get('cv_url')
    city = request_data.get('city')
    photo_name_file = request_data.get('photo_path')

    # Social media
    vk_nickname = request_data.get('vk_nickname')
    facebook_nickname = request_data.get('facebook_nickname')
    instagram_nickname = request_data.get('instagram_nickname')

    # Notifications
    notify_new_request = request_data.get('notify_new_request')
    notify_news = request_data.get('notify_news')
    notify_analytics = request_data.get('notify_analytics')

    profile_data = {}

    if first_name:
        profile_data['first_name'] = first_name
    if last_name:
        profile_data['last_name'] = last_name
    if phone:
        profile_data['phone'] = phone
    if birthday:
        profile_data['birthday'] = birthday
    if address:
        profile_data['address'] = address
    if sex:
        profile_data['sex'] = sex
    if has_auto:
        profile_data['has_auto'] = True if has_auto.lower() == 'true' else False
    if car_make:
        profile_data['car_make'] = car_make
    if cv_url:
        profile_data['cv_url'] = cv_url
    if city:
        profile_data['city'] = city
    if photo_name_file:
        profile_data['photo_path'] = photo_name_file
    if vk_nickname:
        profile_data['vk_nickname'] = vk_nickname
    if facebook_nickname:
        profile_data['facebook_nickname'] = facebook_nickname
    if instagram_nickname:
        profile_data['instagram_nickname'] = instagram_nickname
    if notify_new_request:
        profile_data['notify_new_request'] = True if notify_new_request.lower() == 'true' else False
    if notify_news:
        profile_data['notify_news'] = True if notify_news.lower() == 'true' else False
    if notify_analytics:
        profile_data['notify_analytics'] = True if notify_analytics.lower() == 'true' else False

    return profile_data
