"""
Provide configuration for admin panel user tables.
"""
from django.contrib import admin

from user.models import (
    BusinessUserProfile,
    SecretGuestProfile,
    User,
)

admin.site.register(BusinessUserProfile)
admin.site.register(SecretGuestProfile)
admin.site.register(User)
