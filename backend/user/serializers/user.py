"""
Provide implementation of user model serializers.
"""
from rest_framework import serializers
from rest_framework import fields as serial_fields
from company.serializers.company import IndustrySerializer
from generic.models import CHECK_INDUSTRIES
from user.models import (
    BusinessUserProfile,
    SecretGuestProfile,
    User,
)


class UserSerializer(serializers.ModelSerializer):
    """
    User model serializer implementation.
    """
    profile = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = User
        fields = ['id', 'email', 'created', 'profile', 'is_deleted']


class SecretGuestProfileSerializer(serializers.ModelSerializer):
    """
    Secret guest profile model serializer implementation.
    """

    industries = IndustrySerializer(serial_fields.MultipleChoiceField(choices=CHECK_INDUSTRIES))

    class Meta:
        model = SecretGuestProfile
        fields = ['first_name', 'last_name', 'phone', 'birthday', 'address', 'sex', 'user_type',
                  'cv_url', 'has_auto', 'car_make', 'photo_path', 'time_zone_name', 'time_zone_offset',
                  'city', 'vk_nickname', 'facebook_nickname', 'instagram_nickname', 'notify_new_request',
                  'notify_news', 'notify_analytics', 'industries', 'photo_path', 'country', 'tag', 'last_ip_address']


class BusinessUserProfileSerializer(serializers.ModelSerializer):
    """
    Business profile model serializer implementation.
    """

    class Meta:
        model = BusinessUserProfile
        fields = ['first_name', 'position', 'phone', 'user_type']
