"""
Provide implementation of user registration.
"""
from user.domain.errors import (
    SpecifiedUserPasswordIsIncorrectError,
    UserWithSpecifiedEmailAddressAlreadyExistsError,
    UserWithSpecifiedEmailAddressDoesNotExistError,

)


class RegisterUser:
    """
    User registration implementation.
    """

    def __init__(self, user):
        """
        Constructor.
        """
        self.user = user

    def by_email(self, email, password, profile_data):
        """
        Create a user with specified e-mail address and password.
        """
        if self.user.does_exist_by_email(email=email):
            raise UserWithSpecifiedEmailAddressAlreadyExistsError

        return self.user.create_with_email(email=email, password=password, profile_data=profile_data)


class ChangeUserPassword:
    """
    Change user password implementation.
    """

    def __init__(self, user):
        """
        Constructor.
        """
        self.user = user

    def do(self, email, old_password, new_password):
        """
        Change user password.
        """
        if not self.user.does_exist_by_email(email=email):
            raise UserWithSpecifiedEmailAddressDoesNotExistError

        is_password_matched = self.user.verify_password(email=email, password=old_password)

        if not is_password_matched:
            raise SpecifiedUserPasswordIsIncorrectError

        self.user.set_new_password(email=email, password=new_password)


class ChangeUserEmail:
    """
    Change user password implementation.
    """

    def __init__(self, user):
        """
        Constructor.
        """
        self.user = user

    def do(self, old_email, new_email):
        """
        Change user password.
        """
        if not self.user.does_exist_by_email(email=old_email):
            raise UserWithSpecifiedEmailAddressDoesNotExistError

        self.user.set_new_email(old_email=old_email, new_email=new_email)


class DeleteUser:
    """
    Delete user implementation.
    """

    def __init__(self, user):
        """
        Constructor.
        """
        self.user = user

    def do(self, email):
        """
        Delete user.
        """
        if not self.user.does_exist_by_email(email=email):
            raise UserWithSpecifiedEmailAddressDoesNotExistError

        self.user.delete(email=email)
