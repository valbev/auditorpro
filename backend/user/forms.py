"""
Provide implementation of operation with user forms.
"""
from django import forms

from user.models import (
    BusinessUserProfile,
    SecretGuestProfile,
)


class RestoreUserPasswordForm(forms.Form):
    """
    Restore user password form implementation.
    """

    email = forms.EmailField()


class UserRegistrationForm(forms.Form):
    """
    Register user form implementation.
    """

    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)


class ChangeUserPasswordForm(forms.Form):
    """
    Change user password form implementation.
    """

    old_password = forms.CharField(widget=forms.PasswordInput)
    new_password = forms.CharField(widget=forms.PasswordInput)


class SecretGuestProfileRegistrationForm(forms.ModelForm):
    """
    Register secret guest profile form implementation.
    """

    class Meta:
        model = SecretGuestProfile
        fields = ['first_name', 'last_name', 'phone', 'birthday', 'address']


class BusinessProfileRegistrationForm(forms.ModelForm):
    """
    Register business profile form implementation.
    """

    class Meta:
        model = BusinessUserProfile
        fields = ['first_name', 'phone']


class ChangeUserEmailForm(forms.Form):
    """
    Change user password form implementation.
    """

    old_email = forms.CharField(widget=forms.EmailInput)
    new_email = forms.CharField(widget=forms.EmailInput)
