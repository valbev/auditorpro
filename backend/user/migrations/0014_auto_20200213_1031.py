# Generated by Django 2.2.6 on 2020-02-13 07:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0013_auto_20200211_1142'),
    ]

    operations = [
        migrations.AddField(
            model_name='secretguestprofile',
            name='time_zone_name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='secretguestprofile',
            name='time_zone_offset',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
