# Generated by Django 2.2.6 on 2020-03-13 05:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0019_remove_secretguestprofile_photo_path'),
    ]

    operations = [
        migrations.AddField(
            model_name='secretguestprofile',
            name='photo_path',
            field=models.CharField(blank=True, default='', max_length=256, null=True),
        ),
    ]
