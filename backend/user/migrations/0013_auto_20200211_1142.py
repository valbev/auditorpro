# Generated by Django 2.2.6 on 2020-02-11 08:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0012_auto_20200211_0942'),
    ]

    operations = [
        migrations.RenameField(
            model_name='secretguestprofile',
            old_name='photo_path',
            new_name='photo_path',
        ),
    ]
