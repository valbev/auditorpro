#!/usr/bin/env bash

export DATABASE_URL=postgresql://localhost:5432/dmitrii?user=postgres&password=postgres
export DJANGO_CONFIGURATION=DevLocal
export SECRET_KEY=huiewyfgew74364hdh8DhysdDurfey73545
export DJANGO_SETTINGS_MODULE=settings

./venv/bin/python3.6 ./manage.py makemigrations

./venv/bin/python3.6 ./manage.py migrate
