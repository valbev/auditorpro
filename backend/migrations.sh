#!/usr/bin/env bash

./venv/bin/python3.6 ./manage.py makemigrations

./venv/bin/python3.6 ./manage.py migrate
