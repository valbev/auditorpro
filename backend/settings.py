import logging
import logging.config
import os
from datetime import timedelta
from os.path import join
from pathlib import Path
from decouple import config
from configurations import Configuration


class DevBase(Configuration):
    DEBUG = config('DEBUG', default=True, cast=bool)
    SECRET_KEY = os.environ.get('SECRET_KEY')

    BASE_DIR = Path(__file__).parent

    ALLOWED_HOSTS = ['*']

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'rest_framework_swagger',
        'corsheaders',
        'rest_framework',
        'refreshtoken',
        'misc',
        'check',
        'company',
        'user',
        'poll',
        'image_poll',
        'multiselectfield',
        #'neural_network'
        #'drf_yasg',
    ]

    MIDDLEWARE = [
        'corsheaders.middleware.CorsMiddleware',
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        #'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ]

    ROOT_URLCONF = 'urls'

    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': ['templates', join(BASE_DIR, 'neural_network/pages')],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ]

    WSGI_APPLICATION = 'wsgi.application'

    REST_FRAMEWORK = {
        'DEFAULT_PERMISSION_CLASSES': (
            'rest_framework.permissions.IsAuthenticated',
        ),
        'DEFAULT_AUTHENTICATION_CLASSES': (
            'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        ),
        'UNAUTHENTICATED_USER': None,
    }

    JWT_AUTH = {
        'JWT_SECRET_KEY': SECRET_KEY,
        'JWT_VERIFY_EXPIRATION': False,
        'JWT_EXPIRATION_DELTA': timedelta(minutes=10),
        'JWT_REFRESH_EXPIRATION_DELTA': timedelta(days=3),
        'JWT_ALLOW_REFRESH': True,
        'JWT_RESPONSE_PAYLOAD_HANDLER': 'generic.view.jwt_response_payload_handler',
        'JWT_AUTH_COOKIE': None
    }

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': os.environ.get('API_PG_DB'),
            'USER': os.environ.get('API_PG_USER'),
            'PASSWORD': os.environ.get('API_PG_PASSWORD'),
            'HOST': os.environ.get('API_PG_HOST'),
            'PORT': os.environ.get('API_PG_PORT'),
            'OPTIONS': {
                'sslmode': 'verify-full',
                'sslrootcert': os.path.join(BASE_DIR, 'postgres_ssl.crt')
            },
        }
    }

    AUTH_PASSWORD_VALIDATORS = [
        {
            'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
        },
    ]

    STATICFILES_DIRS = [
        os.path.join(BASE_DIR, "static"),
        # ('poll_files', os.path.join(BASE_DIR, "media")),
        # ('profiles', os.path.join(BASE_DIR, "media"))
    ]

    AUTH_USER_MODEL = 'user.User'

    LANGUAGE_CODE = 'en-us'

    TIME_ZONE = 'UTC'

    USE_I18N = True

    USE_L10N = True

    USE_TZ = True

    CORS_ORIGIN_ALLOW_ALL = True

    STATIC_URL = '/static/'
    STATIC_ROOT = os.path.join(BASE_DIR, 'static_production')

    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
    MEDIA_URL = '/media/'

    GOOGLE_GEOCODING_KEY = 'AIzaSyD1scZWZOV_DueDT-vdnO7JzYhT72KBTUM'
    YANDEX_GEOCODING_KEY = '3bcc3b2b-0464-40a9-9c1e-db2f275b0a0c'


if os.environ.get('DJANGO_CONFIGURATION') == 'DevRemote':
    class DevRemote(DevBase):

        LOGGING = {
            'version': 1,
            'disable_existing_loggers': True,
            'filters': {
                'only_error_critical': {
                    '()': 'generic.log.OnlyErrorCriticalFilter',
                },
                'only_debug_warning': {
                    '()': 'generic.log.OnlyDebugWarningFilter',
                },
                'only_info': {
                    '()': 'generic.log.OnlyInfoFilter',
                }
            },
            'handlers': {
                'file_debug': {
                    'class': 'logging.FileHandler',
                    'formatter': 'default',
                    'filename':  '/var/log/mystery_guest/debug.log',
                    'encoding': 'utf-8',
                    'filters': ['only_debug_warning']
                },
                'file_error': {
                    'class': 'logging.FileHandler',
                    'formatter': 'default',
                    'filename': '/var/log/mystery_guest/errors.log',
                    'encoding': 'utf-8',
                    'filters': ['only_error_critical']
                },
                'file_info': {
                    'class': 'logging.FileHandler',
                    'formatter': 'default',
                    'filename': '/var/log/mystery_guest/info.log',
                    'encoding': 'utf-8',
                    'filters': ['only_info']
                }
            },
            'loggers': {
                'api': {
                    'handlers': ['file_debug', 'file_error', 'file_info'],
                    'level': 'DEBUG',  #'DEBUG', 'ERROR'
                }
            },
            'formatters': {
                'default': {
                    'format': "%(asctime)s|%(name)s|%(levelname)s|%(message)s"
                }
            },
        }

        logging.config.dictConfig(LOGGING)

if os.environ.get('DJANGO_CONFIGURATION') == 'DevLocal':
    class DevLocal(DevBase):

        JWT_AUTH = {
            'JWT_SECRET_KEY': DevBase.SECRET_KEY,
            'JWT_VERIFY_EXPIRATION': False,
            'JWT_EXPIRATION_DELTA': timedelta(minutes=60*24*5),
            'JWT_REFRESH_EXPIRATION_DELTA': timedelta(days=3),
            'JWT_ALLOW_REFRESH': True,
            'JWT_RESPONSE_PAYLOAD_HANDLER': 'generic.view.jwt_response_payload_handler',
            'JWT_AUTH_COOKIE': None
        }

        LOGGING = {
            'version': 1,
            'disable_existing_loggers': True,
            'filters': {
                'only_error_critical': {
                    '()': 'generic.log.OnlyErrorCriticalFilter',
                },
                'only_debug_warning': {
                    '()': 'generic.log.OnlyDebugWarningFilter',
                },
                'only_info': {
                    '()': 'generic.log.OnlyInfoFilter',
                }
            },
            'handlers': {
                'file_debug': {
                    'class': "logging.FileHandler",
                    'formatter': "default",
                    'filename':  join(DevBase.BASE_DIR, 'log', 'debug.log'),
                    'encoding': 'utf-8',
                    'mode': 'w',
                    'filters': ['only_debug_warning']
                },
                'file_error': {
                    'class': "logging.FileHandler",
                    'formatter': "default",
                    'filename': join(DevBase.BASE_DIR,'log', 'errors.log'),
                    'encoding': 'utf-8',
                    'mode': 'w',
                    'filters': ['only_error_critical']
                },
                'file_info': {
                    'class': "logging.FileHandler",
                    'formatter': "default",
                    'filename': join(DevBase.BASE_DIR, 'log', 'info.log'),
                    'encoding': 'utf-8',
                    'mode': 'w',
                    'filters': ['only_info']
                }
            },
            'loggers': {
                'api': {
                    'handlers': ['file_debug', 'file_error', 'file_info'],
                    'level': 'DEBUG',  #'DEBUG', 'ERROR'
                }
            },
            'formatters': {
                'default': {
                    'format': "%(asctime)s|%(name)s|%(levelname)s|%(message)s"
                }
            },
        }

        logging.config.dictConfig(LOGGING)
