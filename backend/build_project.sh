#!/usr/bin/env bash


docker build -t auditor_api \
             --build-arg DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE \
             --build-arg API_PG_DB=$API_PG_DB \
             --build-arg API_PG_USER=$API_PG_USER \
             --build-arg API_PG_PASSWORD=$API_PG_PASSWORD \
             --build-arg API_PG_HOST=$API_PG_HOST \
             --build-arg API_PG_PORT=$API_PG_PORT \
             --build-arg SECRET_KEY=$SECRET_KEY .