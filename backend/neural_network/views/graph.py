from django.template.response import TemplateResponse


def show_me_graph(request):
    return TemplateResponse(request, 'graph_3d.html', {})
