import io
import re
from http import HTTPStatus
from io import StringIO, BytesIO
from os.path import dirname, join
from django.template.response import TemplateResponse
import base64
from PIL import Image
import urllib.parse
import numpy as np
from neural_network.models.target_model import get_model, get_model_cnn
#from rest_framework.response import Response
from rest_framework.decorators import api_view
import json
from django.http import HttpResponse

model = get_model_cnn(num_classes=10, input_shape=(28, 28, 1))
model.load_weights(join(dirname(__file__), 'digital_recognise_cnn.model'))

def get_digit_class(input_vector):
    global model
    try:
        predict_class = model.predict_classes(input_vector, verbose=True)
        return predict_class[0]
    except Exception as er:
        print(er)
        return -1


def let_paint(request):
    return TemplateResponse(request, 'digital_predict.html', {})

#@api_view(['POST'])
def i_guess_your_digit(request):
    img_str = request.body.decode('ascii')
    img_str = urllib.parse.unquote(img_str)

    image_data = re.sub("^imgBase64=data:image/png;base64,", "", img_str)
    image_data = base64.b64decode(image_data)
    image_data = BytesIO(image_data)
    im = Image.open(image_data)
    im = im.resize((28, 28)).convert('LA')  # convert image to black and white
    #im.show()
    matrix_image = np.array(im)
    matrix_2d = np.zeros((28, 28))
    for i, row in enumerate(matrix_image):
        for j, cell in enumerate(row):
            matrix_2d[i, j] = cell[1]  #/ 255
    # im = Image.fromarray(matrix_2d)
    # im.show()
    #input_vector = matrix_2d.reshape((1, 784)).astype(np.float32)
    input_vector = matrix_2d.reshape((1, 28, 28, 1)).astype(np.float32)
    digit_class = get_digit_class(input_vector)
    #return Response({'result': digit_class}, status=HTTPStatus.OK)
    return HttpResponse(json.dumps({'result': str(digit_class)}), content_type="application/json")


def decode_base64(data):
    """Decode base64, padding being optional.

    :param data: Base64 data as an ASCII byte string
    :returns: The decoded byte string.

    """
    missing_padding = len(data) % 4
    if missing_padding:
        data += b'=' * (4 - missing_padding)
    return base64.b64decode(data)



