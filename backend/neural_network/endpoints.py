from django.urls import path
from neural_network.views.digitals import let_paint, i_guess_your_digit
from neural_network.views.graph import show_me_graph


neural_network_endpoints = [
    path('guess-number/', let_paint),
    path('guess-number/guess_now/', i_guess_your_digit),
    path('graph/show_me_graph/', show_me_graph)


    #path('<int:pk>/', PollCollection.as_view()),
    #
    # path('<int:pk>/fields/', CompanyFieldCollection.as_view()),
    # path('<int:pk>/fields/<int:field_identifier>/', CompanyFieldSingle.as_view()),
]