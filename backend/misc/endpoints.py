"""
Provide implementation of user endpoints .
"""
from django.urls import path
from misc.views.files import upload_image


rootapp_endpoints = [
    path('upload-image/<str:where>/', upload_image),
]

