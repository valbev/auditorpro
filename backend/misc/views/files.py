import logging
import uuid
from http import HTTPStatus
from os.path import join, dirname, basename
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse
from rest_framework.decorators import authentication_classes
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

logger = logging.getLogger('api.upload')


@authentication_classes((JSONWebTokenAuthentication,))
def upload_image(request, where):

    if where is None:
        return JsonResponse({'error': 'The "where" param is missing'}, status=HTTPStatus.BAD_REQUEST)

    if 'photo' in request.FILES:
        try:
            photo_name_file = str(uuid.uuid4())  # self.add_prefix(request.FILES['photo'].name)
            fs = FileSystemStorage(location=join(settings.MEDIA_ROOT, 'images', where))
            photo_name_file += '_{}'.format(request.FILES['photo'].name)
            fs.save(photo_name_file, request.FILES['photo'])
            photo_path_file = join(basename(settings.MEDIA_ROOT), 'images', where, photo_name_file)
            return JsonResponse({'path': photo_path_file}, status=HTTPStatus.OK)
        except Exception as er:
            logger.error(er)
            return JsonResponse(str(er), status=HTTPStatus.BAD_REQUEST)

    return JsonResponse({'result': 'Image file has not sent'}, status=HTTPStatus.BAD_REQUEST)


@authentication_classes((JSONWebTokenAuthentication,))
def upload_audio(request):

    where = request.body.get('where')
    if where is None:
        return JsonResponse({'error': 'The "where" param is missing'}, status=HTTPStatus.BAD_REQUEST)

    if 'audio' in request.FILES:
        try:
            photo_name_file = str(uuid.uuid4())  # self.add_prefix(request.FILES['photo'].name)
            fs = FileSystemStorage(location=join(settings.MEDIA_ROOT, 'audio', where))
            photo_name_file += '_{}'.format(request.FILES['photo'].name)
            fs.save(photo_name_file, request.FILES['photo'])
            return JsonResponse({'path': photo_name_file}, status=HTTPStatus.OK)
        except Exception as er:
            logger.error(er)
            return JsonResponse(str(er), status=HTTPStatus.BAD_REQUEST)

    return JsonResponse({'result': 'Audio file has not sent'}, status=HTTPStatus.BAD_REQUEST)
