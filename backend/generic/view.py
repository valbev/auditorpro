"""
Provide implementation of custom view functionality.
"""
from http import HTTPStatus
from django.conf import settings
from rest_framework.response import Response
from company.models import Company
from check.models import CheckSchedule


def user_identifier_only_permission(view_method):
    """
    Check if user fetch data corresponding to own identifier.
    """

    def wrapper(_, request, pk, **kwargs):
        """
        Wrapper.
        """
        if request.user.pk != pk:
            return Response({
                'error': 'User has no authority to perform this operation.'
            }, status=HTTPStatus.BAD_REQUEST)

        return view_method(_, request, pk, **kwargs)

    return wrapper


def company_identifier_request_permission(view_method):
    """
    Check if company.yml fetch data corresponding to own identifier.
    """

    def wrapper(_, request, pk, *args, **kwargs):
        """
        Wrapper.
        """
        user_companies = Company.objects.filter(pk=pk, user=request.user)

        if not user_companies:
            return Response({'error': 'Company does not exist.'}, status=HTTPStatus.BAD_REQUEST)

        user_request_company = user_companies.first()

        if request.user != user_request_company.user:
            return Response({
                'error': 'User has no authority to perform this operation.',
            }, status=HTTPStatus.BAD_REQUEST)

        request.company = user_request_company

        return view_method(_, request, pk, **kwargs)

    return wrapper


def check_schedule_request_permission(view_method):
    """
    Check if check schedule fetch data corresponding to own identifier.
    """

    def wrapper(_, request, check_schedule_identifier, *args, **kwargs):
        """
        Wrapper.
        """
        user_check_schedules = CheckSchedule.objects.filter(pk=check_schedule_identifier, user=request.user)

        if not user_check_schedules:
            return Response({'error': 'Check schedule does not exist.'}, status=HTTPStatus.BAD_REQUEST)

        user_check_schedule = user_check_schedules.first()

        if request.user != user_check_schedule.user:
            return Response({
                'error': 'User has no authority to perform this operation.',
            }, status=HTTPStatus.BAD_REQUEST)

        request.check_schedule = user_check_schedule

        return view_method(_, request, check_schedule_identifier, **kwargs)

    return wrapper


def jwt_response_payload_handler(token, user=None, request=None):
    """
    Return token and user_id
    :param token:
    :param user:
    :param request:
    :return:
    """
    return {
        'token': token,
        #'user_id': user.id,
        'token_lifetime': int(settings.JWT_AUTH['JWT_EXPIRATION_DELTA'].total_seconds())  # seconds
    }


def poll_identifier_request_permission(view_method):
    """
    Check if company.yml fetch data corresponding to own identifier.
    """

    def wrapper(_, request, pk, *args, **kwargs):
        """
        Wrapper.
        """
        user_companies = Company.objects.filter(pk=pk, user=request.user)

        if not user_companies:
            return Response({'error': 'Company does not exist.'}, status=HTTPStatus.BAD_REQUEST)

        user_request_company = user_companies.first()

        if request.user != user_request_company.user:
            return Response({
                'error': 'User has no authority to perform this operation.',
            }, status=HTTPStatus.BAD_REQUEST)

        request.company = user_request_company

        return view_method(_, request, pk, **kwargs)

    return wrapper

