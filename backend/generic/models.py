"""
Provide implementation of generic Django database models.
"""
from django.db import models
from multiselectfield import MultiSelectField


class SocialNetworks(models.Model):
    """
    Social networks abstract implementation.
    """

    vk_nickname = models.CharField(max_length=300, blank=True, null=True)
    facebook_nickname = models.CharField(max_length=300, blank=True, null=True)
    instagram_nickname = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
       abstract = True


CHECK_INDUSTRIES = [('0', '---------'),
                    ('1', 'Автосалон'),
                    ('2', 'Агентство недвижимости'),
                    ('3', 'АЗС'),
                    ('4', 'Банк (Финансовые учреждения)'),
                    ('5', 'Ресторан и кафе')]


def name_to_code_industry(name_industry):
    for item_industry in CHECK_INDUSTRIES:
        code, name = item_industry
        if name_industry == name:
            return code
    return None


def code_to_name_industry(code_industry):
    for item_industry in CHECK_INDUSTRIES:
        code, name = item_industry
        if code_industry == code:
            return name
    return '---------'


def names_to_code_industries(names_industries):
    output = []
    for name_industry in names_industries:
        res = name_to_code_industry(name_industry)
        if res:
            output.append(res)
    return output


class Industry(models.Model):

    industries = MultiSelectField(choices=CHECK_INDUSTRIES, blank=True, max_choices=5, max_length=500, default='0')

    class Meta:
       abstract = True


class Notifications(models.Model):

    notify_new_request = models.BooleanField(default=False)
    notify_news = models.BooleanField(default=False)
    notify_analytics = models.BooleanField(default=False)

    class Meta:
       abstract = True

