"""
Provide implementation of enums.
"""
from enum import Enum


class UserType(Enum):
    """
    User types implementation.
    """

    secret_guest = 'secret-guest'
    business = 'business'
