from user.models import SecretGuestProfile, BusinessUserProfile


def get_profile(user):

    if user is None:
        return None

    if SecretGuestProfile.objects.filter(user=user).count() == 1:
        return SecretGuestProfile.objects.filter(user=user).first()

    if BusinessUserProfile.objects.filter(user=user).count() == 1:
        return BusinessUserProfile.objects.filter(user=user).first()

    return None
