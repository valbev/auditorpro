import logging


def log_api(logger=None):

    def actual_decorator(func):

        def wrapper(*args, **kwargs):
            return_value = None
            try:
                return_value = func(*args, **kwargs)
            except Exception as er:
                logger.debug(er)
            return return_value

        return wrapper

    return actual_decorator


class OnlyErrorCriticalFilter(logging.Filter):
    def filter(self, record):
        """
        ERROR or CRITICAL
        :param record:
        :return: False - выбрасывать, True - оставлять
        """
        return record.levelno == logging.ERROR or record.levelno == logging.CRITICAL


class OnlyDebugWarningFilter(logging.Filter):
    def filter(self, record):
        """
        DEBUG or WARNING
        :param record:
        :return: False - выбрасывать, True - оставлять
        """
        return record.levelno == logging.DEBUG or record.levelno == logging.WARNING


class OnlyInfoFilter(logging.Filter):
    def filter(self, record):
        """
        INFO
        :param record:
        :return: False - выбрасывать, True - оставлять
        """
        return record.levelno == logging.INFO
