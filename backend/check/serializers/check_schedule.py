"""
Provide implementation of company.yml models serializers.
"""
from rest_framework import serializers

from check.models import (
    CheckScheduleIntervalPeriod,
    CheckSchedule,
    CheckScheduleIntervalExcludedDates,
    CheckSchedulePayment,
    CheckSchedulePeriod,
    CheckScheduleSecretGuest,
    CheckScheduleWeeklyExcludedDates,
    CheckScheduleWeeklyPeriod,
    CheckScheduleWeeklyPeriodDay,
)
from company.serializers.company import CompanySerializer


class CheckSchedulePeriodSerializer(serializers.ModelSerializer):
    """
    Check schedule period model serializer implementation.
    """

    class Meta:
        model = CheckSchedulePeriod
        fields = ['date_from', 'date_to', 'type']


class CheckScheduleSerializer(serializers.ModelSerializer):
    """
    Check schedule model serializer implementation.
    """

    companies = CompanySerializer(many=True, read_only=True)

    class Meta:
        model = CheckSchedule
        fields = ['companies', 'id', 'name', 'description', 'is_active', 'is_public', 'uuid']
