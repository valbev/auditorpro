from django.contrib import admin

from check.models import (
    CheckScheduleIntervalPeriod,
    CheckSchedule,
    CheckScheduleIntervalExcludedDates,
    CheckSchedulePayment,
    CheckSchedulePeriod,
    CheckScheduleSecretGuest,
    CheckScheduleWeeklyExcludedDates,
    CheckScheduleWeeklyPeriod,
    CheckScheduleWeeklyPeriodDay,
)

admin.site.register(CheckScheduleIntervalPeriod)
admin.site.register(CheckSchedule)
admin.site.register(CheckScheduleIntervalExcludedDates)
admin.site.register(CheckSchedulePayment)
admin.site.register(CheckSchedulePeriod)
admin.site.register(CheckScheduleSecretGuest)
admin.site.register(CheckScheduleWeeklyExcludedDates)
admin.site.register(CheckScheduleWeeklyPeriod)
admin.site.register(CheckScheduleWeeklyPeriodDay)
