"""
Provide database models for check.
"""
from __future__ import unicode_literals
import uuid
from django.db import models
from company.models import (
    WEEK_DAYS,
    Company,
)
from user.models import User

SECRET_GUEST_ONE_MORE_CHECK_TYPES = (
    ('day', 'day'),
    ('week', 'week'),
    ('month', 'month'),
)

CHECK_PAYMENT_TYPES = (
    ('money', 'money'),
)

CHECK_PAYMENT_CURRENCIES = (
    ('rubles', 'rubles'),
)

CHECK_PERIOD_TYPES = (
    ('by-week-days', 'by-week-days'),
    ('by-interval', 'by-interval'),
)

SEXES_STATUSES = (
    ('any', 'any'),
    ('male', 'male'),
    ('female', 'female'),
)


class CheckSchedule(models.Model):
    """
    Check schedule database model.
    """

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    companies = models.ManyToManyField(Company)

    name = models.CharField(max_length=30, blank=False, null=False)
    description = models.CharField(max_length=1000, blank=True)

    is_active = models.BooleanField(default=True)
    is_public = models.BooleanField(default=False)

    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    def __str__(self):
        """
        Get string representation of an object.
        """
        return self.name

    @classmethod
    def does_exit(cls, identifier: dict):
        """
        Check if check schedule exists.
        """
        if cls.objects.filter(id=identifier):
            return True

        return False

    @classmethod
    def delete(cls, identifier: int):
        """
        Delete check schedule database entity.
        """
        cls.objects.filter(id=identifier).delete()

    @classmethod
    def create(cls, user, data: dict):
        """
        Create check schedule.
        """
        return cls.objects.create(user=user, **data)

    @classmethod
    def add_company(cls, identifier, company_identifier):
        """
        Add a company.yml to check schedule.
        """
        check_schedule = cls.objects.get(pk=identifier)
        company = Company.objects.get(pk=company_identifier)

        check_schedule.companies.add(company)


class CheckSchedulePeriod(models.Model):
    """
    Check schedule period database model.
    """

    check_schedule = models.OneToOneField(CheckSchedule, on_delete=models.CASCADE, primary_key=True)

    date_from = models.DateField(blank=False, null=False)
    date_to = models.DateField(blank=False, null=False)

    type = models.CharField(max_length=25, choices=CHECK_PERIOD_TYPES, blank=False, null=False)

    @classmethod
    def create(cls, check_schedule: CheckSchedule, data: dict):
        """
        Create check schedule.
        """
        return cls.objects.create(check_schedule=check_schedule, **data)


class CheckScheduleWeeklyPeriod(models.Model):
    """
    Check schedule weekly period database model.
    """

    check_schedule_period = models.OneToOneField(CheckSchedulePeriod, on_delete=models.CASCADE, primary_key=True)

    @classmethod
    def create(cls, check_schedule_period: CheckSchedulePeriod):
        """
        Create check schedule.
        """
        return cls.objects.create(check_schedule_period=check_schedule_period)


class CheckScheduleWeeklyPeriodDay(models.Model):
    """
    Check schedule weekly period day database model.
    """

    check_schedule_weekly_period = models.ForeignKey(
        CheckScheduleWeeklyPeriod,
        related_name='days',
        on_delete=models.CASCADE,
    )

    day = models.CharField(max_length=10, choices=WEEK_DAYS, blank=False, null=False)

    def __str__(self):
        """
        Get string representation of an object.
        """
        return f'{self.check_schedule_weekly_period.check_schedule.name}. {self.day}: ' \
                   f'{self.check_schedule_weekly_period.date_from} — {self.check_schedule_weekly_period.date_to}'

    @classmethod
    def create(cls, check_schedule_weekly_period: CheckScheduleWeeklyPeriod, days: dict):
        """
        Create company.yml schedule day database entity.
        """
        for day in days:
            cls.objects.create(check_schedule_weekly_period=check_schedule_weekly_period, **day)


class CheckScheduleWeeklyExcludedDates(models.Model):
    """
    Check schedule weekly period excluded dates database model.
    """

    check_schedule_weekly_period = models.ForeignKey(
        CheckScheduleWeeklyPeriod,
        related_name='excluded_days',
        on_delete=models.CASCADE,
    )

    date = models.DateField()

    def __str__(self):
        """
        Get string representation of an object.
        """
        return f'{self.check_schedule_weekly_period.check_schedule.name}. {self.day}: ' \
                   f'{self.check_schedule_weekly_period.date_from} — {self.check_schedule_weekly_period.date_to}'

    @classmethod
    def create(cls, check_schedule_weekly_period: CheckScheduleWeeklyPeriodDay, dates: dict):
        """
        Create company.yml schedule day database entity.
        """
        for date in dates:
            cls.objects.create(check_schedule_weekly_period=check_schedule_weekly_period, **date)


class CheckScheduleIntervalPeriod(models.Model):
    """
    Check schedule weekly period database model.
    """

    check_schedule_period = models.OneToOneField(CheckSchedulePeriod, on_delete=models.CASCADE, primary_key=True)


class CheckScheduleIntervalExcludedDates(models.Model):
    """
    Check schedule interval period excluded dates database model.
    """

    check_schedule_interval_period = models.ForeignKey(
        CheckScheduleIntervalPeriod,
        related_name='excluded_dates',
        on_delete=models.CASCADE,
    )

    date = models.DateField()

    def __str__(self):
        """
        Get string representation of an object.
        """
        return f'{self.check_schedule_interval_period.check_schedule.name}. {self.day}: ' \
                   f'{self.check_schedule_interval_period.date_from} — {self.check_schedule_interval_period.date_to}'

    @classmethod
    def create(cls, check_schedule_interval_period: CheckScheduleIntervalPeriod, dates: dict):
        """
        Create company.yml schedule day database entity.
        """
        for date in dates:
            cls.objects.create(check_schedule_interval_period=check_schedule_interval_period, **date)


class CheckSchedulePayment(models.Model):
    """
    Check schedule payment database model.
    """

    check_schedule = models.OneToOneField(CheckSchedule, on_delete=models.CASCADE, primary_key=True)

    type = models.CharField(max_length=10, choices=CHECK_PAYMENT_TYPES, blank=False, null=False)
    amount = models.IntegerField(blank=True, null=True)
    currency = models.CharField(max_length=10, choices=CHECK_PAYMENT_CURRENCIES, blank=True, null=True)


class CheckScheduleSecretGuest(models.Model):
    """
    Check schedule secret guest database model.
    """

    check_schedule = models.OneToOneField(CheckSchedule, on_delete=models.CASCADE, primary_key=True)

    age_from = models.IntegerField(blank=False, null=False, default=0)
    male = models.CharField(max_length=10, choices=SEXES_STATUSES, blank=False, null=False)

    one_more_check = models.BooleanField(default=False)
    one_more_check_limitation = models.BooleanField(default=True)
    one_more_check_number = models.IntegerField(blank=True, null=True)
    one_more_check_type = models.CharField(
        max_length=10, choices=SECRET_GUEST_ONE_MORE_CHECK_TYPES, blank=True, null=True,
    )
