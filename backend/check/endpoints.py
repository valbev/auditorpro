"""
Provide implementation of company endpoints.
"""
from django.urls import path

from check.views.check_schedule import (
    CheckScheduleCollection,
    CheckScheduleSingle,
)

check_schedule_endpoints = [
    path('', CheckScheduleCollection.as_view()),
    path('<int:id>/', CheckScheduleSingle.as_view()),
]
