"""
Provide implementation of company.yml endpoints.
"""
from http import HTTPStatus

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.decorators import authentication_classes
from rest_framework.response import Response
from rest_framework.views import APIView

from generic.view import check_schedule_request_permission
from check.serializers.check_schedule import (
    CheckScheduleSerializer,
    CheckSchedulePeriodSerializer,
)
from company.models import (
    Company
)
from check.models import (
    CheckScheduleIntervalPeriod,
    CheckSchedule,
    CheckScheduleIntervalExcludedDates,
    CheckSchedulePayment,
    CheckSchedulePeriod,
    CheckScheduleSecretGuest,
    CheckScheduleWeeklyExcludedDates,
    CheckScheduleWeeklyPeriod,
    CheckScheduleWeeklyPeriodDay,
)


class CheckScheduleSingle(APIView):
    """
    Check schedule single endpoints endpoint implementation.
    """

    @check_schedule_request_permission
    @authentication_classes((JSONWebTokenAuthentication,))
    def delete(self, request, id, format=None):
        """
        Delete company.yml's schedule.
        """
        CheckSchedule.delete(identifier=id)
        return Response({'result': 'Check schedule has been deleted.'}, status=HTTPStatus.OK)


class CheckScheduleCollection(APIView):
    """
    Check schedule collection endpoints endpoint implementation.
    """

    def put(self, request, format=None):
        """
        Create a check schedule.
        """
        companies_ids = request.data.get('companies_ids')

        del request.data['companies_ids']

        check_schedule_serializer = CheckScheduleSerializer(data=request.data)

        if not check_schedule_serializer.is_valid():
            return Response({'errors': check_schedule_serializer.errors}, status=HTTPStatus.BAD_REQUEST)

        for company_id in companies_ids:
            if not Company.does_exist(identifier=company_id):
                return Response({'errors': f'Company with identifier `{company_id}` does not exist.'}, status=HTTPStatus.BAD_REQUEST)

        check_schedule = CheckSchedule.create(user=request.user, data=request.data)

        for company_id in companies_ids:
            CheckSchedule.add_company(identifier=check_schedule.id, company_identifier=company_id)

        return Response({'result': CheckScheduleSerializer(check_schedule).data}, status=HTTPStatus.CREATED)
