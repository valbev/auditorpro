#!/usr/bin/env bash

docker run -it -p 6432:6432 -p 9002:9002 --name auditor-api-service --rm auditor_api python3.7 ./manage.py runserver 0.0.0.0:9002