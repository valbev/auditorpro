"""
Provide configuration for admin panel poll tables.
"""
from django.contrib import admin

from poll.models.poll import Poll
from poll.models.questions import DivisionQuestion, ItemQuestion, ManyFromListQuestion, MediaFile, MediaQuestion, \
    OneFromListQuestion, RatingQuestion, TextQuestion

admin.site.register(Poll)
admin.site.register(DivisionQuestion)
admin.site.register(ItemQuestion)
admin.site.register(ManyFromListQuestion)
admin.site.register(MediaFile)
admin.site.register(MediaQuestion)
admin.site.register(OneFromListQuestion)
admin.site.register(RatingQuestion)
admin.site.register(TextQuestion)



