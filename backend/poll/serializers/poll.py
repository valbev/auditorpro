from rest_framework import serializers

from generic.users import get_profile
from poll.models.poll import Poll
from poll.models.questions import DivisionQuestion, OneFromListQuestion, ItemQuestion
from poll.serializers.questions import DivisionQuestionSerializer, ManyFromListQuestionSerializer, \
    OneFromListQuestionSerializer, TextQuestionSerializer, RatingQuestionSerializer, MediaQuestionSerializer


class PollSerializer(serializers.ModelSerializer):
    """
    Poll model serializer implementation.
    """

    class Meta:
        model = Poll

    def __init__(self, data):
        super(PollSerializer, self).__init__()
        # TODO implement validate
        self.is_valid = True

    def to_representation(self, instance):
        questions = []
        if instance.divisionquestion_set:
            questions.extend(list(instance.divisionquestion_set.all()))
        if instance.onefromlistquestion_set:
            questions.extend(list(instance.onefromlistquestion_set.all()))
        if instance.manyfromlistquestion_set:
            questions.extend(list(instance.manyfromlistquestion_set.all()))
        if instance.textquestion_set:
            questions.extend(list(instance.textquestion_set.all()))
        if instance.ratingquestion_set:
            questions.extend(list(instance.ratingquestion_set.all()))
        if instance.mediaquestion_set:
            questions.extend(list(instance.mediaquestion_set.all()))

        questions = sorted(questions, key=lambda x: x.order_id)
        questions_serialized = []
        for question in questions:
            if question.question_type == 'DivisionQuestion':
                questions_serialized.append(DivisionQuestionSerializer().to_representation(question))
            if question.question_type == 'ManyFromListQuestion':
                questions_serialized.append(ManyFromListQuestionSerializer().to_representation(question))
            if question.question_type == 'OneFromListQuestion':
                questions_serialized.append(OneFromListQuestionSerializer().to_representation(question))
            if question.question_type == 'TextQuestion':
                questions_serialized.append(TextQuestionSerializer().to_representation(question))
            if question.question_type == 'RatingQuestion':
                questions_serialized.append(RatingQuestionSerializer().to_representation(question))
            if question.question_type == 'MediaQuestion':
                questions_serialized.append(MediaQuestionSerializer().to_representation(question))
        profile = get_profile(instance.user)
        return {
            'poll_id': instance.poll_id,
            'author': profile.full_name,  # instance.user.email
            'title': instance.title,
            'test_mode_global': instance.test_mode_global,
            'questions': questions_serialized
        }

    def loсal_update(self, question, model_class, model_serializer):
        if 'question_id' in question:
            edit_qu = model_class.objects.filter(question_id=question['question_id'])
            if edit_qu.count() > 0:
                model_serializer.update(edit_qu.first(), question)
        else:
            new_div = model_serializer.create(question)
            new_div.save()

    def update(self, instance, validated_data):

        # for question in validated_data['questions']:
        #     question['poll_id'] = instance.poll_id
        #     if question['question_type'] == 'DivisionQuestion':
        #         new_div = DivisionQuestionSerializer.create(question)
        #         new_div.save()
        #     if question['question_type'] == 'OneFromListQuestion':
        #         items = question['items']
        #         del question['items']
        #         new_qu = OneFromListQuestionSerializer.create(question)
        #         new_item = ItemQuestion.objects.create(**items)
        #         new_qu.items.add(new_item)
        #         new_qu.save()

        instance.title = validated_data['title'] if 'title' in validated_data else ''
        instance.test_mode_global = validated_data['test_mode_global']  #.lower() == 'true' if 'test_mode_global' in validated_data else False
        instance.save()
        return instance

    def create(self, user, validated_data):
        poll = Poll()
        poll.user = user
        poll.title = validated_data['title'] if 'title' in validated_data else ''
        poll.test_mode_global = validated_data['test_mode_global'].lower() == 'true' if 'test_mode_global' in validated_data else False
        poll.save()
        # for question in validated_data['questions']:
        #     question['poll_id'] = poll.poll_id
        #     if question['question_type'] == 'DivisionQuestion':
        #         new_div = DivisionQuestionSerializer.create(question)
        #         new_div.save()
        #     if question['question_type'] == 'OneFromListQuestion':
        #         items = question['items']
        #         del question['items']
        #         new_qu = OneFromListQuestionSerializer.create(question)
        #         new_item = ItemQuestion.objects.create(**items)
        #         new_qu.items.add(new_item)
        #         new_qu.save()
        return poll
