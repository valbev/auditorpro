from rest_framework import serializers

from poll.models.questions import DivisionQuestion, ItemQuestion, ManyFromListQuestion, OneFromListQuestion, \
    RatingQuestion, MediaFile, MediaQuestion, TextQuestion, Question


class BaseQuestionSerializer(serializers.Serializer):
    question_id = serializers.IntegerField(read_only=True)
    question_type = serializers.CharField(read_only=True, max_length=100, default='Question')
    order_id = serializers.IntegerField()
    description = serializers.CharField(max_length=512)
    comment = serializers.CharField(max_length=512)
    caption = serializers.CharField(max_length=512)

    require = serializers.BooleanField(required=False)
    mix_answers = serializers.BooleanField(required=False)
    time_for_answer = serializers.BooleanField(required=False)
    answer_time = serializers.IntegerField(default=0, required=False)
    type_for_show = serializers.IntegerField(default=0, required=False)
    title_image = serializers.CharField(max_length=512, required=False)
    resize_image = serializers.BooleanField(required=False)
    test_mode = serializers.BooleanField(required=False)

    def update(self, instance, validated_data):
        instance.order_id = validated_data.get('order_id', instance.order_id)
        instance.description = validated_data.get('description', instance.description)
        instance.comment = validated_data.get('comment', instance.comment)
        instance.caption = validated_data.get('caption', instance.caption)

        # Необязательные поля
        instance.require = validated_data.get('require', instance.require)
        instance.mix_answers = validated_data.get('mix_answers', instance.mix_answers)
        instance.time_for_answer = validated_data.get('time_for_answer', instance.time_for_answer)
        instance.answer_time = validated_data.get('answer_time', instance.answer_time)
        instance.type_for_show = validated_data.get('type_for_show', instance.type_for_show)
        instance.title_image = validated_data.get('title_image', instance.title_image)
        instance.resize_image = validated_data.get('resize_image', instance.resize_image)
        instance.test_mode = validated_data.get('test_mode', instance.test_mode)


class DivisionQuestionSerializer(BaseQuestionSerializer):
    @staticmethod
    def create(validated_data):
        return DivisionQuestion.objects.create(**validated_data)

    def update(self, instance, validated_data):
        super(DivisionQuestionSerializer, self).update(instance, validated_data)
        instance.save()
        return instance


class ItemQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemQuestion
        fields = ['item_question_id', 'order_id', 'text', 'checked', 'photo_path', 'points']


class OneFromListQuestionSerializer(BaseQuestionSerializer):

    items = ItemQuestionSerializer(many=True)

    @staticmethod
    def create(validated_data):
        question_items = validated_data['items']
        del validated_data['items']

        many_from_list_question = OneFromListQuestion.objects.create(**validated_data)

        for question_item in question_items:
            many_from_list_question.items.add(ItemQuestion.objects.create(**question_item))

        validated_data['items'] = question_items
        return many_from_list_question

    def update(self, instance, validated_data):
        super(OneFromListQuestionSerializer, self).update(instance, validated_data)
        _items = validated_data.get('items', instance.items)
        if _items:
            instance.items.clear()
            for question_item in _items:
                instance.items.add(ItemQuestion.objects.create(**question_item))
        instance.save()
        return instance


class ManyFromListQuestionSerializer(BaseQuestionSerializer):

    description_mode = serializers.BooleanField(required=False)
    count_of_answer = serializers.IntegerField(required=False)
    current_number_value = serializers.IntegerField(required=False)
    answer_from = serializers.IntegerField(required=False)
    answer_to = serializers.IntegerField(required=False)

    items = ItemQuestionSerializer(many=True)

    def create(self, validated_data):
        question_items = validated_data['items']
        del validated_data['items']

        many_from_list_question = ManyFromListQuestion.objects.create(**validated_data)

        for question_item in question_items:
            many_from_list_question.items.add(ItemQuestion.objects.create(**question_item))

        validated_data['items'] = question_items
        return many_from_list_question

    def update(self, instance, validated_data):
        super(ManyFromListQuestionSerializer, self).update(instance, validated_data)

        # Необязательные поля
        instance.description_mode = validated_data.get('description_mode', instance.description_mode)
        instance.count_of_answer = validated_data.get('count_of_answer', instance.count_of_answer)
        instance.current_number_value = validated_data.get('current_number_value', instance.current_number_value)
        instance.answer_from = validated_data.get('answer_from', instance.answer_from)
        instance.answer_to = validated_data.get('answer_to', instance.answer_to)

        _items = validated_data.get('items', instance.items)
        if _items:
            instance.items.clear()
            for question_item in _items:
                instance.items.add(ItemQuestion.objects.create(**question_item))
        instance.save()
        return instance


class RatingQuestionSerializer(BaseQuestionSerializer):

    rating = serializers.IntegerField()

    def create(self, validated_data):
        return RatingQuestion.objects.create(**validated_data)

    def update(self, instance, validated_data):
        super(RatingQuestionSerializer, self).update(instance, validated_data)
        instance.rating = validated_data.get('rating', instance.rating)
        instance.save()
        return instance


class MediaFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaFile
        fields = ['file_id', 'path_to_file']


class MediaQuestionSerializer(BaseQuestionSerializer):

    files = MediaFileSerializer(many=True)

    def create(self, validated_data):
        files = validated_data['files']
        del validated_data['files']

        media_question = MediaQuestion.objects.create(**validated_data)

        for file in files:
            media_question.files.add(MediaFile.objects.create(**file))

        validated_data['files'] = files
        return media_question

    def update(self, instance, validated_data):
        super(MediaQuestionSerializer, self).update(instance, validated_data)
        _items = validated_data.get('files', instance.files)
        if _items:
            instance.files.clear()
            for file in _items:
                instance.files.add(MediaFile.objects.create(**file))
        instance.save()
        return instance


class TextQuestionSerializer(BaseQuestionSerializer):

    text = serializers.CharField(max_length=1024)

    def create(self, validated_data):
        return TextQuestion.objects.create(**validated_data)

    def update(self, instance, validated_data):
        super(TextQuestionSerializer, self).update(instance, validated_data)
        instance.text = validated_data.get('text', instance.text)
        instance.save()
        return instance