from rest_framework import serializers
from poll.models.answer import AnswerQuestion


class AnswerSerializer(serializers.ModelSerializer):

    class Meta:
        model = AnswerQuestion

    # def __init__(self, data):
    #     super(AnswerSerializer, self).__init__(data)
    #     # TODO implement validate
    #     self.is_valid = True

    def to_representation(self, instance):
        return {'poll_id': instance.poll_id,
                'text': instance.text,
                'user_id': instance.user_id,
                'photo_path': instance.photo_path,
                'items_question': instance.items_question,
                'checked': instance.checked,
                'id': instance.id
                }

    def create(self, validated_data):
        new_answer = AnswerQuestion(**validated_data)
        return new_answer
