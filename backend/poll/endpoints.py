from django.urls import path
from poll.views.answers import AnswerCollection, AnswerCollectionSet
from poll.views.items_question import ItemQuestionCollection
from poll.views.poll import PollCollection, PollCollectionSet
from poll.views.questions import QuestionCollection


poll_endpoints = [

    # Poll
    path('', PollCollectionSet.as_view()),
    path('poll/<int:pk>/', PollCollection.as_view()),
    path('<slug:slug>/', PollCollectionSet.as_view()),

    # Answers
    path('answers/', AnswerCollectionSet.as_view()),
    path('answers/<slug:slug>/', AnswerCollectionSet.as_view()),
    path('answer/<int:pk>/', AnswerCollection.as_view()),

    # Questions
    path('<int:pk>/questions/', QuestionCollection.as_view()),
    path('questions/<int:pk>/<str:question_type>/', QuestionCollection.as_view()),

    # Items
    path('questions/items/<int:pk>/', ItemQuestionCollection.as_view()),

    #
    # path('<int:pk>/fields/', CompanyFieldCollection.as_view()),
    # path('<int:pk>/fields/<int:field_identifier>/', CompanyFieldSingle.as_view()),
]
