from django.db import models
from user.models import User
from django.contrib.postgres.fields import ArrayField


class Question(models.Model):
    question_id = models.AutoField(primary_key=True)
    order_id = models.IntegerField(default=0)
    poll = models.ForeignKey('Poll', on_delete=models.CASCADE)
    caption = models.CharField(max_length=100)
    description = models.CharField(max_length=512)
    comment = models.CharField(max_length=512)
    question_type = models.CharField(max_length=100, default='Question')

    # Необязательные поля
    require = models.BooleanField(default=False)
    mix_answers = models.BooleanField(default=False)
    time_for_answer = models.BooleanField(default=False)
    answer_time = models.IntegerField(default=0)
    type_for_show = models.IntegerField(default=0)
    title_image = models.CharField(max_length=512, default='')
    resize_image = models.BooleanField(default=False)
    test_mode = models.BooleanField(default=False)

    class Meta:
        abstract = True


class DivisionQuestion(Question):
    """
    Division
    """
    def __init__(self, *args, **kwargs):
        super(DivisionQuestion, self).__init__(*args, **kwargs)
        self.question_type = __class__.__name__

    class Meta:
        db_table = 'poll_question_division'
        verbose_name_plural = 'poll_divisions'


class ItemQuestion(models.Model):
    item_question_id = models.AutoField(primary_key=True)
    order_id = models.IntegerField(default=0)
    text = models.CharField(max_length=100, default='')
    checked = models.BooleanField(default=False)
    photo_path = models.CharField(max_length=500, default='')
    points = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'poll_question_item'
        verbose_name_plural = 'poll_item_questions'


class ManyFromListQuestion(Question):
    """
    ManyFromList
    """
    items = models.ManyToManyField(ItemQuestion)

    # Необязательные поля
    description_mode = models.BooleanField(default=False)
    count_of_answer = models.IntegerField(default=0)
    current_number_value = models.IntegerField(default=None, blank=True, null=True)
    answer_from = models.IntegerField(default=None, blank=True, null=True)
    answer_to = models.IntegerField(default=None, blank=True, null=True)

    def __init__(self, *args, **kwargs):
        super(ManyFromListQuestion, self).__init__(*args, **kwargs)
        self.question_type = __class__.__name__

    class Meta:
        db_table = 'poll_question_many_from_list'
        verbose_name_plural = 'poll_many_from_list'


class OneFromListQuestion(Question):
    """
    OneFromList
    """
    items = models.ManyToManyField(ItemQuestion)

    def __init__(self, *args, **kwargs):
        super(OneFromListQuestion, self).__init__(*args, **kwargs)
        self.question_type = __class__.__name__

    class Meta:
        db_table = 'poll_question_one_from_list'
        verbose_name_plural = 'poll_one_from_list'


class RatingQuestion(Question):
    """
    Rating
    """
    rating = models.IntegerField(default=0)

    def __init__(self, *args, **kwargs):
        super(RatingQuestion, self).__init__(*args, **kwargs)
        self.question_type = __class__.__name__

    class Meta:
        db_table = 'poll_question_rating'
        verbose_name_plural = 'poll_rating_questions'


class MediaFile(models.Model):
    file_id = models.IntegerField(primary_key=True)
    path_to_file = models.CharField(max_length=2048)

    class Meta:
        db_table = 'poll_media_file'
        verbose_name_plural = 'poll_media_files'


class MediaQuestion(Question):
    """
    Media
    """
    files = models.ManyToManyField(MediaFile)

    def __init__(self, *args, **kwargs):
        super(MediaQuestion, self).__init__(*args, **kwargs)
        self.question_type = __class__.__name__

    class Meta:
        db_table = 'poll_question_media'
        verbose_name_plural = 'poll_media_questions'


class TextQuestion(Question):
    """
    Just Text
    """
    text = models.TextField(default='')

    def __init__(self, *args, **kwargs):
        super(TextQuestion, self).__init__(*args, **kwargs)
        self.question_type = __class__.__name__

    class Meta:
        db_table = 'poll_question_text'
        #ordering = ['number']
        verbose_name_plural = 'poll_text_questions'
