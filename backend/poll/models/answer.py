from django.db import models
from user.models import User
from django.contrib.postgres.fields import ArrayField


class AnswerQuestion(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    poll_id = models.IntegerField(blank=False, null=False)
    question_id = models.IntegerField(blank=False, null=False)
    question_type = models.CharField(max_length=100, default='NoType')
    text = models.CharField(max_length=500, default=None, blank=True, null=True)
    checked = models.BooleanField(default=None, blank=True, null=True)
    photo_path = models.CharField(max_length=500, default=None, blank=True, null=True)
    items_question = ArrayField(ArrayField(models.IntegerField()), blank=True, null=True)  # выбор элементов из списка в вопросе

    class Meta:
        db_table = 'poll_answer_question'
        #ordering = ['number']
        verbose_name_plural = 'poll_answer_questions'
