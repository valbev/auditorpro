from django.db import models
from user.models import User


class Poll(models.Model):
    """
    Poll
    """
    poll_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=200, default='')
    image = models.CharField(max_length=200, default='')
    test_mode_global = models.BooleanField(default=False)

    class Meta:
        db_table = 'poll'
        verbose_name_plural = 'polls'

    def delete(self, using=None, keep_parents=False):
        for d in self.divisionquestion_set.all():
            d.delete()
        for d in self.onefromlistquestion_set.all():
            d.delete()
        super(Poll, self).delete()
