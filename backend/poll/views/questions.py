from http import HTTPStatus
from rest_framework.response import Response
from rest_framework.views import APIView

from poll.models.poll import Poll
from poll.models.questions import ManyFromListQuestion, OneFromListQuestion, RatingQuestion, MediaQuestion, \
    TextQuestion, DivisionQuestion
from poll.serializers.questions import ManyFromListQuestionSerializer, OneFromListQuestionSerializer, \
    RatingQuestionSerializer, MediaQuestionSerializer, TextQuestionSerializer, DivisionQuestionSerializer


class QuestionCollection(APIView):

    # @user_identifier_only_permission
    #@authentication_classes((JSONWebTokenAuthentication,))
    #def get(self, request, pk, question_type=None, format=None):

    def get(self, request, pk, question_type=None, format=None):
        """
        Get question
        """
        try:
            serialized_questions = None
            if question_type == 'DivisionQuestion':
                question = DivisionQuestion.objects.get(question_id=pk)
                serialized_questions = DivisionQuestionSerializer(question)
            if question_type == 'ManyFromListQuestion':
                question = ManyFromListQuestion.objects.get(question_id=pk)
                serialized_questions = ManyFromListQuestionSerializer(question)
            if question_type == 'OneFromListQuestion':
                question = OneFromListQuestion.objects.get(question_id=pk)
                serialized_questions = OneFromListQuestionSerializer(question)
            if question_type == 'RatingQuestion':
                question = RatingQuestion.objects.get(question_id=pk)
                serialized_questions = RatingQuestionSerializer(question)
            if question_type == 'TextQuestion':
                question = TextQuestion.objects.get(question_id=pk)
                serialized_questions = TextQuestionSerializer(question)
            if question_type == 'MediaQuestion':
                question = MediaQuestion.objects.get(question_id=pk)
                serialized_questions = MediaQuestionSerializer(question)

            if serialized_questions:
                return Response({'result': serialized_questions.data}, status=HTTPStatus.OK)
            else:
                raise Exception('Bad request')

        except ManyFromListQuestion.DoesNotExist or OneFromListQuestion.DoesNotExist or RatingQuestion.DoesNotExist \
               or ManyFromListQuestion.DoesNotExist or TextQuestion.DoesNotExist or MediaQuestion.DoesNotExist:
            return Response({'result': 'Not found'}, status=HTTPStatus.NOT_FOUND)
        except Exception as er:
            return Response({'error': ''.format(er)}, status=HTTPStatus.BAD_REQUEST)

    def put(self, request, pk, question_type, format=None):

        try:

            edit_question = None
            serialized_questions = None
            if question_type == 'DivisionQuestion':
                question = DivisionQuestion.objects.get(question_id=pk)
                edit_question = DivisionQuestionSerializer().update(question, request.data)
                serialized_questions = DivisionQuestionSerializer(edit_question)
            if question_type == 'ManyFromListQuestion':
                question = ManyFromListQuestion.objects.get(question_id=pk)
                edit_question = ManyFromListQuestionSerializer().update(question, request.data)
                serialized_questions = ManyFromListQuestionSerializer(edit_question)
            if question_type == 'OneFromListQuestion':
                question = OneFromListQuestion.objects.get(question_id=pk)
                edit_question = OneFromListQuestionSerializer().update(question, request.data)
                serialized_questions = OneFromListQuestionSerializer(edit_question)
            if question_type == 'RatingQuestion':
                question = RatingQuestion.objects.get(question_id=pk)
                edit_question = RatingQuestionSerializer().update(question, request.data)
                serialized_questions = RatingQuestionSerializer(edit_question)
            if question_type == 'TextQuestion':
                question = TextQuestion.objects.get(question_id=pk)
                edit_question = TextQuestionSerializer().update(question, request.data)
                serialized_questions = TextQuestionSerializer(edit_question)
            if question_type == 'MediaQuestion':
                question = MediaQuestion.objects.get(question_id=pk)
                edit_question = MediaQuestionSerializer().update(question, request.data)
                serialized_questions = MediaQuestionSerializer(edit_question)

            if edit_question:
                return Response({'result': serialized_questions.data}, status=HTTPStatus.OK)
            else:
                raise Exception('Bad request')

        except ManyFromListQuestion.DoesNotExist or OneFromListQuestion.DoesNotExist or RatingQuestion.DoesNotExist \
               or ManyFromListQuestion.DoesNotExist or TextQuestion.DoesNotExist or MediaQuestion.DoesNotExist:
            return Response({'result': 'Not found'}, status=HTTPStatus.NOT_FOUND)
        except Exception as er:
            return Response({'error': ''.format(er)}, status=HTTPStatus.BAD_REQUEST)

    def post(self, request, pk, format=None):

        try:

            poll = Poll.objects.get(poll_id=pk)
            if 'question_type' in request.data:
                question_type = request.data['question_type']
            else:
                return Response({'error': 'Question type is not found'}, status=HTTPStatus.BAD_REQUEST)

            request.data['poll_id'] = poll.pk
            question_serializer = None

            if question_type == 'DivisionQuestion':
                question_serializer = DivisionQuestionSerializer(request.data)
            if question_type == 'ManyFromListQuestion':
                question_serializer = ManyFromListQuestionSerializer(request.data)
            if question_type == 'OneFromListQuestion':
                question_serializer = OneFromListQuestionSerializer(request.data)
            if question_type == 'RatingQuestion':
                question_serializer = RatingQuestionSerializer(request.data)
            if question_type == 'TextQuestion':
                question_serializer = TextQuestionSerializer(request.data)
            if question_type == 'MediaQuestion':
                question_serializer = MediaQuestionSerializer(request.data)

            if question_serializer.is_valid:
                new_question = question_serializer.create(request.data)
                new_question.save()
            else:
                Response({'error': 'Question is not valid'}, status=HTTPStatus.BAD_REQUEST)

            if question_serializer:
                if isinstance(new_question, ManyFromListQuestion):
                    return Response({'result': ManyFromListQuestionSerializer(new_question).data}, status=HTTPStatus.OK)
                result = question_serializer.data
                result['question_id'] = new_question.question_id
                return Response({'result': result}, status=HTTPStatus.OK)
            else:
                raise Exception('Bad request')

        except Poll.DoesNotExist:
            return Response({'result': 'Poll is not found'}, status=HTTPStatus.NOT_FOUND)
        except ManyFromListQuestion.DoesNotExist or OneFromListQuestion.DoesNotExist or RatingQuestion.DoesNotExist \
               or ManyFromListQuestion.DoesNotExist or TextQuestion.DoesNotExist or MediaQuestion.DoesNotExist:
            return Response({'result': 'Question is not found'}, status=HTTPStatus.NOT_FOUND)
        except Exception as er:
            return Response({'error': ''.format(str(er))}, status=HTTPStatus.BAD_REQUEST)

    def delete(self, request, pk, question_type, format=None):
        try:
            count = 0
            if question_type == 'DivisionQuestion':
                count = DivisionQuestion.objects.filter(question_id=pk).delete()
            if question_type == 'ManyFromListQuestion':
                count = ManyFromListQuestion.objects.filter(question_id=pk).delete()
            if question_type == 'OneFromListQuestion':
                count = OneFromListQuestion.objects.filter(question_id=pk).delete()
            if question_type == 'RatingQuestion':
                count = RatingQuestion.objects.filter(question_id=pk).delete()
            if question_type == 'TextQuestion':
                count = TextQuestion.objects.filter(question_id=pk).delete()
            if question_type == 'MediaQuestion':
                count = MediaQuestion.objects.filter(question_id=pk).delete()

            if count == 0:
                return Response({'result': 'This question is not found'}, status=HTTPStatus.NOT_FOUND)

        except ManyFromListQuestion.DoesNotExist or OneFromListQuestion.DoesNotExist or RatingQuestion.DoesNotExist \
               or ManyFromListQuestion.DoesNotExist or TextQuestion.DoesNotExist or MediaQuestion.DoesNotExist:
            return Response({'result': 'Question is not found'}, status=HTTPStatus.NOT_FOUND)
        except Exception as er:
            return Response({'error': str(er)}, status=HTTPStatus.INTERNAL_SERVER_ERROR)
        return Response({'result': 'The question has been deleted'}, status=HTTPStatus.OK)




