from http import HTTPStatus
from rest_framework.response import Response
from rest_framework.views import APIView
from poll.models.answer import AnswerQuestion
from poll.serializers.answer import AnswerSerializer


class AnswerCollectionSet(APIView):

    def get(self, request, slug='my', format=None):
        if slug == 'my':
            answers = AnswerQuestion.objects.filter(user=request.user).all()
        if slug == 'all':
            answers = AnswerQuestion.objects.all()
        serialized_answer = AnswerSerializer(answers, many=True)
        return Response({'result': serialized_answer.data}, status=HTTPStatus.OK)

    def post(self, request, format=None):
        try:
            serialized_answer = AnswerSerializer(request.data)
            if serialized_answer.is_valid:
                validated_data = request.data
                validated_data['user'] = request.user
                new_answer = serialized_answer.create(validated_data)
                new_answer.save()
        except Exception as er:
            return Response({'error': str(er)}, status=HTTPStatus.INTERNAL_SERVER_ERROR)
        return Response({'result': 'The answer has been created'}, status=HTTPStatus.OK)


class AnswerCollection(APIView):

    def get(self, request, pk, format=None):
        try:
            answer = AnswerQuestion.objects.get(id=pk)
        except AnswerQuestion.DoesNotExist:
            return Response({'result': 'This answer is not exist'}, status=HTTPStatus.NOT_FOUND)
        serialized_answer = AnswerSerializer(answer, many=False)
        return Response({'result': serialized_answer.data}, status=HTTPStatus.OK)

    # def put(self, request, pk, format=None):
    #     try:
    #         poll_serializer = RollSerializer(request.data)
    #         if poll_serializer.is_valid:
    #             new_poll = poll_serializer.create(request.user, request.data)
    #             new_poll.save()
    #     except Exception as er:
    #         return Response({'error': str(er)}, status=HTTPStatus.INTERNAL_SERVER_ERROR)
    #     return Response({'result': 'The poll has been created'}, status=HTTPStatus.OK)

    def delete(self, request, pk, format=None):
        try:
            polls_count = AnswerQuestion.objects.filter(poll_id=pk).delete()
            if polls_count == 0:
                return Response({'result': 'This answer is not found'}, status=HTTPStatus.NOT_FOUND)
        except Exception as er:
            return Response({'error': str(er)}, status=HTTPStatus.INTERNAL_SERVER_ERROR)
        return Response({'result': 'The answer has been deleted'}, status=HTTPStatus.OK)
