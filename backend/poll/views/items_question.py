from http import HTTPStatus
from rest_framework.response import Response
from rest_framework.views import APIView
from poll.models.questions import ItemQuestion, ManyFromListQuestion
from poll.serializers.questions import ItemQuestionSerializer


class ItemQuestionCollection(APIView):

    def put(self, request, pk, format=None):

        try:
            item_question = ItemQuestion.objects.get(item_question_id=pk)
            item_question = ItemQuestionSerializer().update(item_question, request.data)
            serialized_item_question = ItemQuestionSerializer(item_question)

            if item_question:
                return Response({'result': serialized_item_question.data}, status=HTTPStatus.OK)
            else:
                raise Exception('Bad request')

        except ItemQuestion.DoesNotExist:
            return Response({'result': 'Not found'}, status=HTTPStatus.NOT_FOUND)
        except Exception as er:
            return Response({'error': ''.format(er)}, status=HTTPStatus.BAD_REQUEST)

    def post(self, request,  pk, format=None):

        try:

            question = ManyFromListQuestion.objects.get(question_id=pk)
            new_item_question = ItemQuestionSerializer(data=request.data)
            if new_item_question.is_valid():
                new_item_question.save()

            question.items.add(new_item_question.instance)
            question.save()

            item_question = ItemQuestionSerializer(new_item_question.instance)
            return Response({'result': item_question.data}, status=HTTPStatus.OK)

        except ManyFromListQuestion.DoesNotExist:
            return Response({'result': 'ManyFromListQuestion is not found'}, status=HTTPStatus.NOT_FOUND)
        except Exception as er:
            return Response({'error': ''.format(str(er))}, status=HTTPStatus.BAD_REQUEST)

    def delete(self, request, pk, format=None):
        try:
            count = ItemQuestion.objects.filter(item_question_id=pk).delete()

            if count == 0:
                return Response({'result': 'This item question is not found'}, status=HTTPStatus.NOT_FOUND)

        except Exception as er:
            return Response({'error': str(er)}, status=HTTPStatus.INTERNAL_SERVER_ERROR)
        return Response({'result': 'The item question has been deleted'}, status=HTTPStatus.OK)
