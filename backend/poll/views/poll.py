from http import HTTPStatus
from django.http import JsonResponse
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.views import APIView

from generic.permistions import PublicEndpoint
from generic.view import user_identifier_only_permission
from poll.models.poll import Poll

from rest_framework.permissions import AllowAny

from poll.serializers.poll import PollSerializer
#from drf_yasg.utils import swagger_auto_schema


class PollCollectionSet(APIView):

    #@swagger_auto_schema(responses={200: PollSerializer})
    def get(self, request, slug, format=None):
        if slug == 'my':
            polls = Poll.objects.filter(user=request.user).all()
        if slug == 'all':
            polls = Poll.objects.all()
        serialized_poll = PollSerializer(polls, many=True)
        return Response({'result': serialized_poll.data}, status=HTTPStatus.OK)

    def post(self, request, format=None):
        try:
            poll_serializer = PollSerializer(request.data)
            if poll_serializer.is_valid:
                new_poll = poll_serializer.create(request.user, request.data)
                new_poll.save()
        except Exception as er:
            return Response({'error': str(er)}, status=HTTPStatus.INTERNAL_SERVER_ERROR)
        return Response({'result': 'The poll has been created', 'poll_id': '{}'.format(new_poll.pk)},
                        status=HTTPStatus.OK)


class PollCollection(APIView):

    permission_classes = (PublicEndpoint,)

    def get(self, request, pk, format=None):
        """
        Get poll
        """
        try:
            poll = Poll.objects.get(poll_id=pk)
            serialized_poll = PollSerializer([poll], many=True)
            return Response({'result': serialized_poll.data}, status=HTTPStatus.OK)
        except Poll.DoesNotExist:
            return Response({'result': 'Not found'}, status=HTTPStatus.NOT_FOUND)

    def put(self, request, pk, format=None):
        try:
            poll = Poll.objects.get(poll_id=pk)

            poll_serializer = PollSerializer(request.data)
            if poll_serializer.is_valid:
                new_poll = poll_serializer.update(poll, request.data)
                return Response({'result': request.data}, status=HTTPStatus.OK)
        except Poll.DoesNotExist:
            return Response({'error': 'Poll is not found, pk: {}'.format(pk)}, status=HTTPStatus.NOT_FOUND)

    def delete(self, request, pk, format=None):
        try:
            polls_count = Poll.objects.filter(poll_id=pk).delete()
            if polls_count == 0:
                return Response({'result': 'This poll is not found'}, status=HTTPStatus.NOT_FOUND)
        except Exception as er:
            return Response({'error': str(er)}, status=HTTPStatus.INTERNAL_SERVER_ERROR)
        return Response({'result': 'The poll has been deleted'}, status=HTTPStatus.OK)
