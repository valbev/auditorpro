"""
Provide implementation of company.yml endpoints.
"""
from django.urls import path

from company.views.company import (
    CompanyCollection,
    CompanySingle,
)
from company.views.schedule import (
    CompanyScheduleCollection,
    CompanyScheduleSingle,
)
from company.views.field import (
    CompanyFieldSingle,
    CompanyFieldCollection
)


company_endpoints = [
    path('', CouponCollection.as_view()),
    path('<int:pk>/', CompanySingle.as_view()),

    path('<int:pk>/schedules/', CompanyScheduleCollection.as_view()),
    path('<int:pk>/schedules/<int:schedule_identifier>/', CompanyScheduleSingle.as_view()),

    path('<int:pk>/fields/', CompanyFieldCollection.as_view()),
    path('<int:pk>/fields/<int:field_identifier>/', CompanyFieldSingle.as_view()),
]
