



class CouponFieldSerializer(serializers.ModelSerializer):
    """
    Company field model serializer implementation.
    """

    class Meta:
        model = CompanyField
        fields = ['id', 'name']