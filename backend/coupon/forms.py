from django import forms
from coupon.models import Coupon


class CouponForm(forms.ModelForm):
    """
    Company form implementation.
    """

    class Meta:
        model = Coupon
        fields = [
            'name',
            'value',
            'activated',
        ]
