from django.db import models


class Coupon(models.Model):

    value = models.CharField(max_length=100, blank=False, null=False)
    name = models.CharField(max_length=100, blank=False, null=False)
    activated = models.BooleanField(blank=True, default=False)


